<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Consultation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id', 'book_id', 'formdate', 'editorial', 'contemporary','lingerie','photographers','outfits','permission'
    ];

}
