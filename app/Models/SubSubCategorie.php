<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubSubCategorie extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'subcat_id'
    ];

}
