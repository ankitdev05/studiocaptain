<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','unique_id', 'type', 'fname', 'lname', 'email', 'number', 'dob', 'address','lead_source','note','attachment'
    ];

}
