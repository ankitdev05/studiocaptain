<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubCategorie extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'cat_id'
    ];

}
