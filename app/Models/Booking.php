<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id', 'refnum','catid', 'subcatid', 'subsubcatid', 'bookdate', 'booktime', 'duration', 'note','voucher','deposite','addon','total','payment_type','attachment'
    ];

}
