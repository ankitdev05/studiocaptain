<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id', 'booking_id', 'refnum', 'photonum', 'size','qty','skin','hair','arms','tummy','legs','tidy','packcost','bookfee','bookrefund','paydate','extra','outstandingamt','status'
    ];

}
