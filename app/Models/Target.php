<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Target extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'photobooking', 'lead', 'convertlead', 'satisfaction','binned','notviewed','phonecall','todo'
    ];

}
