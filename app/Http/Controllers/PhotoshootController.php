<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use App\Models\Booking;
use Session;
use Auth;
use App\User;
use App\Models\Client;
use App\Models\Sale;
use App\Models\ToDo;

class PhotoshootController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $bookArr = array();
        $bookArr1 = array();
        $bookArr2 = array();

        $booking = Booking::where('status','>=','7')->where('status','<=','9')->get();
        foreach($booking as $book) {
            $getClient = Client::where(["id"=>$book->client_id])->first();
            if($getClient) {
                $todoStatus = ToDo::where(["refnum"=>$book->refnum,"type"=>"retouching"])->first();
                $bookArr[] = [
                    "ref_num"=>$book->refnum,
                    "fname"=>$getClient->fname,
                    "lname"=>$getClient->lname,
                    "number"=>$getClient->number,
                    "email"=>$getClient->email,
                    "id"=>$todoStatus->id,
                    "pay_status"=>$todoStatus->status
                ];

                $todoStatus1 = ToDo::where(["refnum"=>$book->refnum,"type"=>"print"])->first();
                $bookArr1[] = [
                    "ref_num"=>$book->refnum,
                    "fname"=>$getClient->fname,
                    "lname"=>$getClient->lname,
                    "number"=>$getClient->number,
                    "email"=>$getClient->email,
                    "id"=>$todoStatus1->id,
                    "pay_status"=>$todoStatus1->status
                ];

                $todoStatus2 = ToDo::where(["refnum"=>$book->refnum,"type"=>"frame"])->first();
                $bookArr2[] = [
                    "ref_num"=>$book->refnum,
                    "fname"=>$getClient->fname,
                    "lname"=>$getClient->lname,
                    "number"=>$getClient->number,
                    "email"=>$getClient->email,
                    "id"=>$todoStatus2->id,
                    "pay_status"=>$todoStatus2->status
                ];
            }
        }
        return view('admin/photoshoot', compact('bookArr','bookArr1','bookArr2'));
    }

    public function pastview() {
        $bookArr = array();
        $booking = Booking::where(["status"=>9])->get();
        foreach($booking as $book) {
            
                $getClient = Client::where(["id"=>$book->client_id])->first();
                $bookArr[] = [
                    "ref_num"=>$book->refnum,
                    "fname"=>$getClient->fname,
                    "lname"=>$getClient->lname,
                    "unique"=>$getClient->unique_id,
                    "number"=>$getClient->number,
                    "email"=>$getClient->email,
                    "refnum"=>$book->refnum,
                    "bookdate"=>$book->bookdate,
                    "total"=>$book->total,
                ];
            
        }
        return view('admin/pastphotoshoot', compact('bookArr'));
    }

    public function debitview() {
        $bookArr = array();
        $booking = Booking::where(["status"=>5])->get();
        foreach($booking as $book) {

                $getClient = Client::where(["id"=>$book->client_id])->first();
                $bookArr[] = [
                    "ref_num"=>$book->refnum,
                    "fname"=>$getClient->fname,
                    "lname"=>$getClient->lname,
                    "unique"=>$getClient->unique_id,
                    "number"=>$getClient->number,
                    "email"=>$getClient->email,
                    "refnum"=>$book->refnum,
                    "bookdate"=>$book->bookdate,
                    "total"=>$book->total,
                ];
        }
        return view('admin/photoshootdebit', compact('bookArr'));
    }

    public function changestatus($id) {
        ToDo::where(["id"=>$id])->update(["status"=>2]);
        return redirect()->back();
    }

    
    
}
