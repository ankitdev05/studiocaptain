<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use Session;
use Auth;
use App\User;
use App\Models\Categorie;
use App\Models\Client;
use App\Models\Note;
use App\Models\Booking;
use App\Models\Recyclebin;
use App\Models\Attachment;

class LeadController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $clients = array();
        $getClients = Client::select('fname','lname','unique_id','number','email','dob','address','lead_source','id')->where(["type"=>2])->orderBy("id","DESC")->get();
        
        foreach($getClients as $getClient) {
            $booking = Booking::select('bookdate')->where(['client_id'=>$getClient->id])->first();
            
            if($booking) {
                $clients[] = [
                    "id"=>$getClient->id,
                    "unique"=>$getClient->unique_id,
                    "fname"=>$getClient->fname,
                    "lname"=>$getClient->lname,
                    "email"=>$getClient->email,
                    "number"=>$getClient->number,
                    "dob"=>$getClient->dob,
                    "address"=>$getClient->address,
                    "bookdate"=>$booking->bookdate,
                    "lead_source"=>$getClient->lead_source,
                ];
            } else {
                $clients[] = [
                    "id"=>$getClient->id,
                    "unique"=>$getClient->unique_id,
                    "fname"=>$getClient->fname,
                    "lname"=>$getClient->lname,
                    "email"=>$getClient->email,
                    "number"=>$getClient->number,
                    "dob"=>$getClient->dob,
                    "address"=>$getClient->address,
                    "bookdate"=>"",
                    "lead_source"=>$getClient->lead_source,
                ];
            }
        }

        // $staffArr = array();
        // $staffs = User::where(["role"=>3])->get();
        // foreach($staffs as $staff) {
        //     $staffArr[] = ["id"=>$staff->id, "name"=>$staff->name, "email"=>$staff->email];
        // }
        
        return view('admin/leadbook', compact('clients'));
    }

    public function addlead() {
        $cats = Categorie::All();
        return view('admin/addlead', compact('cats'));
    }

    public function leadcreate(Request $request) {
        $this->validate($request, [
            "fname"=>"required",
            "lname"=>"required",
            "email"=>"required|email",
            "number"=>"required",
        ]);

        if($request->hasFile('fileattach')) {
            $filenameWithExt = $request->file('fileattach')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('fileattach')->getClientOriginalExtension();
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            $path = $request->file('fileattach')->storeAs('public/clientattach', $fileNameToStore);
        } else {
            $fileNameToStore = '';
        }

        $created = Client::create([
            "user_id"=>Auth::user()->id,
            "unique_id"=> 'c'.$this->random_strings(4),
            "type"=>2,
            "fname"=>$request->fname,
            "lname"=>$request->lname,
            "email"=>$request->email,
            "number"=>$request->number,
            "dob"=>$request->dob,
            "address"=>$request->address,
            "lead_source"=>$request->source,
            "note"=>$request->note,
        ]);

        Attachment::create([
            "client_id"=>$created->id,
            "type"=>'lead',
            "attachfile"=>$fileNameToStore,
        ]);

        return redirect()->route('lead-book');
    }

    public function leadnote(Request $request) {

        $this->validate($request, [
            "message"=>"required",
        ]);
        
        if(isset($request->datetime)) {
            Note::create([
                "user_id"=>Auth::user()->id,
                "attach_id"=>$request->attach,
                "type"=>$request->type,
                "title"=>$request->title,
                "datetime"=>$request->datetime,
                "message"=>$request->message,
            ]);
        } else {
            Note::create([
                "user_id"=>Auth::user()->id,
                "attach_id"=>$request->attach,
                "type"=>$request->type,
                "title"=>$request->title,
                "datetime"=>"",
                "message"=>$request->message,
            ]);
        }

        return redirect()->back();
    }

    public function leadview(Request $request) {
        $clients = Client::select('fname','lname','unique_id','number','email','id')->where(['id'=>$request->id])->first();
        return response()->json(array('msg'=> $clients), 200);
    }

    public function leadsingle($id) {
        $clients = array();
        $getClient = Client::where(['id'=>$id])->first();
        
        $clients = [
            "id"=>$getClient->id,
            "unique"=>$getClient->unique_id,
            "fname"=>$getClient->fname,
            "lname"=>$getClient->lname,
            "email"=>$getClient->email,
            "number"=>$getClient->number,
            "dob"=>$getClient->dob,
            "address"=>$getClient->address,
            "lead_source"=>$getClient->lead_source,
            "note"=>$getClient->note,
        ];

        $getNotes = Note::where(["attach_id"=>$getClient->id, "type"=>"lead"])->get();
        $attachArr = array();
        $getattachs = Attachment::where(["client_id"=>$id, "type"=>"lead"])->get();
        if($getattachs) {
            foreach($getattachs as $getattach) {
                $attachArr[] = ["attach"=>$getattach->attachfile];
            }
        }
        return view('admin/leadprofile', compact('clients','getNotes','attachArr'));
    }

    public function leadedit($id) {
        $clients = array();
        $getClient = Client::where(['id'=>$id])->first();
        
        $clients = [
            "id"=>$getClient->id,
            "unique"=>$getClient->unique_id,
            "fname"=>$getClient->fname,
            "lname"=>$getClient->lname,
            "email"=>$getClient->email,
            "number"=>$getClient->number,
            "dob"=>$getClient->dob,
            "address"=>$getClient->address,
            "lead_source"=>$getClient->lead_source,
            "note"=>$getClient->note,
        ];

        return view('admin/editlead', compact('clients'));
    }

    public function leadupdate(Request $request, $id) {
        $this->validate($request, [
            "fname"=>"required",
            "lname"=>"required",
            "email"=>"required|email",
            "number"=>"required",
        ]);

        $getClient = Client::where(['id'=>$id])->first();
        $getClient->fname = $request->fname;
        $getClient->lname = $request->lname;
        $getClient->email = $request->email;
        $getClient->number = $request->number;
        $getClient->dob = $request->dob;
        $getClient->address = $request->address;
        $getClient->lead_source = $request->source;
        $getClient->note = $request->note;
        $getClient->save();

        if($request->hasFile('fileattach')) {
            $filenameWithExt = $request->file('fileattach')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('fileattach')->getClientOriginalExtension();
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            $path = $request->file('fileattach')->storeAs('public/clientattach', $fileNameToStore);

            $findAttach = Attachment::where(["client_id"=>$id, "type"=>"lead"])->first();
            if($findAttach) {
                $findAttach->attachfile = $fileNameToStore;
                $findAttach->save();
            } else {
                Attachment::create([
                    "client_id"=>$id,
                    "type"=>'lead',
                    "attachfile"=>$fileNameToStore,
                ]);
            }
        }

        return redirect()->back();
    }

    public function movetobin($id) {
        $clients = Client::where(['id'=>$id])->first();

        Recyclebin::create([
            "user_id"=>$clients->user_id,
            "unique_id"=> $clients->unique_id,
            "type"=>$clients->type,
            "fname"=>$clients->fname,
            "lname"=>$clients->lname,
            "email"=>$clients->email,
            "number"=>$clients->number,
            "lead_source"=>$clients->lead_source,
            "note"=>$clients->note,
        ]);
        
        Client::where(['id'=>$id])->delete();
        return redirect()->route('lead-book');
    }

    public function uploadFile(Request $request) {

        if ($request->input('submit') != null ) {
    
          $file = $request->file('file');
          // File Details 
          $filename = $file->getClientOriginalName();
          $extension = $file->getClientOriginalExtension();
          $tempPath = $file->getRealPath();
          $fileSize = $file->getSize();
          $mimeType = $file->getMimeType();
    
          // Valid File Extensions
          $valid_extension = array("csv");
    
          // 2MB in Bytes
          $maxFileSize = 2097152; 
    
          // Check file extension
          if(in_array(strtolower($extension),$valid_extension)){
    
            // Check file size
            if($fileSize <= $maxFileSize){
    
              // File upload location
              $location = 'fileuploade';
    
              // Upload file
              $file->move($location,$filename);
    
              // Import CSV to Database
              $filepath = public_path($location."/".$filename);
    
              // Reading file
              $file = fopen($filepath,"r");
    
              $importData_arr = array();
              $i = 0;
    
              while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                 $num = count($filedata );
                 
                 // Skip first row (Remove below comment if you want to skip the first row)
                 if($i == 0){
                    $i++;
                    continue; 
                 }
                 for ($c=0; $c < $num; $c++) {
                    $importData_arr[$i][] = $filedata [$c];
                 }
                 $i++;
              }
              fclose($file);
    
              // Insert to MySQL database
              foreach($importData_arr as $importData){

                $insertData = array(
                    "user_id"=>Auth::user()->id,
                    "unique_id"=> 'c'.$this->random_strings(4),
                    "type"=>2,
                    "fname"=>$importData[0],
                    "lname"=>$importData[1],
                    "email"=>$importData[2],
                    "number"=>$importData[3],
                    "dob"=>$importData[4],
                    "address"=>$importData[5],
                    "lead_source"=>$importData[6],
                    "note"=>$importData[7]
                );
                Client::create($insertData);
              }
              Session::flash('message','Import Successful.');
            }else{
              Session::flash('message','File too large. File must be less than 2MB.');
            }
    
          }else{
             Session::flash('message','Invalid File Extension.');
          }
    
        } else {
            Session::flash('message','Invalid Request');
        }
        // Redirect to index
        return redirect()->action('LeadController@index');
    }

    function random_strings($length_of_string) {
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle($str_result), 0, $length_of_string);
    }

}
