<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use Session;
use Auth;
use DB;
use App\User;
use App\Models\Client;
use App\Models\Sale;
use App\Models\Booking;
use App\Models\Order;
use App\Models\Appointment;
use Illuminate\Support\Facades\Mail;
use App\Mail\FeedbackEmail;

class SaleController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        return view('admin/sale');
    }

    public function view($id) {
        
        $clients = array();
        $booking = Booking::where(['refnum'=>$id])->first();
        $getClient = Client::where(['id'=>$booking->client_id])->first();
        $getOrder = Order::where(['refnum'=>$booking->refnum])->first();

        if($getOrder) {
            $clients = [
                "id"=>$getClient->id,
                "unique"=>$getClient->unique_id,
                "fname"=>$getClient->fname,
                "lname"=>$getClient->lname,
                "email"=>$getClient->email,
                "number"=>$getClient->number,
                "dob"=>$getClient->dob,
                "address"=>$getClient->address,
                "product"=>'',
                "quantity"=>$getOrder->qty,
                "payment_type"=>$booking->payment_type,
                "amount"=>$booking->total,
            ];
        } else {
            $clients = [
                "id"=>$getClient->id,
                "unique"=>$getClient->unique_id,
                "fname"=>$getClient->fname,
                "lname"=>$getClient->lname,
                "email"=>$getClient->email,
                "number"=>$getClient->number,
                "dob"=>$getClient->dob,
                "address"=>$getClient->address,
                "product"=>'',
                "quantity"=>'',
                "payment_type"=>$booking->payment_type,
                "amount"=>$booking->total,
            ];
        }
        Booking::where(["refnum"=>$booking->refnum])->update(["status"=>8]);

        return view('admin/saleview', compact('clients'));
    }

    public function ordersaleview($id) {
        $clients = array();
        $order = Order::where(['id'=>$id])->first();
        $getBook = Booking::where(['id'=>$order->booking_id])->first();
        $getClient = Client::where(['id'=>$order->client_id])->first();

        $clients = [
            "id"=>$getClient->id,
            "refnum"=>$getBook->refnum,
            "unique"=>$getClient->unique_id,
            "fname"=>$getClient->fname,
            "lname"=>$getClient->lname,
            "email"=>$getClient->email,
            "number"=>$getClient->number,
            "dob"=>$getClient->dob,
            "address"=>$getClient->address,
            "product"=>'',
            "quantity"=>$order->qty,
            "payment_type"=>$getBook->payment_type,
            "amount"=>$order->outstandingamt,
        ];

        Booking::where(["refnum"=>$getBook->refnum])->update(["status"=>8]);

        return view('admin/ordersaleview', compact('clients'));
    }

    public function treatmentsaleview($id) {
        $clients = array();
        $getBook = Booking::where(['id'=>$id])->first();
        $getClient = Client::where(['id'=>$getBook->client_id])->first();

        $clients = [
            "id"=>$getClient->id,
            "refnum"=>$getBook->refnum,
            "unique"=>$getClient->unique_id,
            "fname"=>$getClient->fname,
            "lname"=>$getClient->lname,
            "email"=>$getClient->email,
            "number"=>$getClient->number,
            "dob"=>$getClient->dob,
            "address"=>$getClient->address,
            "product"=>'',
            "quantity"=>'',
            "payment_type"=>$getBook->payment_type,
            "amount"=>$getBook->total - $getBook->deposite,
        ];

        return view('admin/ordersaleview', compact('clients'));
    }

    public function savesale(Request $request) {
        $this->validate($request, [
            "fname"=>"required",
            "lname"=>"required",
            "email"=>"required|email",
            "number"=>"required",
            "dob"=>"required",
            "address"=>"required",
            "product"=>"required",
            "quantity"=>"required",
            "type"=>"required",
            "amount"=>"required",
        ]);

        $createClient = Client::create([
            "user_id"=>Auth::user()->id,
            "unique_id"=> 'c'.$this->random_strings(4),
            "type"=>1,
            "fname"=>$request->fname,
            "lname"=>$request->lname,
            "email"=>$request->email,
            "number"=>$request->number,
            "dob"=>$request->dob,
            "address"=>$request->address,
        ]);
        
        $saveSale = Sale::Create([
            "client_id"=>$createClient->id,
            "product"=>$request->product,
            "quantity"=>$request->quantity,
            "payment_type"=>$request->type,
            "amount"=>$request->amount,
        ]);

        $saleid = base64_encode($saveSale->id);
        $email = $request->email;
        Mail::to($email)->send(new FeedbackEmail($saleid));
        
        return redirect()->back();
    }

    public function ordersavesale(Request $request, $id) {
        $this->validate($request, [
            "product"=>"required",
            "quantity"=>"required",
            "type"=>"required",
            "amount"=>"required",
        ]);

        $getBook = Booking::where(['refnum'=>$id])->first();
        
        $saveSale = Sale::Create([
            "client_id"=>$getBook->client_id,
            "refnum"=>$id,
            "product"=>$request->product,
            "quantity"=>$request->quantity,
            "payment_type"=>$request->type,
            "amount"=>$request->amount,
        ]);

        $saleid = base64_encode($saveSale->id);
        $email = $request->email;
        Mail::to($email)->send(new FeedbackEmail($saleid));

        Appointment::where(["refnum"=>$id])->delete();
        DB::table('bookings')->where(["refnum"=>$id])->update(["status"=>9]);
        
        return redirect()->route('calender');
    }

    function random_strings($length_of_string) {
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle($str_result), 0, $length_of_string);
    }

    public function save(Request $request, $id) {
        $this->validate($request, [
            "fname"=>"required",
            "lname"=>"required",
            "email"=>"required|email",
            "number"=>"required",
            "dob"=>"required",
            "address"=>"required",
            "product"=>"required",
            "quantity"=>"required",
            "type"=>"required",
            "amount"=>"required",
        ]);

        $getClient = Client::where(['id'=>$id])->first();
        $getClient->fname = $request->fname;
        $getClient->lname = $request->lname;
        $getClient->email = $request->email;
        $getClient->number = $request->number;
        $getClient->dob = $request->dob;
        $getClient->address = $request->address;
        $getClient->save();
        
        $saveSale = Sale::Create([
            "client_id"=>$id,
            "product"=>$request->product,
            "quantity"=>$request->quantity,
            "payment_type"=>$request->type,
            "amount"=>$request->amount,
        ]);

        $saleid = base64_encode($saveSale->id);
        $email = $request->email;
        Mail::to($email)->send(new FeedbackEmail($saleid));
        
        return redirect()->route('client-book');
    }

}
