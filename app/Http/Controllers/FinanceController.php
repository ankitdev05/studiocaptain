<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use Session;
use Auth;
use Carbon\Carbon;
use View;
use DB;
use App\Models\Client;
use App\Models\Sale;
use App\Models\Booking;
use App\Models\Categorie;
use App\Models\SubCategorie;
use App\Models\SubSubCategorie;

class FinanceController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index() {
        $photoDatas = array();
        $treatDatas = array();
        $otherDatas = array();

        $totalAmountA = 0;
        $totalAmountB = 0;
        $totalAmountC = 0;
        $totalPhotoA = 0;
        $totalPhotoB = 0;


        $getSales = Sale::All();
        
        if($getSales) {
        
            foreach($getSales as $getSale) {
                $getBook = Booking::where(['refnum'=>$getSale->refnum])->first();
                $getClient = Client::where(['id'=>$getSale->client_id])->first();

                if($getBook) {
                    if($getBook->catid == 1) {
                            $photoDatas[] = [
                                    "id"=>$getClient->id,
                                    "unique"=>$getClient->unique_id,
                                    "booknum"=>$getBook->refnum,
                                    "fname"=>$getClient->fname,
                                    "lname"=>$getClient->lname,
                                    "email"=>$getClient->email,
                                    "number"=>$getClient->number,
                                    "product"=>$getSale->product,
                                    "quantity"=>$getSale->quantity,
                                    "payment_type"=>$getSale->payment_type,
                                    "amount"=>$getSale->amount,
                            ];
                        $totalAmountA = $totalAmountA + $getSale->amount;
                    } else {
                            $treatDatas[] = [
                                    "id"=>$getClient->id,
                                    "unique"=>$getClient->unique_id,
                                    "booknum"=>$getBook->refnum,
                                    "fname"=>$getClient->fname,
                                    "lname"=>$getClient->lname,
                                    "email"=>$getClient->email,
                                    "number"=>$getClient->number,
                                    "product"=>$getSale->product,
                                    "quantity"=>$getSale->quantity,
                                    "payment_type"=>$getSale->payment_type,
                                    "amount"=>$getSale->amount,
                            ];
                        $totalAmountB = $totalAmountB + $getSale->amount;
                    }
                } else {
                    $otherDatas[] = [
                                    "id"=>$getClient->id,
                                    "unique"=>$getClient->unique_id,
                                    "fname"=>$getClient->fname,
                                    "lname"=>$getClient->lname,
                                    "email"=>$getClient->email,
                                    "number"=>$getClient->number,
                                    "product"=>$getSale->product,
                                    "quantity"=>$getSale->quantity,
                                    "payment_type"=>$getSale->payment_type,
                                    "amount"=>$getSale->amount,
                            ];
                    $totalAmountC = $totalAmountC + $getSale->amount;
                }
            }
        }

        $bookArr = array();
        $getBooks = Booking::all();
        if($getBooks) {
            foreach($getBooks as $getBook) {
                $getClient = Client::where(['id'=>$getBook->client_id])->first();

                $cats = Categorie::where(['id'=>$getBook->catid])->first();
                $subCats = SubCategorie::where(['id'=>$getBook->catid])->first();
                $subSubCats = SubSubCategorie::where(['id'=>$getBook->subcatid])->first();

                $bookArr[] = [
                        "id"=>$getClient->id,
                        "unique"=>$getClient->unique_id,
                        "fname"=>$getClient->fname,
                        "lname"=>$getClient->lname,
                        "email"=>$getClient->email,
                        "number"=>$getClient->number,
                        "dob"=>$getClient->dob,
                        "address"=>$getClient->address,
                        "catid"=>$cats->name,
                        "subcatid"=>$subCats->name,
                        "subsubcatid"=>$subSubCats->name,
                        "bookdate"=>$getBook->bookdate,
                        "booktime"=>date('h:ia',$getBook->booktime),
                        "duration"=>$getBook->duration,
                        "note"=>$getBook->note,
                        "voucher"=>$getBook->voucher,
                        "deposite"=>$getBook->deposite,
                        "addon"=>$getBook->addon,
                        "total"=>$getBook->total,
                        "payment_type"=>$getBook->payment_type,
                ];
            }
        }


        return view('admin/finance', compact('photoDatas', 'treatDatas','otherDatas','totalAmountA','totalPhotoA','totalAmountB','totalPhotoB','totalAmountC','bookArr'));
    }

    public function filterphoto(Request $request) {
        $photoDatas = array();
        $totalAmountA = 0;
        $totalPhotoA = 0;

        $dateS = new Carbon($request->fromdate);
        $dateE = new Carbon($request->todate);

        $getSales = Sale::whereBetween('created_at', [$dateS->format('Y-m-d'), $dateE->format('Y-m-d')])->get();
        
        if($getSales) {
        
            foreach($getSales as $getSale) {
                $getBook = Booking::where(['refnum'=>$getSale->refnum])->first();
                if($getBook) {
                    if($getBook->catid == 1) {
                        $getClient = Client::where(['id'=>$getSale->client_id])->first();
                        $photoDatas[] = [
                                "id"=>$getClient->id,
                                "unique"=>$getClient->unique_id,
                                "booknum"=>$getBook->refnum,
                                "fname"=>$getClient->fname,
                                "lname"=>$getClient->lname,
                                "email"=>$getClient->email,
                                "number"=>$getClient->number,
                                "product"=>$getSale->product,
                                "quantity"=>$getSale->quantity,
                                "payment_type"=>$getSale->payment_type,
                                "amount"=>$getSale->amount,
                        ];
                        $totalAmountA = $totalAmountA + $getSale->amount;
                    }
                }
            }
        }

        $view = View::make('admin/financefilter', compact('photoDatas','totalAmountA','totalPhotoA'));
        return $contents = $view->render();
    }

    public function filtertreat(Request $request) {
        $photoDatas = array();
        $totalAmountA = 0;

        $dateS = new Carbon($request->fromdate);
        $dateE = new Carbon($request->todate);

        $getSales = Sale::whereBetween('created_at', [$dateS->format('Y-m-d'), $dateE->format('Y-m-d')])->get();
        
        if($getSales) {
        
            foreach($getSales as $getSale) {
                $getBook = Booking::where(['refnum'=>$getSale->refnum])->first();
                if($getBook) {
                    if($getBook->catid == 2) {
                        $getClient = Client::where(['id'=>$getSale->client_id])->first();
                        $photoDatas[] = [
                                "id"=>$getClient->id,
                                "unique"=>$getClient->unique_id,
                                "booknum"=>$getBook->refnum,
                                "fname"=>$getClient->fname,
                                "lname"=>$getClient->lname,
                                "email"=>$getClient->email,
                                "number"=>$getClient->number,
                                "product"=>$getSale->product,
                                "quantity"=>$getSale->quantity,
                                "payment_type"=>$getSale->payment_type,
                                "amount"=>$getSale->amount,
                        ];
                        $totalAmountA = $totalAmountA + $getSale->amount;
                    }
                }
            }
        }

        $view = View::make('admin/financefilterr', compact('photoDatas','totalAmountA'));
        return $contents = $view->render();
    }

    public function filterdirect(Request $request) {
        $photoDatas = array();
        $totalAmountA = 0;

        $dateS = new Carbon($request->fromdate);
        $dateE = new Carbon($request->todate);

        $getSales = Sale::whereBetween('created_at', [$dateS->format('Y-m-d'), $dateE->format('Y-m-d')])->get();
        
        if($getSales) {
        
            foreach($getSales as $getSale) {
                $getBook = Booking::where(['refnum'=>$getSale->refnum])->first();
                if($getBook) {
                    
                } else {
                    $getClient = Client::where(['id'=>$getSale->client_id])->first();
                    $photoDatas[] = [
                            "id"=>$getClient->id,
                            "unique"=>$getClient->unique_id,
                            "fname"=>$getClient->fname,
                            "lname"=>$getClient->lname,
                            "email"=>$getClient->email,
                            "number"=>$getClient->number,
                            "product"=>$getSale->product,
                            "quantity"=>$getSale->quantity,
                            "payment_type"=>$getSale->payment_type,
                            "amount"=>$getSale->amount,
                    ];
                    $totalAmountA = $totalAmountA + $getSale->amount;
                }
            }
        }

        $view = View::make('admin/financefilterrr', compact('photoDatas','totalAmountA'));
        return $contents = $view->render();
    }

}
