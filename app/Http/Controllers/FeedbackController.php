<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use Session;
use Auth;
use DB;
use App\User;
use App\Models\Client;
use App\Models\Sale;
use App\Models\Booking;
use App\Models\FeedBack;

class FeedbackController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($id) {
        $id = base64_decode($id);
        //return view('admin/feedback');
        $feedback = FeedBack::where(["id"=>$id])->first();

        if($feedback) {
            Session::flash('error','Feedback Already Submitted Successful.');    
        }
        return view('admin/feedback', compact('id'));
    }

    public function feedbacksave(Request $request, $id) {
        $this->validate($request, [
            "experience"=>"required",
            "comments"=>"required",
            "inputone"=>"required",
        ]);

        $feedback = FeedBack::where(["id"=>$id])->first();

        if(!$feedback) {
            FeedBack::create([
                "sale_id"=>$id,
                "experience"=>$request->experience,
                "comments"=>$request->comments,
                "inputone"=>$request->inputone,
            ]);
            Session::flash('success','Feedback Submitted Successful.');
        }
        
        Session::flash('error','Feedback Already Submitted Successful.');

        return redirect()->back();
    }

}
