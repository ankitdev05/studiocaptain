<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Auth;
use Session;
use Symfony\Component\HttpFoundation\Response;

class RegisterController extends Controller {
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */

    public function view() {
        return view('admin/register');
    }

    public function create(Request $request) {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|min:5',
            'mobile' => 'required',
            'address' => 'required',
            'role' => 'required',
        ]);

        User::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'password'=> bcrypt($request->password),
            'mobile'=> $request->mobile,
            'address'=> $request->address,
            'role'=> $request->role
        ]);
        $request->session()->flash('success', 'User created successfully');
        return redirect()->route('add-user');
    }

    function generateUniqueNumber() {
        $number = mt_rand(1000,999999);
    
        if ($this->uniqueNumberExists($number)) {
            return $this->generateUniqueNumber();
        }
        return $number;
    }
    
    function uniqueNumberExists($number) {
        return User::where('unique_id', $number)->exists();
    }
}
