<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use App\User;
use App\Models\RequestPassword;
use Session;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }

    public function index() {
        return view("admin/login");
    }

    public function check(Request $request, MessageBag $message_bag) {
        
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:5'
        ]);
        
        if(Auth::guard('web')->attempt(['email'=>$request->email, 'password'=>$request->password, 'status'=>1], $request->remember)) {
            return redirect()->intended(route('dashboard'));
        } else {
            $check = User::where(['email'=> $request->email])->first();

            if($check) {

            } else {
                Session::flash('_old_input.email', $request->email);
                $message_bag->add('email', 'These credentials do not match');
            }
            
            return view('admin/login')->withErrors($message_bag);
        }

        return view('admin/login');
    }

    public function logout() {
        Auth::logout();
	    return Redirect()->route('login');
    }

    public function forgot(Request $request) {
        $this->validate($request, [
            'email' => 'required|string|email',
        ]);
        
        $getUser = User::where('email', $request->email)->first();
        
        if($getUser) {
            
            if($getUser->role == 1) {
                Mail::to($email)->send(new adminPasswordReset($email));
                Session::flash('error', 'New password has been send to your email id');
                return redirect()->route('login');

            } elseif($getUser->role == 2) {

                return redirect()->route('login');

            } elseif($getUser->role == 3) {

                $reqPass = RequestPassword::where(['email'=>$request->email])->first();

                if($reqPass) {
                    RequestPassword::create(['email'=>$request->email]);
                    Session::flash('error', 'Request for change password has been send to admin');
                    return redirect()->route('login');
                } else {
                    Session::flash('error', 'Already request for change password');
                    return redirect()->route('login');
                }
            }
        } else {
            Session::flash('error', 'Email id does not exists');
            return redirect()->route('login');
        }
    }

    public function forgotChangePassword($id) {
        $user = User::where(["email"=>$id, 'role'=>'1'])->first();
        $newpass = $this->random_code(6);
        $user->password = bcrypt($newpass);
        $user->save();

        Mail::to($email)->send(new ChangePassword($email, $newpass));

        Session::flash('success', 'Password Changed successfully.');
        return redirect()->route('login');
    }

    function random_code($limit) {
        return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
    }
}
