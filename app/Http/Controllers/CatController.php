<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use Session;
use Auth;
use App\Models\Categorie;
use App\Models\SubCategorie;
use App\Models\SubSubCategorie;

class CatController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function subcategory(Request $request) {
        $subs = '<option value="">Type Of Services</option>';
        $subcats = SubCategorie::where(['cat_id'=>$request->id])->get();
        
        foreach($subcats as $subcat) {
            $subs = $subs . "<option value=". $subcat->id .">". $subcat->name ."</option>";
        }

        return response()->json(array('msg'=> $subs), 200);
    }

    public function subsubcategory(Request $request) {
        $subs = '<option value="">Type Of Services</option>';
        $subcats = SubSubCategorie::where(['subcat_id'=>$request->id])->get();
        
        foreach($subcats as $subcat) {
            $subs = $subs . "<option value=". $subcat->id .">". $subcat->name ."</option>";
        }

        return response()->json(array('msg'=> $subs), 200);
    }

}
