<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use Session;
use Auth;
use Carbon\Carbon;
use View;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\AppointNoShow;
use App\User;
use App\Models\Client;
use App\Models\Booking;
use App\Models\Categorie;
use App\Models\SubCategorie;
use App\Models\SubSubCategorie;
use App\Models\Appointment;
use App\Models\Order;
use App\Models\LastAction;
use App\Models\Attachment;
use App\Models\Refund;
use App\Models\ToDo;
use App\Models\Consultation;

class CalenderController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {

        $todayArr = array();
        $getTodays = Booking::whereDate('bookdate', '=', Carbon::today()->toDateString())->get();
        if($getTodays) {
            foreach($getTodays as $getToday) {
                if($getToday->status == 1 || $getToday->status == 2) {
                    $getclient = Client::where(["id"=>$getToday->client_id])->first();
                    $getcat = Categorie::select('name')->where(["id"=>$getToday->catid])->first();
                    $getsubcat = SubCategorie::select('name')->where(["id"=>$getToday->subcatid])->first();
                    $getsubsubcat = SubSubCategorie::select('name')->where(["id"=>$getToday->subsubcatid])->first();

                    $todayArr[] = [
                        "id"=>$getToday->id,
                        "name"=>$getclient->fname.' '.$getclient->lname,
                        "Booknum"=>$getToday->refnum,
                        "Category"=>$getcat->name,
                        "Categoryid"=>$getToday->catid,
                        "SubCategory"=>$getsubcat->name,
                        "SubsubCategory"=>$getsubsubcat->name,
                        "date"=>$getToday->bookdate,
                        "time"=>date('h:ia', $getToday->booktime),
                    ];
                }
            }
        }

        $tomArr = array();
        $getToms = Booking::whereDate('bookdate', '=', Carbon::now()->tomorrow()->toDateString())->get();
        if($getToms) {
            foreach($getToms as $getTom) {
                if($getTom->status == 1 || $getTom->status == 2) {
                    $getclient = Client::where(["id"=>$getTom->client_id])->first();
                    $getcat = Categorie::select('name')->where(["id"=>$getTom->catid])->first();
                    $getsubcat = SubCategorie::select('name')->where(["id"=>$getTom->subcatid])->first();
                    $getsubsubcat = SubSubCategorie::select('name')->where(["id"=>$getTom->subsubcatid])->first();

                    $tomArr[] = [
                        "id"=>$getTom->id,
                        "name"=>$getclient->fname.' '.$getclient->lname,
                        "Booknum"=>$getTom->refnum,
                        "Category"=>$getcat->name,
                        "Categoryid"=>$getTom->catid,
                        "SubCategory"=>$getsubcat->name,
                        "SubsubCategory"=>$getsubsubcat->name,
                        "date"=>$getTom->bookdate,
                        "time"=>date('h:ia', $getTom->booktime),
                    ];
                }
            }
        }

        $open_time = strtotime("9:00 A.M");
        $close_time = strtotime("9:00 P.M");
        $now = time();
        $gettimeWises = array();
        $tt = 1;

        for( $i=$open_time; $i<=$close_time; $i+=900) {

            $timeArr = array();
            $timeArr1 = array();
            $timeArr2 = array();
            $timeArr3 = array();

            $timeNew = date("h:ia",$i);
            $timeStr = (int)strtotime($timeNew);

            //$minustime = $timeStr + 899;

            $getWises = Booking::select('id','client_id','refnum','bookdate','booktime','status','subcatid')->where('booktime', $timeStr)->whereDate('bookdate', '=', Carbon::today()->toDateString())->get();
            
            $timeArr = array();
            $timeArr1 = array();
            $timeArr2 = array();
            $timeArr3 = array();

            if($getWises) {
                
                foreach($getWises as $getWise) {
                    
                    if($getWise->status == 1 || $getWise->status == 2) {
                        $clientGet = Client::select('fname','lname')->where(["id"=>$getWise->client_id])->first();

                        if($getWise->subcatid == 1) {

                            $timeArr = ["id"=>$getWise->id,"clientid"=>$getWise->client_id,"name"=>$clientGet->fname.' '.$clientGet->lname,"bookNum"=>$getWise->refnum,"bookdate"=>$getWise->bookdate,"booktime"=>date('h:ia',$getWise->booktime)];

                        } elseif($getWise->subcatid == 2) {

                            $timeArr1 = ["id"=>$getWise->id,"clientid"=>$getWise->client_id,"name"=>$clientGet->fname.' '.$clientGet->lname,"bookNum"=>$getWise->refnum,"bookdate"=>$getWise->bookdate,"booktime"=>date('h:ia',$getWise->booktime)];
                        
                        } elseif($getWise->subcatid == 3) {

                            $timeArr2 = ["id"=>$getWise->id,"clientid"=>$getWise->client_id,"name"=>$clientGet->fname.' '.$clientGet->lname,"bookNum"=>$getWise->refnum,"bookdate"=>$getWise->bookdate,"booktime"=>date('h:ia',$getWise->booktime)];
                        
                        } elseif($getWise->subcatid == 4) {
                            
                            $timeArr3 = ["id"=>$getWise->id,"clientid"=>$getWise->client_id,"name"=>$clientGet->fname.' '.$clientGet->lname,"bookNum"=>$getWise->refnum,"bookdate"=>$getWise->bookdate,"booktime"=>date('h:ia',$getWise->booktime)];
                        }
                    }
                }
            } else {
                $timeArr = [];
                $timeArr1 = [];
                $timeArr2 = [];
                $timeArr3 = [];
            }

            $getWises4 = Appointment::select('id','client_id','refnum','bookdate','booktime')->where('booktime',$timeStr)->whereDate('bookdate', '=', Carbon::today()->toDateString())->get();
            $timeArr4 = array();
            if($getWises4) {
                
                foreach($getWises4 as $getWise) {
                    $checkBook = Booking::select('catid')->where(["refnum"=>$getWise->refnum])->first();

                    if($checkBook->catid == 1) {
                        $clientGet = Client::select('fname','lname')->where(["id"=>$getWise->client_id])->first();
                        $timeArr4 = ["id"=>$getWise->id,"name"=>$clientGet->fname.' '.$clientGet->lname,"bookNum"=>$getWise->refnum,"bookdate"=>$getWise->bookdate,"booktime"=>date('h:ia',$getWise->booktime)];
                    }
                }
            } else {
                $timeArr4 = [];
            }

            $gettimeWises[] = ['time'=>$timeNew,'strtotime'=>$timeStr,'data1'=>$timeArr,'data2'=>$timeArr1,'data3'=>$timeArr2,'data4'=>$timeArr3,'data5'=>$timeArr4];

        }

        $lastActions = LastAction::orderBy("id","DESC")->limit(10)->get();

        //dd($gettimeWises);
        return view('admin/calender', compact('todayArr','tomArr','gettimeWises','lastActions'));
    }

    public function calendarmonth(Request $request) {
        $monthArr = array();
        $dateS = new Carbon($request->fromdate);
        $dateE = new Carbon($request->todate);

        $getMonths = Booking::whereBetween('bookdate', [$dateS->format('Y-m-d'), $dateE->format('Y-m-d')])->get();
        
        if($getMonths) {
            foreach($getMonths as $getMonth) {
                if($getMonth->status == 1 || $getMonth->status == 2) {
                    $getclient = Client::where(["id"=>$getMonth->client_id])->first();
                    $getcat = Categorie::select('name')->where(["id"=>$getMonth->catid])->first();
                    $getsubcat = SubCategorie::select('name')->where(["id"=>$getMonth->subcatid])->first();
                    $getsubsubcat = SubSubCategorie::select('name')->where(["id"=>$getMonth->subsubcatid])->first();

                    $monthArr[] = [
                        "id"=>$getMonth->id,
                        "name"=>$getclient->fname.' '.$getclient->lname,
                        "Booknum"=>$getMonth->refnum,
                        "Category"=>$getcat->name,
                        "Categoryid"=>$getMonth->catid,
                        "SubCategory"=>$getsubcat->name,
                        "SubsubCategory"=>$getsubsubcat->name,
                        "date"=>$getMonth->bookdate,
                        "time"=>date('h:ia', $getMonth->booktime),
                    ];
                }
            }
        }
        $view = View::make('admin/calendarmonth', compact('monthArr'));
        return $contents = $view->render();
    }

    public function calenderday(Request $request) {
        $monthArr = array();
        $dateS = new Carbon($request->dayselect);

        $getMonths = Booking::where('bookdate', $dateS->format('Y-m-d'))->get();
        
        if($getMonths) {
            foreach($getMonths as $getMonth) {
                if($getMonth->status == 1 || $getMonth->status == 2) {
                    $getclient = Client::where(["id"=>$getMonth->client_id])->first();
                    $getcat = Categorie::select('name')->where(["id"=>$getMonth->catid])->first();
                    $getsubcat = SubCategorie::select('name')->where(["id"=>$getMonth->subcatid])->first();
                    $getsubsubcat = SubSubCategorie::select('name')->where(["id"=>$getMonth->subsubcatid])->first();

                    $monthArr[] = [
                        "id"=>$getMonth->id,
                        "name"=>$getclient->fname.' '.$getclient->lname,
                        "Booknum"=>$getMonth->refnum,
                        "Category"=>$getcat->name,
                        "Categoryid"=>$getMonth->catid,
                        "SubCategory"=>$getsubcat->name,
                        "SubsubCategory"=>$getsubsubcat->name,
                        "date"=>$getMonth->bookdate,
                        "time"=>date('h:ia', $getMonth->booktime),
                    ];
                }
            }
        }
        $view = View::make('admin/calendarmonth', compact('monthArr'));
        return $contents = $view->render();
    }

    public function calenderblock(Request $request) {
        $dateS = new Carbon($request->dayselect);

        $open_time = strtotime("9:00 A.M");
        $close_time = strtotime("9:00 P.M");
        $now = time();
        $gettimeWises = array();
        $tt = 1;

        for( $i=$open_time; $i<=$close_time; $i+=900) {

            $timeArr = array();
            $timeArr1 = array();
            $timeArr2 = array();
            $timeArr3 = array();

            $timeNew = date("h:ia",$i);
            $timeStr = strtotime($timeNew);

            //$minustime = $timeStr + 899;

            $getWises = Booking::select('id','client_id','refnum','bookdate','booktime','status','subcatid')->where('booktime',$timeStr)->where('bookdate', $dateS->format('Y-m-d'))->get();
            $timeArr = array();
            if($getWises) {
                
                foreach($getWises as $getWise) {

                    if($getWise->status == 1 || $getWise->status == 2) {
                        $clientGet = Client::select('fname','lname')->where(["id"=>$getWise->client_id])->first();

                        if($getWise->subcatid == 1) {

                            $timeArr = ["id"=>$getWise->id,"clientid"=>$getWise->client_id,"name"=>$clientGet->fname.' '.$clientGet->lname,"bookNum"=>$getWise->refnum,"bookdate"=>$getWise->bookdate,"booktime"=>date('h:ia',$getWise->booktime)];

                        } elseif($getWise->subcatid == 2) {

                            $timeArr1 = ["id"=>$getWise->id,"clientid"=>$getWise->client_id,"name"=>$clientGet->fname.' '.$clientGet->lname,"bookNum"=>$getWise->refnum,"bookdate"=>$getWise->bookdate,"booktime"=>date('h:ia',$getWise->booktime)];
                        
                        } elseif($getWise->subcatid == 3) {

                            $timeArr2 = ["id"=>$getWise->id,"clientid"=>$getWise->client_id,"name"=>$clientGet->fname.' '.$clientGet->lname,"bookNum"=>$getWise->refnum,"bookdate"=>$getWise->bookdate,"booktime"=>date('h:ia',$getWise->booktime)];
                        
                        } elseif($getWise->subcatid == 4) {
                            
                            $timeArr3 = ["id"=>$getWise->id,"clientid"=>$getWise->client_id,"name"=>$clientGet->fname.' '.$clientGet->lname,"bookNum"=>$getWise->refnum,"bookdate"=>$getWise->bookdate,"booktime"=>date('h:ia',$getWise->booktime)];
                        }
                    }
                }
            } else {
                $timeArr = [];
                $timeArr1 = [];
                $timeArr2 = [];
                $timeArr3 = [];
            }

            $getWises4 = Appointment::select('id','client_id','refnum','bookdate','booktime')->where('booktime',$timeStr)->where('bookdate', $dateS->format('Y-m-d'))->get();
            $timeArr4 = array();
            if($getWises4) {
                
                foreach($getWises4 as $getWise) {
                    $checkBook = Booking::select('catid')->where(["refnum"=>$getWise->refnum])->first();

                    if($checkBook->catid == 1) {
                        $clientGet = Client::select('fname','lname')->where(["id"=>$getWise->client_id])->first();
                        $timeArr4 = ["id"=>$getWise->id,"name"=>$clientGet->fname.' '.$clientGet->lname,"bookNum"=>$getWise->refnum,"bookdate"=>$getWise->bookdate,"booktime"=>date('h:ia',$getWise->booktime)];
                    }
                }
            } else {
                $timeArr4 = [];
            }

            $gettimeWises[] = ['time'=>$timeNew,'data1'=>$timeArr,'data2'=>$timeArr1,'data3'=>$timeArr2,'data4'=>$timeArr3,'data5'=>$timeArr4];

        }

        $view = View::make('admin/calendarblock', compact('gettimeWises'));
        return $contents = $view->render();
    }

    public function appointmentview($id) {
        $clients = array();
        $booking = Booking::where(['id'=>$id])->first();
        $getClient = Client::where(['id'=>$booking->client_id])->first();

        $cats = Categorie::where(["id"=>$booking->catid])->first();
        $subCats = SubCategorie::where(['id'=>$booking->subcatid])->first();
        $subSubCats = SubSubCategorie::where(['id'=>$booking->subsubcatid])->first();
        
        $clients = [
            "id"=>$getClient->id,
            "bookid"=>$booking->id,
            "bookref"=>$booking->refnum,
            "unique"=>$getClient->unique_id,
            "type"=>$getClient->type,
            "fname"=>$getClient->fname,
            "lname"=>$getClient->lname,
            "email"=>$getClient->email,
            "number"=>$getClient->number,
            "source"=>$getClient->lead_source,
            "dob"=>$getClient->dob,
            "address"=>$getClient->address,
            "catid"=>$cats->name,
            "subcatid"=>$subCats->name,
            "subsubcatid"=>$subSubCats->name,
            "bookdate"=>$booking->bookdate,
            "booktime"=>date("h:ia",$booking->booktime),
            "duration"=>$booking->duration,
            "note"=>$booking->note,
            "voucher"=>$booking->voucher,
            "deposite"=>$booking->deposite,
            "addon"=>$booking->addon,
            "total"=>$booking->total,
            "payment_type"=>$booking->payment_type,
            "status"=>$booking->status,
        ];

        $attachArr = array();
        if($getClient->type == 1) {
            $getattachs = Attachment::where(["client_id"=>$getClient->id, "type"=>"client"])->get();
        } else {
            $getattachs = Attachment::where(["client_id"=>$getClient->id, "type"=>"lead"])->get();
        }
        if($getattachs) {
            foreach($getattachs as $getattach) {
                $attachArr[] = ["attach"=>$getattach->attachfile];
            }
        }

        $consultData = array();
        $getConsult = Consultation::where(["book_id"=>$booking->id])->first();
        if($getConsult) {
            $haveConsult = 1;

            $consultData = [
                "date"=>$getConsult->formdate,
                "unique"=>$getClient->unique_id,
                "fname"=>$getClient->fname,
                "lname"=>$getClient->lname,
                "email"=>$getClient->email,
                "number"=>$getClient->number,
                "editorial"=>$getClient->editorial,
                "contemporary"=>$getClient->contemporary,
                "lingerie"=>$getClient->lingerie,
                "photographers"=>$getClient->photographers,
                "outfits"=>$getClient->outfits,
                "permission"=>$getClient->permission,
            ];

        } else {
            $haveConsult = 0;
        }

        return view('admin/appointmentview', compact('clients','attachArr','haveConsult','consultData'));
    }

    public function treatmentview($id) {
        $clients = array();
        $booking = Booking::where(['id'=>$id])->first();
        $getClient = Client::where(['id'=>$booking->client_id])->first();

        $cats = Categorie::where(["id"=>$booking->catid])->first();
        $subCats = SubCategorie::where(['id'=>$booking->subcatid])->first();
        $subSubCats = SubSubCategorie::where(['id'=>$booking->subsubcatid])->first();
        
        $clients = [
            "id"=>$getClient->id,
            "bookid"=>$booking->id,
            "bookref"=>$booking->refnum,
            "unique"=>$getClient->unique_id,
            "type"=>$getClient->type,
            "fname"=>$getClient->fname,
            "lname"=>$getClient->lname,
            "email"=>$getClient->email,
            "number"=>$getClient->number,
            "source"=>$getClient->lead_source,
            "dob"=>$getClient->dob,
            "address"=>$getClient->address,
            "catid"=>$cats->name,
            "subcatid"=>$subCats->name,
            "subsubcatid"=>$subSubCats->name,
            "bookdate"=>$booking->bookdate,
            "booktime"=>date("h:ia",$booking->booktime),
            "duration"=>$booking->duration,
            "note"=>$booking->note,
            "voucher"=>$booking->voucher,
            "deposite"=>$booking->deposite,
            "addon"=>$booking->addon,
            "total"=>$booking->total,
            "payment_type"=>$booking->payment_type,
            "status"=>$booking->status,
        ];

        $attachArr = array();
        if($getClient->type == 1) {
            $getattachs = Attachment::where(["client_id"=>$getClient->id, "type"=>"client"])->get();
        } else {
            $getattachs = Attachment::where(["client_id"=>$getClient->id, "type"=>"lead"])->get();
        }
        if($getattachs) {
            foreach($getattachs as $getattach) {
                $attachArr[] = ["attach"=>$getattach->attachfile];
            }
        }

        return view('admin/treatmentappointment', compact('clients','attachArr'));
    }

    public function arrive($id) {
        $clients = array();
        $booking = Booking::where(['id'=>$id])->first();

        if($booking->catid == 1) {
            Booking::where(["refnum"=>$booking->refnum])->update(["status"=>2]);
            return redirect()->route('appointment-view', $id);
        } else {
            Booking::where(["refnum"=>$booking->refnum])->update(["status"=>2]);
            return redirect()->route('treatment-appointment-view', $id);
        }
    }

    public function viewingbook(Request $request) {

        $checkBook = Booking::where(["refnum"=>$request->ref])->first();

        if($checkBook->catid == 1) {
            Appointment::Create([
                "client_id"=>$request->client,
                "booking_id"=>$request->book,
                "refnum"=>$request->ref,
                "bookdate"=>$request->bookdate,
                "booktime"=>strtotime($request->booktime),
            ]);
            DB::table('bookings')->where(["id"=>$request->book])->update(["status"=>3]);

            $action = "Booking refnum ".$request->ref." is book for viewing";
            LastAction::create([
                "action"=>$action
            ]);

            return redirect()->route('calender');

        } else {
            Appointment::Create([
                "client_id"=>$request->client,
                "booking_id"=>$request->book,
                "refnum"=>$request->ref,
                "bookdate"=>$request->bookdate,
                "booktime"=>strtotime($request->booktime),
            ]);
            DB::table('bookings')->where(["id"=>$request->book])->update(["status"=>3]);

            return redirect()->route('treatment-sale-view', $request->book);
        }
    }

    public function viewstage($id) {
        $clients = array();
        $booking = Booking::where(['refnum'=>$id])->first();
        $appoint = Appointment::where(['refnum'=>$id])->first();
        $getClient = Client::where(['id'=>$booking->client_id])->first();

        $cats = Categorie::where(["id"=>$booking->catid])->first();
        $subCats = SubCategorie::where(['id'=>$booking->subcatid])->first();
        $subSubCats = SubSubCategorie::where(['id'=>$booking->subsubcatid])->first();

        $clients = [
            "id"=>$getClient->id,
            "bookid"=>$booking->id,
            "bookref"=>$booking->refnum,
            "unique"=>$getClient->unique_id,
            "type"=>$getClient->type,
            "fname"=>$getClient->fname,
            "lname"=>$getClient->lname,
            "email"=>$getClient->email,
            "number"=>$getClient->number,
            "source"=>$getClient->lead_source,
            "dob"=>$getClient->dob,
            "address"=>$getClient->address,
            "catid"=>$cats->name,
            "subcatid"=>$subCats->name,
            "subsubcatid"=>$subSubCats->name,
            "bookdate"=>$appoint->bookdate,
            "booktime"=>date("h:ia",$appoint->booktime),
            "duration"=>$booking->duration,
            "note"=>$booking->note,
        ];
        return view('admin/viewingstage', compact('clients'));
    }

    public function order($id, $type) {
        $clients = array();
        $booking = Booking::where(['refnum'=>$id])->first();
        $appoint = Appointment::where(['refnum'=>$id])->first();
        $getClient = Client::where(['id'=>$booking->client_id])->first();

        $clients = [
            "id"=>$getClient->id,
            "bookid"=>$booking->id,
            "bookref"=>$booking->refnum,
            "unique"=>$getClient->unique_id,
            "bookfee"=>$booking->total,
            "bookdeposite"=>$booking->deposite,
            "outstanding"=>$booking->total - $booking->deposite,
        ];
        return view('admin/orderview', compact('clients', 'type'));
    }

    public function ordersave(Request $request, $id) {

        $this->validate($request, [
            "photonum"=>"required",
            "size"=>"required",
            "qty"=>"required",
            "skin"=>"required",
            "hair"=>"required",
            "arms"=>"required",
            "tummy"=>"required",
            "legs"=>"required",
            "tidy"=>"required",
            "packcost"=>"required",
            "bookfee"=>"required",
            "bookrefund"=>"required",
            "paydate"=>"required",
            "extra"=>"required",
            "outstandingamt"=>"required",
        ]);

        if(isset($request->complete)) {
            $status = 1;
        } else {
            $status = 2;
        }

        $booking = Booking::where(['refnum'=>$id])->first();

        $oo = Order::create([
            "client_id"=>$booking->client_id,
            "booking_id"=>$booking->id,
            "refnum"=>$booking->refnum,
            "photonum"=>$request->photonum,
            "size"=>$request->size,
            "qty"=>$request->qty,
            "skin"=>$request->skin,
            "hair"=>$request->hair,
            "arms"=>$request->arms,
            "tummy"=>$request->tummy,
            "legs"=>$request->legs,
            "tidy"=>$request->tidy,
            "packcost"=>$request->packcost,
            "bookfee"=>$request->bookfee,
            "bookfeepay"=>$request->bookfeepay,
            "bookrefund"=>$request->bookrefund,
            "cardpayment"=>$request->cardpayment,
            "cardamt"=>$request->cardamt,
            "paydate"=>$request->paydate,
            "extra"=>$request->extra,
            "outstandingamt"=>$request->outstandingamt,
            "status"=>$status
        ]);

        $action = "Booking refnum ".$booking->refnum." is ordered";
        LastAction::create([
            "action"=>$action
        ]);
        $currentDate = date('d-m-Y');
        
        ToDo::create([
            "refnum"=>$booking->refnum,
            "title"=>"Send Retouching",
            "content"=>"Need to retouching photo of booking id $booking->refnum on $currentDate",
            "type"=>"retouching",
            "status"=>1,
        ]);

        ToDo::create([
            "refnum"=>$booking->refnum,
            "title"=>"Send Photo Print",
            "content"=>"Need to print photo of booking id $booking->refnum on $currentDate",
            "type"=>"print",
            "status"=>1,
        ]);

        ToDo::create([
            "refnum"=>$booking->refnum,
            "title"=>"Send Photo Frames",
            "content"=>"Need to frame photo of booking id $booking->refnum on $currentDate",
            "type"=>"frame",
            "status"=>1,
        ]);

        if($request->buttontype == "purchase") {
            DB::table('bookings')->where(["refnum"=>$booking->refnum])->update(["status"=>7]);
            return redirect()->route('order-sale-view',$oo->id);
        } else {
            DB::table('bookings')->where(["refnum"=>$booking->refnum])->update(["status"=>5]);
            return redirect()->route('photoshoot');
        }
    }

    public function refundview($id) {
        $clients = array();
        $booking = Booking::where(['refnum'=>$id])->first();
        $appoint = Appointment::where(['refnum'=>$id])->first();
        $getClient = Client::where(['id'=>$booking->client_id])->first();

        $clients = [
            "id"=>$getClient->id,
            "bookid"=>$booking->id,
            "bookref"=>$booking->refnum,
            "fname"=>$getClient->fname,
            "lname"=>$getClient->lname,
            "email"=>$getClient->email,
            "number"=>$getClient->number,
            "dob"=>$getClient->dob,
            "address"=>$getClient->address,
            "unique"=>$getClient->unique_id,
            "bookfee"=>$booking->total,
            "bookdeposite"=>$booking->deposite,
            "outstanding"=>$booking->total - $booking->deposite,
        ];
        return view('admin/refundview', compact('clients'));
    }

    public function refundsave(Request $request, $id) {
        $booking = Booking::where(['refnum'=>$id])->first();
        if($booking) {
            $booking->status = 6; // refund
            $booking->save();
        }

        Refund::create([
            "refnum"=>$id,
            "bookfee"=>$request->bookfee,
            "bookrefund"=>$request->bookrefund,
            "outstandingamt"=>$request->outstandingamt,
        ]);

        $action = "Booking refnum ".$booking->refnum." is refunded";
        LastAction::create([
            "action"=>$action
        ]);

        return redirect()->route('calender');
    }

    public function rebook(Request $request, $id) {
        $booking = Booking::where(['id'=>$id])->first();
        if($booking) {
            $booking->bookdate = $request->bookdate;
            $booking->booktime = strtotime($request->booktime);
            $booking->save();
        }
        return redirect()->route('calender');
    }

    public function noshowmail($id) {
        $booking = Booking::where(['id'=>$id])->first();
        $getClient = Client::where(['id'=>$booking->client_id])->first();
        
        $email = $getClient->email;
        $message = "You have missed your Appointment and have lost your booking deposite";

        Mail::to($email)->send(new AppointNoShow($message));
        Session::flash('success', 'Email/SMS send successfully');
        return redirect()->back();
    }

}
