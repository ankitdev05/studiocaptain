<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use Session;
use Auth;
use Carbon\Carbon;
use App\User;
use App\Models\NoticeBoard;
use App\Models\PhoneBoard;
use App\Models\Log;
use App\Models\Note;
use App\Models\Booking;
use App\Models\Client;
use App\Models\Recyclebin;
use App\Models\Target;
use App\Models\ToDo;

class HomeController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $notices = array();
        $getNotices = NoticeBoard::All();
        if($getNotices) {
            foreach($getNotices as $getNotice) {
                $getUser = User::select('name')->where(['id'=>$getNotice->user_id])->first();
                $getNotes = Note::where(["attach_id"=>$getNotice->id, 'type'=>'notice'])->count();
                $notices[] = [
                    "nid"=>$getNotice->id,
                    "title"=>$getNotice->title,
                    "message"=>$getNotice->message,
                    "uname"=>$getUser->name,
                    "date"=> date('d-m-Y H:i', strtotime($getNotice->created_at)),
                    "countNote"=>$getNotes,
                ];
            }
        }

        $phones = array();
        $getPhones = PhoneBoard::All();
        if($getPhones) {
            foreach($getPhones as $getPhone) {
                $getUser = User::select('name')->where(['id'=>$getPhone->user_id])->first();
                $getNotes = Note::where(["attach_id"=>$getPhone->id, 'type'=>'phone'])->count();
                $phones[] = [
                    "pid"=>$getPhone->id,
                    "title"=>$getPhone->reason,
                    "message"=>$getPhone->message,
                    "uname"=>$getUser->name,
                    "date"=> date('d-m-Y H:i', strtotime($getPhone->created_at)),
                    "countNote"=>$getNotes,
                ];
            }
        }

        $dailyTargetArr = array();
        $monthTargetArr = array();
        $yearTargetArr = array();

        $dailyTarget = Target::where(["type"=>1])->whereDate('updated_at', '=', Carbon::today()->toDateString())->first();
        $dailyPercent = array();

        if($dailyTarget) {

            $dailyTargetArr = [
                "photobooking"=>$dailyTarget->photobooking,
                "lead"=>$dailyTarget->lead,
                "convertlead"=>$dailyTarget->convertlead,
                "satisfaction"=>$dailyTarget->satisfaction,
                "binned"=>$dailyTarget->binned,
                "notviewed"=>$dailyTarget->notviewed,
                "phonecall"=>$dailyTarget->phonecall,
                "todo"=>$dailyTarget->todo,
            ];

            $numofBook = Booking::where(["status"=>1])->whereDate('created_at', '=', Carbon::today()->toDateString())->count();
            $a=1;
            $b=1;
            $getLeadClients = Client::where(["type"=>2])->whereDate('created_at', '=', Carbon::today()->toDateString())->get();
            foreach($getLeadClients as $getLeadClient) {
                $getBook = Booking::where(["client_id"=>$getLeadClient->id])->whereDate('created_at', '=', Carbon::today()->toDateString())->first();
                if($getBook) {
                    $b++;
                }
                $a++;
            }
            $numofLead = $a;
            $numofLeadConvert = $b;

            $numofBin = Recyclebin::whereDate('created_at', '=', Carbon::today()->toDateString())->count();
            $numofPhone = PhoneBoard::whereDate('created_at', '=', Carbon::today()->toDateString())->count();
            
            $dailyPercent = [
                "target1"=> ($numofBook / $dailyTarget->photobooking) * 100,
                "target2"=> ($a / $dailyTarget->lead) * 100,
                "target3"=> ($b / $dailyTarget->convertlead) * 100,
                "target4"=> 0,
                "target5"=> ($numofBin / $dailyTarget->binned) * 100,
                "target6"=> 0,
                "target7"=> ($numofPhone / $dailyTarget->phonecall) * 100,
                "target8"=> 0,
            ];
        } else {
            $dailyPercent = [
                "target1"=> 0,
                "target2"=> 0,
                "target3"=> 0,
                "target4"=> 0,
                "target5"=> 0,
                "target6"=> 0,
                "target7"=> 0,
                "target8"=> 0,
            ];

            $dailyTarget =[];
        }

        $monthTarget = Target::where(["type"=>2])->whereMonth('updated_at', Carbon::now()->month)->first();
        $monthPercent = array();
        if($monthTarget) {
            
            $monthTargetArr = [
                "photobooking"=>$monthTarget->photobooking,
                "lead"=>$monthTarget->lead,
                "convertlead"=>$monthTarget->convertlead,
                "satisfaction"=>$monthTarget->satisfaction,
                "binned"=>$monthTarget->binned,
                "notviewed"=>$monthTarget->notviewed,
                "phonecall"=>$monthTarget->phonecall,
                "todo"=>$monthTarget->todo,
            ];

            $numofBook = Booking::where(["status"=>1])->whereMonth('created_at', Carbon::now()->month)->count();
            $a=1;
            $b=1;
            $getLeadClients = Client::where(["type"=>2])->whereMonth('created_at', Carbon::now()->month)->get();
            foreach($getLeadClients as $getLeadClient) {
                $getBook = Booking::where(["client_id"=>$getLeadClient->id])->whereMonth('created_at', Carbon::now()->month)->first();
                if($getBook) {
                    $b++;
                }
                $a++;
            }
            $numofLead = $a;
            $numofLeadConvert = $b;

            $numofBin = Recyclebin::whereMonth('created_at', Carbon::now()->month)->count();
            $numofPhone = PhoneBoard::whereMonth('created_at', Carbon::now()->month)->count();

            $monthPercent = [
                "target1"=> ($numofBook / $monthTarget->photobooking) * 100,
                "target2"=> ($a / $monthTarget->lead) * 100,
                "target3"=> ($b / $monthTarget->convertlead) * 100,
                "target4"=> 0,
                "target5"=> ($numofBin / $monthTarget->binned) * 100,
                "target6"=> 0,
                "target7"=> ($numofPhone / $monthTarget->phonecall) * 100,
                "target8"=> 0,
            ];
        } else {
            $monthPercent = [
                "target1"=> 0,
                "target2"=> 0,
                "target3"=> 0,
                "target4"=> 0,
                "target5"=> 0,
                "target6"=> 0,
                "target7"=> 0,
                "target8"=> 0,
            ];
            $monthTarget=[];
        }

        $yearTarget = Target::where(["type"=>3])->whereYear('updated_at', Carbon::now()->year)->first();
        $yearPercent = array();
        if($yearTarget) {

            $yearTargetArr = [
                "photobooking"=>$yearTarget->photobooking,
                "lead"=>$yearTarget->lead,
                "convertlead"=>$yearTarget->convertlead,
                "satisfaction"=>$yearTarget->satisfaction,
                "binned"=>$yearTarget->binned,
                "notviewed"=>$yearTarget->notviewed,
                "phonecall"=>$yearTarget->phonecall,
                "todo"=>$yearTarget->todo,
            ];
            
            $numofBook = Booking::where(["status"=>1])->whereYear('created_at', Carbon::now()->year)->count();
            $a=1;
            $b=1;
            $getLeadClients = Client::where(["type"=>2])->whereYear('created_at', Carbon::now()->year)->get();
            foreach($getLeadClients as $getLeadClient) {
                $getBook = Booking::where(["client_id"=>$getLeadClient->id])->whereYear('created_at', Carbon::now()->year)->first();
                if($getBook) {
                    $b++;
                }
                $a++;
            }
            $numofLead = $a;
            $numofLeadConvert = $b;

            $numofBin = Recyclebin::whereYear('created_at', Carbon::now()->year)->count();
            $numofPhone = PhoneBoard::whereYear('created_at', Carbon::now()->year)->count();

            $yearPercent = [
                "target1"=> ($numofBook / $yearTarget->photobooking) * 100,
                "target2"=> ($a / $yearTarget->lead) * 100,
                "target3"=> ($b / $yearTarget->convertlead) * 100,
                "target4"=> 0,
                "target5"=> ($numofBin / $yearTarget->binned) * 100,
                "target6"=> 0,
                "target7"=> ($numofPhone / $yearTarget->phonecall) * 100,
                "target8"=> 0,
            ];
        } else {
            $yearPercent = [
                "target1"=> 0,
                "target2"=> 0,
                "target3"=> 0,
                "target4"=> 0,
                "target5"=> 0,
                "target6"=> 0,
                "target7"=> 0,
                "target8"=> 0,
            ];
            $yearTarget=[];
        }

        $todoArrays = array();
        $todoGets = ToDo::where(["status"=>1])->limit(50)->get();
        foreach($todoGets as $todoGet) {
            $book = Booking::select('client_id')->where(["refnum"=>$todoGet->refnum])->first();
            $client = Client::select('fname','lname')->where(["id"=>$book->client_id])->first();
            $todoArrays[] = [
                "title"=>$todoGet->title,
                "content"=>$todoGet->content,
                "created_at"=> date('d-m-Y', strtotime($todoGet->created_at)),
                "name"=> $client->fname.' '.$client->lname,
            ];
        }

        return view('admin/dashboard', compact('notices', 'phones','dailyTargetArr','monthTargetArr','yearTargetArr','dailyPercent','monthPercent','yearPercent','todoArrays'));
    }

    public function myprofile() {
        $myprofile = User::where('id',Auth::user()->id)->first();
        return view('myaccount', compact('myprofile'));
    }

    public function createnotice(Request $request) {
        $this->validate($request, [
            "title"=>"required",
            "message"=>"required",
        ]);

        NoticeBoard::create([
            "user_id"=>Auth::user()->id,
            "title"=>$request->title,
            "message"=>$request->message,
        ]);
        return redirect()->back();
    }

    public function noticecomplete(Request $request, $id) {
        $getNotice = NoticeBoard::where(['id'=>$id])->first();
        Log::create([
            "type"=>"notice",
            "user_id"=>$getNotice->user_id,
            "title"=>$getNotice->title,
            "message"=>$getNotice->message,
        ]);
        NoticeBoard::where(['id'=>$id])->delete();
        return redirect()->back();
    }

    public function noticeview(Request $request, $id) {
        $notices = array();
        $getNotice = NoticeBoard::where(['id'=>$id])->first();
        
        if($getNotice) {
            $getUser = User::select('name')->where(['id'=>$getNotice->user_id])->first();
            $notices = [
                "nid"=>$getNotice->id,
                "title"=>$getNotice->title,
                "message"=>$getNotice->message,
                "uname"=>$getUser->name,
                "date"=> date('d-m-Y H:i', strtotime($getNotice->created_at)),
            ];
            
            $notes = array();
            $getNotes = Note::where(["attach_id"=>$getNotice->id, 'type'=>'notice'])->get();
            foreach($getNotes as $getNote) {
                $getUser = User::select('name')->where(['id'=>$getNote->user_id])->first();
                $notes[] = [
                    "nid"=>$getNote->id,
                    "message"=>$getNote->message,
                    "uname"=>$getUser->name,
                    "date"=> date('d-m-Y H:i', strtotime($getNote->created_at)),
                ];
            }
            return view('admin/notice', compact('notices','notes'));
        } else {
            return redirect()->route('dashboard');
        }
    }

    public function createphone(Request $request) {
        $this->validate($request, [
            "name"=>"required",
            "number"=>"required",
            "reason"=>"required",
            "message"=>"required",
        ]);

        PhoneBoard::create([
            "user_id"=>Auth::user()->id,
            "name"=>$request->name,
            "number"=>$request->number,
            "reason"=>$request->reason,
            "message"=>$request->message,
        ]);

        return redirect()->back();
    }

    public function phonecomplete(Request $request, $id) {
        $getPhone = PhoneBoard::where(['id'=>$id])->first();
        Log::create([
            "type"=>"phone",
            "user_id"=>$getPhone->user_id,
            "name"=>$getPhone->name,
            "number"=>$getPhone->number,
            "reason"=>$getPhone->reason,
            "message"=>$getPhone->message,
        ]);
        PhoneBoard::where(['id'=>$id])->delete();
        return redirect()->back();
    }

    public function phoneview(Request $request, $id) {
        $phones = array();
        $getPhone = PhoneBoard::where(['id'=>$id])->first();
        
        if($getPhone) {
            $getUser = User::select('name')->where(['id'=>$getPhone->user_id])->first();
            $phones = [
                "pid"=>$getPhone->id,
                "name"=>$getPhone->name,
                "number"=>$getPhone->number,
                "reason"=>$getPhone->reason,
                "message"=>$getPhone->message,
                "uname"=>$getUser->name,
                "date"=> date('d-m-Y H:i', strtotime($getPhone->created_at)),
            ];
            
            $notes = array();
            $getNotes = Note::where(["attach_id"=>$getPhone->id, 'type'=>'phone'])->get();
            foreach($getNotes as $getNote) {
                $getUser = User::select('name')->where(['id'=>$getNote->user_id])->first();
                $notes[] = [
                    "nid"=>$getNote->id,
                    "message"=>$getNote->message,
                    "uname"=>$getUser->name,
                    "date"=> date('d-m-Y H:i', strtotime($getNote->created_at)),
                ];
            }
            return view('admin/phone', compact('phones','notes'));
        } else {
            return redirect()->route('dashboard');
        }
    }

    public function addnote(Request $request, $id) {
        $this->validate($request, [
            "message"=>"required",
        ]);

        Note::create([
            "user_id"=>Auth::user()->id,
            "attach_id"=>$id,
            "type"=>$request->type,
            "message"=>$request->message,
        ]);

        return redirect()->back();
    }

    public function noticelogs(Request $request) {
        $notices = array();
        $getLogs = Log::where(['type'=>'notice'])->get();
        if($getLogs) {
            foreach($getLogs as $getLog) {
                $getUser = User::select('name')->where(['id'=>$getLog->user_id])->first();
                $notices[] = [
                    "nid"=>$getLog->id,
                    "title"=>$getLog->title,
                    "message"=>$getLog->message,
                    "uname"=>$getUser->name,
                    "date"=> date('d-m-Y H:i', strtotime($getLog->created_at)),
                ];
            }
        }
        return view('admin/noticelog', compact('notices'));
    }

    public function phonelogs(Request $request) {
        $phones = array();
        $getLogs = Log::where(['type'=>'phone'])->get();
        if($getLogs) {
            foreach($getLogs as $getLog) {
                $getUser = User::select('name')->where(['id'=>$getLog->user_id])->first();
                $phones[] = [
                    "nid"=>$getLog->id,
                    "name"=>$getLog->name,
                    "number"=>$getLog->number,
                    "reason"=>$getLog->reason,
                    "message"=>$getLog->message,
                    "uname"=>$getUser->name,
                    "date"=> date('d-m-Y H:i', strtotime($getLog->created_at)),
                ];
            }
        }
        return view('admin/phonelog', compact('phones'));
    }

    public function targetdaily(Request $request) {
        $dailyTarget = Target::where(["id"=>1])->first();

        if($request->photobooking == 0) {
            $dailyTarget->photobooking = 1;    
        } else {
            $dailyTarget->photobooking = $request->photobooking;
        }
        if($dailyTarget->lead == 0) {
            $dailyTarget->lead = 1;
        } else {
            $dailyTarget->lead = $request->lead;
        }
        if($request->convertlead == 0) {
            $dailyTarget->convertlead = 1;
        } else {
            $dailyTarget->convertlead = $request->convertlead;
        }
        if($request->satisfaction == 0) {
            $dailyTarget->satisfaction = 1;
        } else {
            $dailyTarget->satisfaction = $request->satisfaction;
        }
        if($request->binned == 0) {
            $dailyTarget->binned = 1;
        } else {
            $dailyTarget->binned = $request->binned;
        }
        if($request->notviewed == 0) {
            $dailyTarget->notviewed = 1;
        } else {
            $dailyTarget->notviewed = $request->notviewed;
        }
        if($request->phonecall == 0) {
            $dailyTarget->phonecall = 1;
        } else {
            $dailyTarget->phonecall = $request->phonecall;
        }
        if($request->todo == 0) {
            $dailyTarget->todo = 1;
        } else {
            $dailyTarget->todo = $request->todo;
        }
        $dailyTarget->save();

        return redirect()->back();
    }

    public function targetmonth(Request $request) {
        $dailyTarget = Target::where(["id"=>2])->first();

        if($request->photobooking == 0) {
            $dailyTarget->photobooking = 1;    
        } else {
            $dailyTarget->photobooking = $request->photobooking;
        }
        if($dailyTarget->lead == 0) {
            $dailyTarget->lead = 1;
        } else {
            $dailyTarget->lead = $request->lead;
        }
        if($request->convertlead == 0) {
            $dailyTarget->convertlead = 1;
        } else {
            $dailyTarget->convertlead = $request->convertlead;
        }
        if($request->satisfaction == 0) {
            $dailyTarget->satisfaction = 1;
        } else {
            $dailyTarget->satisfaction = $request->satisfaction;
        }
        if($request->binned == 0) {
            $dailyTarget->binned = 1;
        } else {
            $dailyTarget->binned = $request->binned;
        }
        if($request->notviewed == 0) {
            $dailyTarget->notviewed = 1;
        } else {
            $dailyTarget->notviewed = $request->notviewed;
        }
        if($request->phonecall == 0) {
            $dailyTarget->phonecall = 1;
        } else {
            $dailyTarget->phonecall = $request->phonecall;
        }
        if($request->todo == 0) {
            $dailyTarget->todo = 1;
        } else {
            $dailyTarget->todo = $request->todo;
        }
        $dailyTarget->save();

        return redirect()->back();
    }

    public function targetyear(Request $request) {
        $dailyTarget = Target::where(["id"=>3])->first();

        if($request->photobooking == 0) {
            $dailyTarget->photobooking = 1;    
        } else {
            $dailyTarget->photobooking = $request->photobooking;
        }
        if($dailyTarget->lead == 0) {
            $dailyTarget->lead = 1;
        } else {
            $dailyTarget->lead = $request->lead;
        }
        if($request->convertlead == 0) {
            $dailyTarget->convertlead = 1;
        } else {
            $dailyTarget->convertlead = $request->convertlead;
        }
        if($request->satisfaction == 0) {
            $dailyTarget->satisfaction = 1;
        } else {
            $dailyTarget->satisfaction = $request->satisfaction;
        }
        if($request->binned == 0) {
            $dailyTarget->binned = 1;
        } else {
            $dailyTarget->binned = $request->binned;
        }
        if($request->notviewed == 0) {
            $dailyTarget->notviewed = 1;
        } else {
            $dailyTarget->notviewed = $request->notviewed;
        }
        if($request->phonecall == 0) {
            $dailyTarget->phonecall = 1;
        } else {
            $dailyTarget->phonecall = $request->phonecall;
        }
        if($request->todo == 0) {
            $dailyTarget->todo = 1;
        } else {
            $dailyTarget->todo = $request->todo;
        }
        $dailyTarget->save();

        return redirect()->back();
    }

    public function usersview() {
        $getClients = User::all();

        return view('admin/clientlist', compact('getClients'));
    }

    public function passchange(Request $request) {

        $getUser = User::where(["id"=>$request->clientid])->first();
        $getUser->password = bcrypt($request->pass);
        $getUser->save();

        Session::flash('success', 'Password Changed successfully .');

        return redirect()->back();
    }

}
