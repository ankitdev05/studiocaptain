<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use App\Models\Booking;
use Session;
use Auth;
use App\User;
use App\Models\Client;
use App\Models\Consultation;
use Carbon\Carbon;
use View;
use DB;

class ConsultationController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($id) {
        $clients = array();
        $booking = Booking::where(['id'=>$id])->first();
        $getClient = Client::where(['id'=>$booking->client_id])->first();
        $clients = [
            "id"=>$getClient->id,
            "bookid"=>$booking->id,
            "unique"=>$getClient->unique_id,
            "fname"=>$getClient->fname,
            "lname"=>$getClient->lname,
            "email"=>$getClient->email,
            "number"=>$getClient->number,
            "source"=>$getClient->lead_source,
            "dob"=>$getClient->dob,
            "address"=>$getClient->address,
        ];
        
        return view('admin/consultation', compact('clients'));
    }

    public function save(Request $request) {
        Consultation::create([
            "client_id"=>$request->client,
            "book_id"=>$request->book,
            "formdate"=>$request->formdate,
            "editorial"=>$request->Editorial,
            "contemporary"=>$request->Contemporary,
            "lingerie"=>$request->Lingerie,
            "photographers"=>$request->Photographers,
            "outfits"=>$request->Outfits,
            "permission"=>$request->permission,
        ]);
        return redirect()->route('appointment-view',$request->book);
    }

}
