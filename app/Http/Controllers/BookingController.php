<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use App\Models\Booking;
use Session;
use Auth;
use App\User;
use App\Models\Categorie;
use App\Models\SubCategorie;
use App\Models\SubSubCategorie;
use App\Models\Client;
use App\Models\Attachment;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeEmail;

class BookingController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $cats = Categorie::All();
        return view('admin/booking', compact('cats'));
    }

    public function view($id, $type) {
        
        $clients = array();
        $getClient = Client::where(['id'=>$id])->first();
        $cats = Categorie::All();
        $booking = Booking::where(['client_id'=>$id])->first();
            
        if($booking) {
            $booking = Booking::where(['client_id'=>$id])->first();
            $subCats = SubCategorie::where(['cat_id'=>$booking->catid])->get();
            $subSubCats = SubSubCategorie::where(['subcat_id'=>$booking->subcatid])->get();
            
            $clients = [
                "id"=>$getClient->id,
                "unique"=>$getClient->unique_id,
                "fname"=>$getClient->fname,
                "lname"=>$getClient->lname,
                "email"=>$getClient->email,
                "number"=>$getClient->number,
                "dob"=>$getClient->dob,
                "address"=>$getClient->address,
                "catid"=>$booking->catid,
                "subcatid"=>$booking->subcatid,
                "subsubcatid"=>$booking->subsubcatid,
                "bookdate"=>$booking->bookdate,
                "booktime"=>date('h:ia',$booking->booktime),
                "duration"=>$booking->duration,
                "note"=>$booking->note,
                "voucher"=>$booking->voucher,
                "deposite"=>$booking->deposite,
                "addon"=>$booking->addon,
                "total"=>$booking->total,
                "payment_type"=>$booking->payment_type,
            ];
        } else {
            $clients = [
                "id"=>$getClient->id,
                "unique"=>$getClient->unique_id,
                "fname"=>$getClient->fname,
                "lname"=>$getClient->lname,
                "email"=>$getClient->email,
                "number"=>$getClient->number,
                "dob"=>$getClient->dob,
                "address"=>$getClient->address,
                "catid"=>"",
                "subcatid"=>"",
                "subsubcatid"=>"",
                "bookdate"=>"",
                "booktime"=>"",
                "duration"=>"",
                "note"=>"",
                "voucher"=>"",
                "deposite"=>"",
                "addon"=>"",
                "total"=>"",
                "payment_type"=>"",
            ]; 
        }

        return view('admin/bookingview', compact('clients','cats','subCats','subSubCats','type'));
    }

    public function viewcalendar($date, $time, $id1, $id2) {
        
        $clients = array();
        $cats = Categorie::where("id",$id1)->first();
        $subcats = SubCategorie::where("id",$id2)->first();
        $subsubcats = SubSubCategorie::where("subcat_id",$id2)->get();

        $date = date('Y-m-d',strtotime($date));

        return view('admin/bookingcalendar', compact('date','time','cats','subcats','subsubcats'));
    }

    public function save(Request $request, $id, $type) {
        $this->validate($request, [
            "fname"=>"required",
            "lname"=>"required",
            "email"=>"required|email",
            "number"=>"required",
            "dob"=>"required",
            "address"=>"required",
            "catid"=>"required",
            "subcatid"=>"required",
            "subsubcatid"=>"required",
            "bookdate"=>"required",
            "booktime"=>"required",
            "duration"=>"required",
        ]);

        $getClient = Client::where(['id'=>$id])->first();
        $getClient->fname = $request->fname;
        $getClient->lname = $request->lname;
        $getClient->email = $request->email;
        $getClient->number = $request->number;
        $getClient->dob = $request->dob;
        $getClient->address = $request->address;
        $getClient->save();
        
        $bookingRef = $this->random_strings(4);
        $getBook = Booking::where(["client_id"=>$id])->first();

        if($getBook) {
            $getBook->refnum = "book".$bookingRef;
            $getBook->catid = $request->catid;
            $getBook->subcatid = $request->subcatid;
            $getBook->subsubcatid = $request->subsubcatid;
            $getBook->bookdate = $request->bookdate;
            $getBook->booktime = strtotime($request->booktime);
            $getBook->duration = $request->duration;
            $getBook->note = $request->note;
            $getBook->voucher = $request->voucher;
            $getBook->deposite = $request->deposite;
            $getBook->addon = $request->addon;
            $getBook->total = $request->total;
            $getBook->payment_type = $request->type;
            $getBook->save();
        } else {
            Booking::Create([
                "client_id"=>$id,
                "refnum"=>"book".$bookingRef,
                "catid"=>$request->catid,
                "subcatid"=>$request->subcatid,
                "subsubcatid"=>$request->subsubcatid,
                "bookdate"=>$request->bookdate,
                "booktime"=>strtotime($request->booktime),
                "duration"=>$request->duration,
                "note"=>$request->note,
                "voucher"=>$request->voucher,
                "deposite"=>$request->deposite,
                "addon"=>$request->addon,
                "total"=>$request->total,
                "payment_type"=>$request->type,
            ]);
        }

        if($request->hasFile('fileattach')) {
            $filenameWithExt = $request->file('fileattach')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('fileattach')->getClientOriginalExtension();
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            $path = $request->file('fileattach')->storeAs('public/clientattach', $fileNameToStore);
        } else {
            $fileNameToStore = '';
        }
        $email = $request->email;
        Mail::to($email)->send(new WelcomeEmail());

        if(!empty($fileNameToStore)) {
            Attachment::create([
                "client_id"=>$id,
                "type"=>$type,
                "attachfile"=>$fileNameToStore,
            ]);
        }
        
        if($type == "lead") {
            return redirect()->route('lead-book');
        } else {
            return redirect()->route('client-book');
        }
    }

    public function savebook(Request $request) {
        $this->validate($request, [
            "fname"=>"required",
            "lname"=>"required",
            "email"=>"required|email",
            "number"=>"required",
            "dob"=>"required",
            "address"=>"required",
            "catid"=>"required",
            "subcatid"=>"required",
            "subsubcatid"=>"required",
            "bookdate"=>"required",
            "booktime"=>"required",
            "duration"=>"required",
        ]);

        $createClient = Client::create([
            "user_id"=>Auth::user()->id,
            "unique_id"=> 'c'.$this->random_strings(4),
            "type"=>1,
            "fname"=>$request->fname,
            "lname"=>$request->lname,
            "email"=>$request->email,
            "number"=>$request->number,
            "dob"=>$request->dob,
            "address"=>$request->address,
        ]);
        
        $bookingRef = $this->random_strings(4);
        Booking::Create([
            "client_id"=>$createClient->id,
            "refnum"=>"book".$bookingRef,
            "catid"=>$request->catid,
            "subcatid"=>$request->subcatid,
            "subsubcatid"=>$request->subsubcatid,
            "bookdate"=>$request->bookdate,
            "booktime"=>strtotime($request->booktime),
            "duration"=>$request->duration,
            "note"=>$request->note,
            "voucher"=>$request->voucher,
            "deposite"=>$request->deposite,
            "addon"=>$request->addon,
            "total"=>$request->total,
            "payment_type"=>$request->type,
        ]);

        if($request->hasFile('fileattach')) {
            $filenameWithExt = $request->file('fileattach')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('fileattach')->getClientOriginalExtension();
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            $path = $request->file('fileattach')->storeAs('public/clientattach', $fileNameToStore);
        } else {
            $fileNameToStore = '';
        }

        $email = $request->email;
        Mail::to($email)->send(new WelcomeEmail());

        if(!empty($fileNameToStore)) {
            Attachment::create([
                "client_id"=>$createClient->id,
                "type"=>'client',
                "attachfile"=>$fileNameToStore,
            ]);
        }
        
        return redirect()->back();
    }

    public function savebookcalendar(Request $request) {
        $this->validate($request, [
            "fname"=>"required",
            "lname"=>"required",
            "email"=>"required|email",
            "number"=>"required",
            "dob"=>"required",
            "address"=>"required",
            "catid"=>"required",
            "subcatid"=>"required",
            "subsubcatid"=>"required",
            "bookdate"=>"required",
            "booktime"=>"required",
            "duration"=>"required",
        ]);

        $createClient = Client::create([
            "user_id"=>Auth::user()->id,
            "unique_id"=> 'c'.$this->random_strings(4),
            "type"=>1,
            "fname"=>$request->fname,
            "lname"=>$request->lname,
            "email"=>$request->email,
            "number"=>$request->number,
            "dob"=>$request->dob,
            "address"=>$request->address,
        ]);
        
        $bookingRef = $this->random_strings(4);
        Booking::Create([
            "client_id"=>$createClient->id,
            "refnum"=>"book".$bookingRef,
            "catid"=>$request->catid,
            "subcatid"=>$request->subcatid,
            "subsubcatid"=>$request->subsubcatid,
            "bookdate"=>$request->bookdate,
            "booktime"=>strtotime($request->booktime),
            "duration"=>$request->duration,
            "note"=>$request->note,
            "voucher"=>$request->voucher,
            "deposite"=>$request->deposite,
            "addon"=>$request->addon,
            "total"=>$request->total,
            "payment_type"=>$request->type,
        ]);

        if($request->hasFile('fileattach')) {
            $filenameWithExt = $request->file('fileattach')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('fileattach')->getClientOriginalExtension();
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            $path = $request->file('fileattach')->storeAs('public/clientattach', $fileNameToStore);
        } else {
            $fileNameToStore = '';
        }

        $email = $request->email;
        Mail::to($email)->send(new WelcomeEmail());

        if(!empty($fileNameToStore)) {
            Attachment::create([
                "client_id"=>$createClient->id,
                "type"=>'client',
                "attachfile"=>$fileNameToStore,
            ]);
        }
        
        return redirect()->route('calender');
    }

    function random_strings($length_of_string) {
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle($str_result), 0, $length_of_string);
    }

}
