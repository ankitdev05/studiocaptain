<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use Session;
use Auth;
use App\User;
use App\Models\Client;
use App\Models\Note;
use App\Models\Booking;
use App\Models\Attachment;
use Illuminate\Support\Facades\Mail;
use App\Mail\ClientEmail;
use App\Mail\SendDetail;

class ClientController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $clients = array();
        $getClients = Client::select('fname','lname','unique_id','number','email','dob','address','id')->where(["type"=>1])->orderBy("id","DESC")->get();
        
        foreach($getClients as $getClient) {
            $booking = Booking::select('bookdate')->where(['client_id'=>$getClient->id])->first();
            
            if($booking) {
                $clients[] = [
                    "id"=>$getClient->id,
                    "unique"=>$getClient->unique_id,
                    "fname"=>$getClient->fname,
                    "lname"=>$getClient->lname,
                    "email"=>$getClient->email,
                    "number"=>$getClient->number,
                    "dob"=>$getClient->dob,
                    "address"=>$getClient->address,
                    "bookdate"=>$booking->bookdate,
                ];
            } else {
                $clients[] = [
                    "id"=>$getClient->id,
                    "unique"=>$getClient->unique_id,
                    "fname"=>$getClient->fname,
                    "lname"=>$getClient->lname,
                    "email"=>$getClient->email,
                    "number"=>$getClient->number,
                    "dob"=>$getClient->dob,
                    "address"=>$getClient->address,
                    "bookdate"=>"",
                ];
            }
        }
        return view('admin/clientbook', compact('clients'));
    }

    public function addclient() {
        return view('admin/addclient');
    }

    public function clientcreate(Request $request) {
        $this->validate($request, [
            "fname"=>"required",
            "lname"=>"required",
            "email"=>"required|email",
            "number"=>"required",
            "dob"=>"required",
            "address"=>"required",
        ]);

        if($request->hasFile('fileattach')) {
            $filenameWithExt = $request->file('fileattach')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('fileattach')->getClientOriginalExtension();
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            $path = $request->file('fileattach')->storeAs('public/clientattach', $fileNameToStore);
        } else {
            $fileNameToStore = '';
        }

        $created = Client::create([
            "user_id"=>Auth::user()->id,
            "unique_id"=> 'c'.$this->random_strings(4),
            "type"=>1,
            "fname"=>$request->fname,
            "lname"=>$request->lname,
            "email"=>$request->email,
            "number"=>$request->number,
            "dob"=>$request->dob,
            "address"=>$request->address,
            "note"=>$request->note,
        ]);

        Attachment::create([
            "client_id"=>$created->id,
            "type"=>'client',
            "attachfile"=>$fileNameToStore,
        ]);

        return redirect()->back();
    }

    public function clientview(Request $request) {
        $clients = Client::select('fname','lname','unique_id','number','email','dob','address','id')->where(['id'=>$request->id])->first();
        return response()->json(array('msg'=> $clients), 200);
    }

    public function clientnote(Request $request) {
        $this->validate($request, [
            "message"=>"required",
        ]);

        Note::create([
            "user_id"=>Auth::user()->id,
            "attach_id"=>$request->attach,
            "type"=>$request->type,
            "title"=>$request->title,
            "message"=>$request->message,
        ]);

        return redirect()->back();
    }

    public function clientsingle($id) {
        $clients = array();
        $getClient = Client::where(['id'=>$id])->first();

        $clients = [
            "id"=>$getClient->id,
            "unique"=>$getClient->unique_id,
            "fname"=>$getClient->fname,
            "lname"=>$getClient->lname,
            "email"=>$getClient->email,
            "number"=>$getClient->number,
            "dob"=>$getClient->dob,
            "address"=>$getClient->address,
            "note"=>$getClient->note,
        ];
        $getNotes = Note::where(["attach_id"=>$getClient->id, "type"=>"client"])->get();
        $attachArr = array();
        $getattachs = Attachment::where(["client_id"=>$id, "type"=>"client"])->get();
        if($getattachs) {
            foreach($getattachs as $getattach) {
                $attachArr[] = ["attach"=>$getattach->attachfile];
            }
        }
        return view('admin/clientprofile', compact('clients','getNotes','attachArr'));
    }

    public function clientedit($id) {
        $clients = array();
        $getClient = Client::where(['id'=>$id])->first();

        $clients = [
            "id"=>$getClient->id,
            "unique"=>$getClient->unique_id,
            "fname"=>$getClient->fname,
            "lname"=>$getClient->lname,
            "email"=>$getClient->email,
            "number"=>$getClient->number,
            "dob"=>$getClient->dob,
            "address"=>$getClient->address,
            "note"=>$getClient->note,
        ];

        return view('admin/editclient', compact('clients'));
    }

    public function clientupdate(Request $request, $id) {
        $this->validate($request, [
            "fname"=>"required",
            "lname"=>"required",
            "email"=>"required|email",
            "number"=>"required",
            "dob"=>"required",
            "address"=>"required",
        ]);

        $getClient = Client::where(['id'=>$id])->first();
        $getClient->fname = $request->fname;
        $getClient->lname = $request->lname;
        $getClient->email = $request->email;
        $getClient->number = $request->number;
        $getClient->dob = $request->dob;
        $getClient->address = $request->address;
        $getClient->note = $request->note;
        $getClient->save();

        if($request->hasFile('fileattach')) {
            $filenameWithExt = $request->file('fileattach')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('fileattach')->getClientOriginalExtension();
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            $path = $request->file('fileattach')->storeAs('public/clientattach', $fileNameToStore);

            $findAttach = Attachment::where(["client_id"=>$id, "type"=>"client"])->first();
            if($findAttach) {
                $findAttach->attachfile = $fileNameToStore;
                $findAttach->save();
            } else {
                Attachment::create([
                    "client_id"=>$id,
                    "type"=>'client',
                    "attachfile"=>$fileNameToStore,
                ]);
            }
        }

        return redirect()->back();
    }

    public function uploadFile(Request $request) {

        if ($request->input('submit') != null ) {
    
          $file = $request->file('file');
          // File Details 
          $filename = $file->getClientOriginalName();
          $extension = $file->getClientOriginalExtension();
          $tempPath = $file->getRealPath();
          $fileSize = $file->getSize();
          $mimeType = $file->getMimeType();
    
          // Valid File Extensions
          $valid_extension = array("csv");
    
          // 2MB in Bytes
          $maxFileSize = 2097152; 
    
          // Check file extension
          if(in_array(strtolower($extension),$valid_extension)){
    
            // Check file size
            if($fileSize <= $maxFileSize){
    
              // File upload location
              $location = 'fileuploade';
    
              // Upload file
              $file->move($location,$filename);
    
              // Import CSV to Database
              $filepath = public_path($location."/".$filename);
    
              // Reading file
              $file = fopen($filepath,"r");
    
              $importData_arr = array();
              $i = 0;
    
              while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                 $num = count($filedata );
                 
                 // Skip first row (Remove below comment if you want to skip the first row)
                 if($i == 0){
                    $i++;
                    continue; 
                 }
                 for ($c=0; $c < $num; $c++) {
                    $importData_arr[$i][] = $filedata [$c];
                 }
                 $i++;
              }
              fclose($file);
    
              // Insert to MySQL database
              foreach($importData_arr as $importData){
    
                $insertData = array(
                    "user_id"=>Auth::user()->id,
                    "unique_id"=> 'c'.$this->random_strings(4),
                    "type"=>1,
                    "fname"=>$importData[0],
                    "lname"=>$importData[1],
                    "email"=>$importData[2],
                    "number"=>$importData[3],
                    "dob"=>$importData[4],
                    "address"=>$importData[5],
                    "note"=>$importData[6]
                );
                Client::create($insertData);
              }
              Session::flash('message','Import Successful.');
            }else{
              Session::flash('message','File too large. File must be less than 2MB.');
            }
    
          }else{
             Session::flash('message','Invalid File Extension.');
          }
    
        } else {
            Session::flash('message','Invalid Request');
        }
        // Redirect to index
        return redirect()->action('ClientController@index');
    }

    function random_strings($length_of_string) {
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle($str_result), 0, $length_of_string);
    }

    public function clientemail(Request $request, $id) {
        //$email = $request->to;
        Note::create([
            "user_id"=>Auth::user()->id,
            "attach_id"=>$id,
            "type"=>'client',
            "title"=>'Email/SMS',
            "message"=>$request->message,
        ]);
        $email = "ankit.mittal@mobulous.com";
        Mail::to($email)->send(new ClientEmail($email, $request->message));
        Session::flash('success', 'Email/SMS send successfully');
        return redirect()->back();
    }

    public function clientsenddetail(Request $request, $id) {
        //$email = $request->to;
        $email = "ankit.mittal@mobulous.com";
        Mail::to($email)->send(new SendDetail($id));
        Session::flash('success', 'Email/SMS send successfully');
        return redirect()->back();
    }

}
