<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Client;

class SendDetail extends Mailable
{
    use Queueable, SerializesModels;
    public $clientid;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($clientid)
    {
        $this->clientid = $clientid;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        $getClient = Client::where(["id"=>$this->clientid])->first();
        $clients = [
                    "id"=>$getClient->id,
                    "unique"=>$getClient->unique_id,
                    "fname"=>$getClient->fname,
                    "lname"=>$getClient->lname,
                    "email"=>$getClient->email,
                    "number"=>$getClient->number,
                    "dob"=>$getClient->dob,
                    "address"=>$getClient->address
                ];
        return $this->view('emails.senddetail', compact('clients'));
    }
}
