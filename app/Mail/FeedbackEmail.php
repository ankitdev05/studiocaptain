<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FeedbackEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $saleid;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($saleid)
    {
        $this->saleid = $saleid;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['saleid'] = $this->saleid;
        return $this->view('emails/feedbackemail', compact('data'));
    }
}
