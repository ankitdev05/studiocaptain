<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@index')->name('login');
Route::get('/login', 'Auth\LoginController@index')->name('login');
Route::post('/login-check', 'Auth\LoginController@check')->name('login-check');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::post('/forgot-password', 'Auth\LoginController@forgot')->name('forgot-password');
Route::get('/forgot_change_password/{id}', 'Auth\LoginController@forgotChangePassword')->name('forgot_change_password');

Route::get('/add-user', 'Auth\RegisterController@view')->name('add-user');
Route::post('/create-user', 'Auth\RegisterController@create')->name('create-user');

Route::get('/user-lists', 'HomeController@usersview')->name('user-lists');
Route::post('/passchange', 'HomeController@passchange')->name('passchange');


Route::get('/dashboard', 'HomeController@index')->name('dashboard');
Route::post('/target-daily', 'HomeController@targetdaily')->name('target-daily');
Route::post('/target-month', 'HomeController@targetmonth')->name('target-month');
Route::post('/target-year', 'HomeController@targetyear')->name('target-year');

Route::post('/add-notice', 'HomeController@createnotice')->name('add-notice');
Route::get('/notice-complete/{id}', 'HomeController@noticecomplete')->name('notice-complete');
Route::get('/notice/{id}', 'HomeController@noticeview')->name('notice');
Route::get('/notice-logs', 'HomeController@noticelogs')->name('notice-logs');

Route::post('/add-phone', 'HomeController@createphone')->name('add-phone');
Route::get('/phone-complete/{id}', 'HomeController@phonecomplete')->name('phone-complete');
Route::get('/phone-board/{id}', 'HomeController@phoneview')->name('phone-board');
Route::get('/phone-logs', 'HomeController@phonelogs')->name('phone-logs');

Route::post('/add-note/{id}', 'HomeController@addnote')->name('add-note');

Route::get('/client-book', 'ClientController@index')->name('client-book');
Route::get('/add-client', 'ClientController@addclient')->name('add-client');
Route::post('/upload-client', 'ClientController@uploadFile')->name('upload-client');
Route::post('/client-create', 'ClientController@clientcreate')->name('client-create');
Route::post('/client-view', 'ClientController@clientview')->name('client-view');
Route::post('/client-note', 'ClientController@clientnote')->name('client-note');
Route::get('/client/{id}', 'ClientController@clientsingle')->name('client');
Route::get('/client-edit/{id}', 'ClientController@clientedit')->name('client-edit');
Route::post('/client-update/{id}', 'ClientController@clientupdate')->name('client-update');
Route::post('/client-email/{id}', 'ClientController@clientemail')->name('client-email');
Route::get('/client-send-detail/{id}', 'ClientController@clientsenddetail')->name('client-send-detail');

Route::post('/subcategory', 'CatController@subcategory')->name('subcategory');
Route::post('/subsubcategory', 'CatController@subsubcategory')->name('subsubcategory');

Route::get('/lead-book', 'LeadController@index')->name('lead-book');
Route::get('/add-lead', 'LeadController@addlead')->name('add-lead');
Route::post('/upload-lead', 'LeadController@uploadFile')->name('upload-lead');
Route::post('/lead-create', 'LeadController@leadcreate')->name('lead-create');
Route::post('/lead-note', 'LeadController@leadnote')->name('lead-note');
Route::post('/lead-view', 'LeadController@leadview')->name('lead-view');
Route::get('/lead/{id}', 'LeadController@leadsingle')->name('lead');
Route::get('/lead-edit/{id}', 'LeadController@leadedit')->name('lead-edit');
Route::post('/lead-update/{id}', 'LeadController@leadupdate')->name('lead-update');

Route::get('/bin/{id}', 'LeadController@movetobin')->name('bin');

Route::get('/booking', 'BookingController@index')->name('booking');
Route::get('/booking-view/{id}/{type}', 'BookingController@view')->name('booking-view');
Route::get('/booking-view/{id}/{type}', 'BookingController@view')->name('booking-view');
Route::post('/booking-save/{id}/{type}', 'BookingController@save')->name('booking-save');
Route::post('/bookingsave', 'BookingController@savebook')->name('bookingsave');
Route::post('/bookingsavecalendar', 'BookingController@savebookcalendar')->name('bookingsavecalendar');

Route::get('/booking-calendar/{date}/{time}/{id1}/{id2}', 'BookingController@viewcalendar')->name('booking-calendar');

Route::get('/sale', 'SaleController@index')->name('sale');
Route::get('/sale-view/{id}', 'SaleController@view')->name('sale-view');
Route::post('/salesave', 'SaleController@savesale')->name('salesave');
Route::post('/sale-save/{id}', 'SaleController@save')->name('sale-save');

Route::get('/photoshoot', 'PhotoshootController@index')->name('photoshoot');
Route::get('/past-photoshoot', 'PhotoshootController@pastview')->name('past-photoshoot');
Route::get('/photoshoot-debit', 'PhotoshootController@debitview')->name('photoshoot-debit');
Route::get('/photostatus/{id}', 'PhotoshootController@changestatus')->name('photostatus');

Route::get('/calender', 'CalenderController@index')->name('calender');
Route::post('/calendarmonth', 'CalenderController@calendarmonth')->name('calendarmonth');
Route::post('/calenderday', 'CalenderController@calenderday')->name('calenderday');
Route::post('/calenderblock', 'CalenderController@calenderblock')->name('calenderblock');

Route::get('/appointment-view/{id}', 'CalenderController@appointmentview')->name('appointment-view');
Route::get('/treatment-appointment-view/{id}', 'CalenderController@treatmentview')->name('treatment-appointment-view');

Route::post('/rebooking/{id}', 'CalenderController@rebook')->name('rebooking');

Route::get('/arrive/{id}', 'CalenderController@arrive')->name('arrive');
Route::get('/noshowmail/{id}', 'CalenderController@noshowmail')->name('noshowmail');

Route::post('/viewing-book', 'CalenderController@viewingbook')->name('viewing-book');

Route::get('/consultation/{id}', 'ConsultationController@index')->name('consultation');
Route::post('/consultation-save', 'ConsultationController@save')->name('consultation-save');

Route::get('/viewing/{id}', 'CalenderController@viewstage')->name('viewing');

Route::get('/order/{id}/{type}', 'CalenderController@order')->name('order');
Route::post('/ordersave/{id}', 'CalenderController@ordersave')->name('ordersave');

Route::get('/order-sale-view/{id}', 'SaleController@ordersaleview')->name('order-sale-view');
Route::post('/sale-save-order/{id}', 'SaleController@ordersavesale')->name('sale-save-order');

Route::get('/treatment-sale-view/{id}', 'SaleController@treatmentsaleview')->name('treatment-sale-view');

Route::get('/refund/{id}', 'CalenderController@refundview')->name('refund');
Route::post('/refund-save/{id}', 'CalenderController@refundsave')->name('refund-save');

Route::get('/finance', 'FinanceController@index')->name('finance');
Route::post('/photoshootfilter', 'FinanceController@filterphoto')->name('photoshootfilter');
Route::post('/treatmentfilter', 'FinanceController@filtertreat')->name('treatmentfilter');
Route::post('/directfilter', 'FinanceController@filterdirect')->name('directfilter');

Route::get('/feedback/{id}', 'FeedbackController@index')->name('feedback');
Route::post('/feedback-save/{id}', 'FeedbackController@feedbacksave')->name('feedback-save');
