@extends('layouts.admin')
@section('content')

<div class="MainWrapper">
    <div class="Wrapper">

        <div class="AppointmentArea">
            
            <div class="row">
                <div class="col-sm-8">
                    <div class="AppointmentForm">
                        <h3>Order Refund</h3>
                        <form class="form-horizontal" method="post" action="{{ route('refund-save', $clients['bookref']) }}">
                            @csrf
                            <h4>Order Information</h4> 
                            
                            <div class="form-group">
                                <label class="control-label col-sm-4">First Name</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['fname'] }}" readonly="readonly">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">Last Name</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['lname'] }}" readonly="readonly">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">Email</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['email'] }}" readonly="readonly">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">Date of Birth</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['dob'] }}" readonly="readonly">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">Address</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['address'] }}" readonly="readonly">
                                </div>
                            </div>
                            

                            <div class="form-group">
                                <label class="control-label col-sm-4">Booking Fee (£)</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="bookfee" value="{{ $clients['bookfee'] }}">
                                    @if ($errors->has('bookfee'))
                                        <p class="invalid-feedback" role="alert">
                                            <strong> The Booking Fee field is required. </strong>
                                        </p>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">Booking Fee To Be Refunded (£)</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="bookrefund" value="{{ $clients['bookdeposite'] }}">
                                    @if ($errors->has('bookrefund'))
                                        <p class="invalid-feedback" role="alert">
                                            <strong> The Booking Fee To Be Refunded field is required. </strong>
                                        </p>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">Outstanding Amount (£)</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="outstandingamt" value="{{ $clients['outstanding'] }}">
                                    @if ($errors->has('outstandingamt'))
                                        <p class="invalid-feedback" role="alert">
                                            <strong> The Outstanding Amount field is required. </strong>
                                        </p>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="Buttons"> 
                                    <button type="submit">Save</button> 
                                    <a href="{{ route('viewing', $clients['bookref']) }}">Cancel</a> 
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="col-sm-4">
                </div> 
            </div>

        </div>

    </div>
</div>

@endsection

@section('scripts')

@endsection