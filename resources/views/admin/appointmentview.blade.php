@extends('layouts.admin')
@section('content')

    <div class="MainWrapper">
        <div class="Wrapper">

            <div class="AppointmentArea">
                
                <div class="row">
                    <div class="col-sm-8">
                        <div class="AppointmentForm">
                        @if($clients['status'] == 2)
                            <h3>Calendar Appointment View
                                @if($haveConsult == 1)
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#viewconsult">View Consulation Form</a>
                                @else
                                    <a href="{{ route('consultation', $clients['bookid']) }}">Consulation Form</a>
                                @endif
                            </h3>
                        @endif
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-sm-4">First Name</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" value="{{ $clients['fname'] }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4">Last Name</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" value="{{ $clients['lname'] }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4">Email</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" value="{{ $clients['email'] }}">
                                    </div>
                                </div> 

                                <div class="form-group">
                                    <label class="control-label col-sm-4">Contact Number</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" value="{{ $clients['number'] }}">
                                    </div>
                                </div> 

                                <div class="form-group">
                                    <label class="control-label col-sm-4">Date Of Birth</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" value="{{ $clients['dob'] }}">
                                    </div>
                                </div> 

                                <div class="form-group">
                                    <label class="control-label col-sm-4">Address</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" value="{{ $clients['address'] }}">
                                    </div>
                                </div> 

                                <div class="form-group">
                                    <label class="control-label col-sm-4">Type Of Services</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" value="{{ $clients['catid'] }}">
                                        <input type="text" class="form-control" value="{{ $clients['subcatid'] }}">
                                        <input type="text" class="form-control" value="{{ $clients['subsubcatid'] }}">
                                    </div>
                                </div> 

                                <div class="form-group">
                                    <label class="control-label col-sm-4">Booking Date</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" value="{{ $clients['bookdate'] }}">
                                    </div>
                                </div> 

                                <div class="form-group">
                                    <label class="control-label col-sm-4">Booking Time</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" value="{{ $clients['booktime'] }}">
                                    </div>
                                </div> 

                                <div class="form-group">
                                    <label class="control-label col-sm-4">Booking Duration</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" value="{{ $clients['duration'] }}">
                                    </div>
                                </div> 

                                <div class="form-group">
                                    <label class="control-label col-sm-4">Note</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" value="{{ $clients['note'] }}">
                                    </div>
                                </div> 

                                <div class="form-group">
                                    <div class="Buttons">
                                    @if($clients['status'] == 2)
                                        <a href="JavaScript:Void(0);" data-toggle="modal" data-target="#bookdate">Book View Date</a>
                                        <a href="JavaScript:Void(0);" data-toggle="modal" data-target="#samedate">View Same Day</a>
                                    @elseif($clients['status'] == 1)
                                        <a href="{{ route('arrive', $clients['bookid']) }}">Arrived</a>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#Rebook">Rebook</a>
                                        <a href="{{ route('noshowmail', $clients['bookid']) }}">No Show</a>
                                        <a href="{{ route('sale-view', $clients['bookref']) }}">Sales</a>
                                    @endif
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="CustomerBox">
                            <h3>Customer Info</h3>
                            <article>
                                <!--<figure><img src="images/Profile.png"></figure>-->
                                <h4>{{ $clients['fname'] }} {{ $clients['lname'] }}</h4>
                                <ul>
                                    <li><span>Unique ID</span> {{ $clients['unique'] }}</li>
                                    <li><span>Contact Number</span> {{ $clients['number'] }}<li>
                                    <li><span>Email</span> {{ $clients['email'] }}</li>
                                    <li><span>Lead Source</span> {{ $clients['source'] }}</li>
                                    <li><span>DOB</span> {{ $clients['dob'] }}</li>
                                    <li><span>Address</span> {{ $clients['address'] }}</li> 
                                </ul>

                                <ol>
                                @if(count($attachArr) > 0)
                                    @foreach($attachArr as $attach)
                                        <li><a href="{{ asset("storage/clientattach/".$attach['attach']) }}" target="_blank"><i class="fa fa-file"></i></a></li>
                                    @endforeach
                                @endif
                                </ol>

                            </article>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

<div class="ModalBox">
    <div id="Rebook" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">                    
                    <a href="JavaScript:Void(0);" class="CloseModal" data-dismiss="modal">&times;</a>
                    <div class="Category">
                        <form method="post" action="{{ route('rebooking', $clients['bookid']) }}">
                            @csrf
                            <h3>Notice </h3>
                            <div class="form-group">
                                <label>Booking Date</label>
                                <input type="date" name="bookdate" class="form-control" placeholder="01 Nov, 2019">
                            </div>
                            <div class="form-group">
                                <label>Booking Time</label>
                                <input id="addtime1" type="text" name="booktime" class="form-control ui-timepicker-input" placeholder="11:00 AM" autocomplete="off">
                            </div>
                            <button type="submit">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="bookdate" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                
                <a href="JavaScript:Void(0);" class="CloseModal" data-dismiss="modal">&times;</a>
                <div class="Category">
                    <form method="post" action="{{ route('viewing-book') }}">
                        @csrf
                        <h3>Book Viewing Date </h3>
                        <div class="form-group">
                            <label class="control-label col-sm-4">Booking Date</label>
                            <div class="col-sm-8">
                                <input type="date" name="bookdate" class="form-control" placeholder="01 Nov, 2019">
                            </div>
                        </div> 

                        <div class="form-group">
                            <label class="control-label col-sm-4">Booking Time</label>
                            <div class="col-sm-8">
                                <input id="addtime2" type="text" name="booktime" class="form-control" placeholder="11:00 AM">
                            </div>
                        </div>
                        <input type="hidden" name="client" value="{{ $clients['id'] }}">
                        <input type="hidden" name="book" value="{{ $clients['bookid'] }}">
                        <input type="hidden" name="ref" value="{{ $clients['bookref'] }}">
                        <button type="submit" name="submit" value="submit">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="samedate" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                
                <a href="JavaScript:Void(0);" class="CloseModal" data-dismiss="modal">&times;</a>
                <div class="Category">
                    <form method="post" action="{{ route('viewing-book') }}">
                        @csrf
                        <h3> Book Same day </h3>
                        <div class="form-group">
                            <label class="control-label col-sm-4">Booking Date</label>
                            <div class="col-sm-8">
                                <input type="date" name="bookdate" class="form-control" value="{{ date('Y-m-d') }}" readonly>
                            </div>
                        </div> 

                        <div class="form-group">
                            <label class="control-label col-sm-4">Booking Time</label>
                            <div class="col-sm-8">
                                <input id="addtime3" type="text" name="booktime" class="form-control" placeholder="11:00 AM">
                            </div>
                        </div>
                        <input type="hidden" name="client" value="{{ $clients['id'] }}">
                        <input type="hidden" name="book" value="{{ $clients['bookid'] }}">
                        <input type="hidden" name="ref" value="{{ $clients['bookref'] }}">
                        <button type="submit" name="submit" value="submit">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@if(count($consultData) > 0)
<div id="viewconsult" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                
                <a href="JavaScript:Void(0);" class="CloseModal" data-dismiss="modal">&times;</a>
                <div class="AppointmentForm">
                    <div class="row consultstyle">
                        <label class="control-label col-sm-8">Date</label>
                        <div class="col-sm-4"> {{ $consultData['date'] }} </div>
                    </div>
                    <div class="row consultstyle">
                        <label class="control-label col-sm-8">Customer Ref Number</label>
                        <div class="col-sm-4"> {{ $consultData['unique'] }} </div>
                    </div>
                    <div class="row consultstyle">
                        <label class="control-label col-sm-8">First Name</label>
                        <div class="col-sm-4"> {{ $consultData['fname'] }} </div>
                    </div>
                    <div class="row consultstyle">
                        <label class="control-label col-sm-8">Last Name</label>
                        <div class="col-sm-4"> {{ $consultData['lname'] }} </div>
                    </div>
                    <div class="row consultstyle">
                        <label class="control-label col-sm-8">Mobile Number</label>
                        <div class="col-sm-4"> {{ $consultData['number'] }} </div>
                    </div>
                    <div class="row consultstyle">
                        <label class="control-label col-sm-8">Email Address</label>
                        <div class="col-sm-4"> {{ $consultData['email'] }} </div>
                    </div>
                    <div class="row consultstyle">
                        <label class="control-label col-sm-8">Fashion/Editorial - More posed style of photography with great use of shape and colour.</label>
                        <div class="col-sm-4"> @if($consultData['editorial'] == 1) Yes @else No @endif </div>
                    </div>
                    <div class="row consultstyle">
                        <label class="control-label col-sm-8">Contemporary - More lifestyle shots using white backgrounds with fun and funky poses.</label>
                        <div class="col-sm-4"> @if($consultData['contemporary'] == 1) Yes @else No @endif </div>
                    </div>

                    <div class="row consultstyle">
                        <label class="control-label col-sm-8">Nude/Lingerie - All four photographers are professionals and all areas in our studio are discreet.</label>
                        <div class="col-sm-4"> @if($consultData['lingerie'] == 1) Yes @else No @endif </div>
                    </div>
                    <div class="row consultstyle">
                        <label class="control-label col-sm-8">Photographers Discretions - This sill contain all 3 options to ensure you get a wider range of shots.</label>
                        <div class="col-sm-4"> @if($consultData['photographers'] == 1) Yes @else No @endif </div>
                    </div>

                    <div class="row consultstyle">
                        <label class="control-label col-sm-8">How Many Outfits Do You Have? (Max 4)</label>
                        <div class="col-sm-4"> @if($consultData['outfits'] == 1) Yes @else No @endif </div>
                    </div>
                    <div class="row consultstyle">
                        <label class="control-label col-sm-8">Do you give permission for The Hello Gorgeous Group to use your future promotional material?</label>
                        <div class="col-sm-4"> @if($consultData['permission'] == 1) Yes @else No @endif </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endif

@endsection

@section('scripts')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#addtime1').timepicker({
           datepicker:false,
            formatTime:"h:i a",
            minTime: '9:00 AM',
            maxTime: '9:00 PM',
            step:15
        });  
        $('#addtime2').timepicker({
           datepicker:false,
            formatTime:"h:i a",
            minTime: '9:00 AM',
            maxTime: '9:00 PM',
            step:15
        });
        $('#addtime3').timepicker({
           datepicker:false,
            formatTime:"h:i a",
            minTime: '9:00 AM',
            maxTime: '9:00 PM',
            step:15
        });
    });

    
</script>
<style type="text/css">
.row.consultstyle {
    border-bottom: 1px solid #ccc;
    padding-bottom: 10px;
    padding-top: 10px;
}
</style>
@endsection