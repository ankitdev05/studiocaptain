@extends('layouts.admin')
@section('content')

<div class="MainWrapper">
    <div class="Wrapper">
        <div class="AppointmentArea">
            <div class="row">
                <div class="col-sm-8">
                    <div class="AppointmentForm">
                        <h3>Add Client Info</h3>
                        <form class="form-horizontal" method="post" action="{{ route('client-create') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label class="control-label col-sm-4">First Name</label>
                                <div class="col-sm-8">
                                    <input type="text" name="fname" class="form-control" value="{{ old('fname') }}" placeholder="First Name">
                                    @if ($errors->has('fname'))
                                        <p class="invalid-feedback" role="alert">
                                            <strong> The First name field is required. </strong>
                                        </p>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">Last Name</label>
                                <div class="col-sm-8">
                                    <input type="text" name="lname" class="form-control" value="{{ old('lname') }}" placeholder="Last Name">
                                    @if ($errors->has('lname'))
                                        <p class="invalid-feedback" role="alert">
                                            <strong> The Last name field is required. </strong>
                                        </p>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">Email</label>
                                <div class="col-sm-8">
                                    <input type="email" name="email" class="form-control" value="{{ old('email') }}"placeholder="Email">
                                    @if ($errors->has('email'))
                                        <p class="invalid-feedback" role="alert">
                                            <strong> {{ $errors->first('email') }} </strong>
                                        </p>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">Contact Number</label>
                                <div class="col-sm-8">
                                    <input type="text" name="number" class="form-control" value="{{ old('number') }}"placeholder="Contact Number">
                                    @if ($errors->has('number'))
                                        <p class="invalid-feedback" role="alert">
                                            <strong> The Contact Number field is required. </strong>
                                        </p>
                                    @endif
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Date Of Birth</label>
                                <div class="col-sm-8">
                                    <input type="date" name="dob" class="form-control" value="{{ old('dob') }}" placeholder="01/08/1996">
                                    @if ($errors->has('number'))
                                        <p class="invalid-feedback" role="alert">
                                            <strong> The Date of birth field is required. </strong>
                                        </p>
                                    @endif
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Address</label>
                                <div class="col-sm-8">
                                    <input type="text" name="address" class="form-control" value="{{ old('address') }}" placeholder="Address">
                                    @if ($errors->has('address'))
                                        <p class="invalid-feedback" role="alert">
                                            <strong> {{ $errors->first('address') }} </strong>
                                        </p>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">Note</label>
                                <div class="col-sm-8">
                                    <input type="text" name="note" class="form-control" value="{{ old('note') }}" placeholder="Note">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">Attach File</label>
                                <div class="col-sm-8">
                                    <input type="file" name="fileattach" class="form-control" placeholder="File">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="Buttons">
                                    <button type="submit">Add</button>
                                    <a href="{{ route('client-book') }}">Cancel</a>  
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

@endsection

@section('scripts')

@endsection