@extends('layouts.admin')
@section('content')

    <div class="MainWrapper">
        <div class="Wrapper">

            <div class="Calender_View">
            	<div class="FinanceHead">
            		<ul class="nav nav-tabs">
	                    <li class="active"><a data-toggle="tab" href="#Photoshoot">Photoshoot</a></li>
                        <li><a data-toggle="tab" href="#Treatment">Treatment</a></li>
                        <li><a data-toggle="tab" href="#Direct">Direct Sale</a></li>
	                    <li><a data-toggle="tab" href="#Booking">Booking</a></li>
	                </ul>
	                
	                <div class="clear"></div>
            	</div>
	                

            	<div class="FinanceBody">
	                <div class="tab-content">
	                    <div id="Photoshoot" class="tab-pane fade in active">
	                    	<div class="row">
                                <div class="col-md-3">
                                    <div class="CalenderLeft">
                                        <form class="formaction" id="forma1" method="POST" action="{{ route('photoshootfilter') }}">
                                            <div class="form-group">
                                                <label>From</label>
                                                <input type="date" id="fromdate" name="fromdate" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>To</label>
                                                <input type="date" id="todate" name="todate" class="form-control">
                                            </div>
                                            <input type="hidden" id="type" name="type" value="photo">                                            
                                            <button id="photoapply" type="button">Apply</button>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-md-9" id="updatetable">
                                    <ol class="Finance-Total">
                                        <li>
                                            Total Photos
                                            <span>{{ $totalPhotoA }}</span>
                                        </li>
                                        <li>
                                            Total Costs
                                            <span>$ {{ $totalAmountA }}</span>
                                        </li>
                                    </ol>
    	                            <div class="Client-Book">   
                                        <table id="example1" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>S No</th>
                                                    <th>Name</th> 
                                                    <th>Unique ID</th>
                                                    <th>Booking Ref.no.</th>
                                                    <th>Contact Number</th>
                                                    <th>Email ID</th>
                                                    <th>Product</th>
                                                    <th>Quantity</th>
                                                    <th>Payment Type</th>
                                                    <th>Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if(count($photoDatas) > 0)
                                            @php $x =1; @endphp
                                                @foreach($photoDatas as $photoData)
                                                    <tr>
                                                        <td> {{ $x }} </td>
                                                        <td> {{ $photoData['fname'] }}  {{ $photoData['lname'] }}</td>
                                                        <td> {{ $photoData['unique'] }} </td>
                                                        <td> {{ $photoData['booknum'] }} </td>
                                                        <td> {{ $photoData['number'] }} </td>
                                                        <td> {{ $photoData['email'] }} </td>
                                                        <td> {{ $photoData['product'] }} </td>
                                                        <td> {{ $photoData['quantity'] }} </td>
                                                        <td> {{ $photoData['payment_type'] }} </td>
                                                        <td> {{ $photoData['amount'] }} </td>
                                                    </tr>
                                                    @php $x++; @endphp
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
    	                           </div>
                               </div>
                           </div>
	                    </div>

	                    <div id="Treatment" class="tab-pane fade">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="CalenderLeft">
                                        <form class="formaction" id="forma2" method="POST" action="{{ route('treatmentfilter') }}">
                                            <div class="form-group">
                                                <label>From</label>
                                                <input type="date" id="fromdate1" name="fromdate" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>To</label>
                                                <input type="date" id="todate1" name="todate" class="form-control">
                                            </div>
                                            <input type="hidden" id="type1" name="type" value="photo">
                                            <button id="treatapply" type="button">Apply</button>
                                        </form>
                                    </div>
                                </div>

                                <div class="col-md-9" id="updatetable1">

        	                    	<ol class="Finance-Total">
        			                	
        			                	<li>
        			                		Total Costs
        			                		<span>$ {{ $totalAmountB }}</span>
        			                	</li>
        			                </ol>
        	                        <div class="Client-Book">
                                        <table id="example2" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>S No</th>
                                                    <th>Name</th> 
                                                    <th>Unique ID</th>
                                                    <th>Booking Ref.no.</th>
                                                    <th>Contact Number</th>
                                                    <th>Email ID</th>
                                                    <th>Product</th>
                                                    <th>Quantity</th>
                                                    <th>Payment Type</th>
                                                    <th>Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if(count($treatDatas) > 0)
                                            @php $x =1; @endphp
                                                @foreach($treatDatas as $treatData)
                                                    <tr>
                                                        <td> {{ $x }} </td>
                                                        <td> {{ $treatData['fname'] }}  {{ $treatData['lname'] }}</td>
                                                        <td> {{ $treatData['unique'] }} </td>
                                                        <td> {{ $treatData['booknum'] }} </td>
                                                        <td> {{ $treatData['number'] }} </td>
                                                        <td> {{ $treatData['email'] }} </td>
                                                        <td> {{ $treatData['product'] }} </td>
                                                        <td> {{ $treatData['quantity'] }} </td>
                                                        <td> {{ $treatData['payment_type'] }} </td>
                                                        <td> {{ $treatData['amount'] }} </td>
                                                    </tr>
                                                    @php $x++; @endphp
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
        	                        </div>
                                </div>
                            </div>
	                    </div>

                        <div id="Direct" class="tab-pane fade">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="CalenderLeft">
                                        <form class="formaction" id="forma3" method="POST" action="{{ route('directfilter') }}">
                                            <div class="form-group">
                                                <label>From</label>
                                                <input type="date" id="fromdate2" name="fromdate" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>To</label>
                                                <input type="date" id="todate2" name="todate" class="form-control">
                                            </div>
                                            <input type="hidden" id="type2" name="type" value="photo">
                                            <button id="directapply" type="button">Apply</button>
                                        </form>
                                    </div>
                                </div>

                                <div class="col-md-9" id="updatetable2">

                                    <ol class="Finance-Total">
                                        
                                        <li>
                                            Total Costs
                                            <span>$ {{ $totalAmountC }}</span>
                                        </li>
                                    </ol>
                                    <div class="Client-Book">
                                        <table id="example3" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>S No</th>
                                                    <th>Name</th> 
                                                    <th>Unique ID</th>
                                                    <th>Contact Number</th>
                                                    <th>Email ID</th>
                                                    <th>Product</th>
                                                    <th>Quantity</th>
                                                    <th>Payment Type</th>
                                                    <th>Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if(count($otherDatas) > 0)
                                            @php $x =1; @endphp
                                                @foreach($otherDatas as $otherData)
                                                    <tr>
                                                        <td> {{ $x }} </td>
                                                        <td> {{ $otherData['fname'] }}  {{ $otherData['lname'] }}</td>
                                                        <td> {{ $otherData['unique'] }} </td>
                                                        <td> {{ $otherData['number'] }} </td>
                                                        <td> {{ $otherData['email'] }} </td>
                                                        <td> {{ $otherData['product'] }} </td>
                                                        <td> {{ $otherData['quantity'] }} </td>
                                                        <td> {{ $otherData['payment_type'] }} </td>
                                                        <td> {{ $otherData['amount'] }} </td>
                                                    </tr>
                                                    @php $x++; @endphp
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="Booking" class="tab-pane fade">
                            <div class="row">

                                <div class="col-md-12" id="updatetable2">
                                    <ol class="Finance-Total">
                                        <li>
                                            <a href="javascript:void(0)" onclick="exportexcel()"> Export Excel</a>
                                        </li>
                                    </ol>
                                    <div class="Client-Book">
                                        <table id="example4" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>S No</th>
                                                    <th>Unique</th>
                                                    <th>Name</th> 
                                                    <th>Email</th>
                                                    <th>Contact Number</th>
                                                    <th>DOB</th>
                                                    <th>Address</th>
                                                    <th>Service</th>
                                                    <th>Service-I</th>
                                                    <th>Service-II</th>
                                                    <th>Book Date</th>
                                                    <th>Book Time</th>
                                                    <th>Duration</th>
                                                    <th>Note</th>
                                                    <th>Voucher</th>
                                                    <th>Deposite</th>
                                                    <th>Addon</th>
                                                    <th>Total</th>
                                                    <th>Payment Type</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if(count($bookArr) > 0)
                                            @php $x =1; @endphp
                                                @foreach($bookArr as $bookA)
                                                    <tr>
                                                        <td> {{ $x }} </td>
                                                        <td> {{ $bookA['unique'] }} </td>
                                                        <td> {{ $bookA['fname'] }}  {{ $bookA['lname'] }}</td>
                                                        <td> {{ $bookA['email'] }} </td>
                                                        <td> {{ $bookA['number'] }} </td>
                                                        <td> {{ $bookA['dob'] }} </td>
                                                        <td> {{ $bookA['address'] }} </td>
                                                        <td> {{ $bookA['catid'] }} </td>
                                                        <td> {{ $bookA['subcatid'] }} </td>
                                                        <td> {{ $bookA['subsubcatid'] }} </td>

                                                        <td> {{ $bookA['bookdate'] }} </td>
                                                        <td> {{ $bookA['booktime'] }} </td>
                                                        <td> {{ $bookA['duration'] }} </td>
                                                        <td> {{ $bookA['note'] }} </td>
                                                        <td> {{ $bookA['voucher'] }} </td>
                                                        <td> {{ $bookA['deposite'] }} </td>
                                                        <td> {{ $bookA['addon'] }} </td>
                                                        <td> {{ $bookA['total'] }} </td>
                                                        <td> {{ $bookA['payment_type'] }} </td>
                                                    </tr>
                                                    @php $x++; @endphp
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
	                </div>
	            </div>
            </div>

        </div>
    </div>

@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('#example1').DataTable();
        $('#example2').DataTable();
        $('#example3').DataTable();
        $('#example4').DataTable({
                "lengthMenu": [[10, 25, 50,100, -1], [10, 25, 50,100, "All"]]
        });
    });

    $(document).ready(function() {
        $("#photoapply").on('click', function() {
            var urlform = $('#forma1').attr('action');
            var fromdate = $("#fromdate").val();
            var todate = $("#todate").val();
            var type = $("#type").val();
            $.ajax({
                type:"POST",
                url:urlform,
                data:{fromdate:fromdate,todate:todate,type:type,_token:'{{csrf_token()}}'},
                success:function(response) {
                    $("#updatetable").html(response);
                }
            });
        });
    });

    $(document).ready(function() {
        $("#treatapply").on('click', function() {
            var urlform = $('#forma2').attr('action');
            var fromdate = $("#fromdate1").val();
            var todate = $("#todate1").val();
            var type = $("#type1").val();
            $.ajax({
                type:"POST",
                url:urlform,
                data:{fromdate:fromdate,todate:todate,type:type,_token:'{{csrf_token()}}'},
                success:function(response) {
                    $("#updatetable1").html(response);
                }
            });
        });
    });

    $(document).ready(function() {
        $("#directapply").on('click', function() {
            var urlform = $('#forma3').attr('action');
            var fromdate = $("#fromdate2").val();
            var todate = $("#todate2").val();
            var type = $("#type2").val();
            $.ajax({
                type:"POST",
                url:urlform,
                data:{fromdate:fromdate,todate:todate,type:type,_token:'{{csrf_token()}}'},
                success:function(response) {
                    $("#updatetable2").html(response);
                }
            });
        });
    });
</script>
<script src="https://cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>
<script type="text/javascript">
        function exportexcel() {  

            $("#example4").table2excel({
                filename: "BookingData.xls"
            });
        }  
</script> 
@endsection