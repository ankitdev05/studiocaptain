@extends('layouts.admin')
@section('content')

<div class="MainWrapper">
        <div class="Wrapper">
            <div class="AppointmentArea">
                <div class="row">
                    <div class="col-sm-12"> 
                        <div class="Client-Book">
                            <h3>Users List</h3>
                            
                            @if (Session::has('success'))

                                <div class="alert alert-success" role="alert">
                                   <strong>Success:</strong> {{ Session::get('success') }}
                                </div>

                            @endif

                            <div class="ClientHead">
                                <a href="javascript:void(0)" onclick="exportexcel()"> Export Excel</a>
                            </div>

                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>S No</th>
                                        <th>Name</th>
                                        <th>Email ID</th>
                                        <th>Contact Number</th>
                                        <th>Address</th>
                                        <th>Role</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(count($getClients)>0)
                                @php $x=1; @endphp
                                @foreach($getClients as $client)
                                    <tr>
                                        <td> {{ $x }} </td>
                                        <td> {{ $client['name'] }} </td>
                                        <td>{{ $client['email'] }}</td>
                                        <td>{{ $client['mobile'] }}</td>
                                        <td>{{ $client['address'] }}</td>
                                        <td>
                                            @if($client['role'] == 1)
                                                Super Admin
                                            @elseif($client['role'] == 2)
                                                Admin
                                            @elseif($client['role'] == 3)
                                                Staff
                                            @endif
                                        </td>
                                        <td> 
                                            @if($client['role'] == 1)
                                            @else
                                                <button type="button" class="btn" onclick="callmodel({{$client['id']}} )">Password Change</button> 
                                            @endif
                                        </td>
                                    </tr>
                                    @php $x++; @endphp
                                @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div> 
                    </div>
                    
                </div>

            </div>

        </div>
    </div>

    <div id="Changes" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    
                    <a href="JavaScript:Void(0);" class="CloseModal" data-dismiss="modal">&times;</a>
                    <div class="Category">
                        <form method="post" action="{{ route('passchange') }}">
                            @csrf
                            <h3>Change Password </h3>

                            <div class="form-group">
                                <label>Password</label>
                                <input type="text" name="pass" class="form-control" placeholder="Enter Password">
                            </div>
                            
                            <input type="hidden" id="fetchid" name="clientid" value="">
                            <button type="submit">POST</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    function callmodel($id) {
        $("#fetchid").val($id);
        $("#Changes").modal("show");
    }
</script>
<script src="https://cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>
<script type="text/javascript">
        function exportexcel() {  
            $("#example").table2excel({
                filename: "UserData.xls"
            });
        }  
</script>
<script>
    $(document).ready(function() {
        $('#example').DataTable({
            "lengthMenu": [[10, 25, 50,100, -1], [10, 25, 50,100, "All"]]
        });
    });
</script>
@endsection