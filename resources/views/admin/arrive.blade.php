@extends('layouts.admin')
@section('content')


<div class="MainWrapper">
    <div class="Wrapper">

        <div class="AppointmentArea">
            

            <div class="row">
                <div class="col-sm-8">
                    <div class="AppointmentForm">
                        <h3>Calendar Appointment View <a href="{{ route('consultation', $clients['bookid']) }}">Consulation form</a></h3>
                        <form class="form-horizontal" >

                            <div class="form-group">
                                <label class="control-label col-sm-4">First Name</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['fname'] }}" placeholder="First Name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">Last Name</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['lname'] }}" placeholder="Last Name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">Email</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['email'] }}" placeholder="Email">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Contact Number</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['number'] }}" placeholder="Contact Number">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Date Of Birth</label>
                                <div class="col-sm-8">
                                    <input type="date" class="form-control" value="{{ $clients['dob'] }}" placeholder="01/08/1996">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Address</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['address'] }}" placeholder="Address">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Type Of Services</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['catid'] }}">
                                    <input type="text" class="form-control" value="{{ $clients['subcatid'] }}">
                                    <input type="text" class="form-control" value="{{ $clients['subsubcatid'] }}">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Booking Date</label>
                                <div class="col-sm-8">
                                    <input type="date" class="form-control" value="{{ $clients['bookdate'] }}" placeholder="01 Nov, 2019">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Booking Time</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['booktime'] }}" placeholder="11:00 AM">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Booking Duration</label>
                                <div class="col-sm-8">
                                    <input type="time" class="form-control" value="{{ $clients['duration'] }}" placeholder="Duration">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Note</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['note'] }}" placeholder="Note">
                                </div>
                            </div> 

                            <div class="form-group">
                                <div class="Buttons">
                                    <a href="JavaScript:Void(0);" data-toggle="modal" data-target="#bookdate">Book View Date</a>
                                    <a href="JavaScript:Void(0);" data-toggle="modal" data-target="#samedate">View Same Day</a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="CustomerBox">
                        <h3>Customer Info</h3>
                        <article>
                            <figure><img src="images/Profile.png"></figure>
                            <h4>{{ $clients['fname'] }} {{ $clients['lname'] }}</h4>
                            <ul>
                                <li><span>Unique ID</span> {{ $clients['unique'] }}</li>
                                <li><span>Contact Number</span> {{ $clients['number'] }}<li>
                                <li><span>Email</span> {{ $clients['email'] }}</li>
                                <li><span>Lead Source</span> {{ $clients['source'] }}</li>
                                <li><span>DOB</span> {{ $clients['dob'] }}</li>
                                <li><span>Address</span> {{ $clients['address'] }}</li>
                            </ul>

                            <ol>
                            @if(count($attachArr) > 0)
                                @foreach($attachArr as $attach)
                                    <li><a href="{{ asset("storage/clientattach/".$attach['attach']) }}" target="_blank"><i class="fa fa-file"></i></a></li>
                                @endforeach
                            @endif
                            </ol>

                        </article>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

<div id="bookdate" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                
                <a href="JavaScript:Void(0);" class="CloseModal" data-dismiss="modal">&times;</a>
                <div class="Category">
                    <form method="post" action="{{ route('viewing-book') }}">
                        @csrf
                        <h3>Book Viewing Date </h3>
                        <div class="form-group">
                            <label class="control-label col-sm-4">Booking Date</label>
                            <div class="col-sm-8">
                                <input type="date" name="bookdate" class="form-control" placeholder="01 Nov, 2019">
                            </div>
                        </div> 

                        <div class="form-group">
                            <label class="control-label col-sm-4">Booking Time</label>
                            <div class="col-sm-8">
                                <input id="addtime2" type="text" name="booktime" class="form-control" placeholder="11:00 AM">
                            </div>
                        </div>
                        <input type="hidden" name="client" value="{{ $clients['id'] }}">
                        <input type="hidden" name="book" value="{{ $clients['bookid'] }}">
                        <input type="hidden" name="ref" value="{{ $clients['bookref'] }}">
                        <button type="submit" name="submit" value="submit">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="samedate" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    
                    <a href="JavaScript:Void(0);" class="CloseModal" data-dismiss="modal">&times;</a>
                    <div class="Category">
                        <form method="post" action="{{ route('viewing-book') }}">
                            @csrf
                            <h3> Book Same day </h3>
                            <div class="form-group">
                                <label class="control-label col-sm-4">Booking Date</label>
                                <div class="col-sm-8">
                                    <input type="date" name="bookdate" class="form-control" value="{{ date('Y-m-d') }}" readonly>
                                </div>
                            </div> 
    
                            <div class="form-group">
                                <label class="control-label col-sm-4">Booking Time</label>
                                <div class="col-sm-8">
                                    <input id="addtime1" type="text" name="booktime" class="form-control" placeholder="11:00 AM">
                                </div>
                            </div>
                            <input type="hidden" name="client" value="{{ $clients['id'] }}">
                            <input type="hidden" name="book" value="{{ $clients['bookid'] }}">
                            <input type="hidden" name="ref" value="{{ $clients['bookref'] }}">
                            <button type="submit" name="submit" value="submit">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#addtime1').timepicker({
           datepicker:false,
           formatTime:"h:i a",
           step:1
        });
        $('#addtime2').timepicker({
           datepicker:false,
           formatTime:"h:i a",
           step:1
        });  
    });
</script>
@endsection