@extends('layouts.admin')
@section('content')

<div class="MainWrapper">
        <div class="Wrapper">

            <div class="LogsArea">
                <a href="{{ route('dashboard') }}" class="Back"><i class="fa fa-long-arrow-left"></i> Back</a>
                <h2>Phone Message</h2>

                <div class="Logs-details">
                    <div class="DetailsHead">
                        <figcaption>
                            <h3> {{ $phones['name'] }} </h3>
                            <span><a href="{{ route('phone-complete', $phones['pid']) }}">Complete</a></span>
                        </figcaption>
                        <aside>
                            <p> <strong>Number :</strong>{{ $phones['number'] }}</p>
                            <p> <strong>Reason :</strong>{{ $phones['reason'] }}</p>
                            <p>{{ $phones['message'] }}</p>
                            <h5>{{ $phones['uname'] }} {{ $phones['date'] }}</h5>
                            <span class="Task">{{ count($notes) }}</span>
                        </aside>

                    </div>

                    <div class="DetailsBody">
                        <h3>Notes</h3>
                        <ul class="BoardList">
                        @if(count($notes) > 0 )
                        @foreach($notes as $note)
                            <li>
                                <div class="BoardBox">
                                    <a href="#">
                                        <figure><img src="{{ asset('admin/images/Profile.png') }}"></figure>
                                        <p><strong>{{ $note['uname'] }}, {{ $note['date'] }}</strong> {{ $note['message'] }}</p>
                                    </a>
                                </div>
                            </li>
                        @endforeach
                        @endif
                        </ul>

                        <form method="post" action="{{ route('add-note',$phones['pid']) }}">
                            @csrf
                            <figure><img src="{{ asset('admin/images/Profile.png') }}"></figure>
                            <textarea rows="2" name="message" placeholder="Add Notes ...."></textarea>
                            <input type="hidden" name="type" value="phone">
                            <button type="submit">Post</button>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('scripts')

@endsection