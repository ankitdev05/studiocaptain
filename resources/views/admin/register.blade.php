@extends('layouts.admin')
@section('content')

    <div class="MainWrapper">
        <div class="Wrapper">
            <div class="InnerSignup">
                <div class="SignBox">
                    <h1>signup </h1>
                    @if (Session::has('success'))
                        <div class="alert alert-success" role="alert">
                            <strong>Success:</strong> {{ Session::get('success') }}
                        </div>
                    @endif
                    <form method="post" action="{{ route('create-user') }}">
                        @csrf
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" value="{{ old('name') }}" placeholder="Please Enter Name"> 
                                    <label>Name</label>
                                    <span><i class="fa fa-user"></i></span>
                                    @if ($errors->has('name'))
                                        <p class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </p>
                                    @endif
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="Please Enter Email">
                                    <label>Email</label>
                                    <span><i class="fa fa-envelope" aria-hidden="true"></i></span>
                                    @if ($errors->has('email'))
                                        <p class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </p>
                                    @endif
                                </div>
                            </div> 
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control" placeholder="Please Enter Password">
                                    <label>Password</label>
                                    <span><i class="fa fa-key" aria-hidden="true"></i></span>
                                    @if ($errors->has('password'))
                                        <p class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </p>
                                    @endif
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="mobile" class="form-control" value="{{ old('mobile') }}" placeholder="Please Enter Mobile"> 
                                    <label>Mobile</label>
                                    <span><i class="fa fa-mobile"></i></span>
                                    @if ($errors->has('mobile'))
                                        <p class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('mobile') }}</strong>
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <!-- <textarea rows="5" class="form-control" placeholder="Please Enter Address"></textarea>  -->
                                    <input type="text" name="address" class="form-control" value="{{ old('address') }}" placeholder="Please Enter Address"> 
                                    <label>Address</label>
                                    <span><i class="fa fa-building-o" aria-hidden="true"></i></span>
                                    @if ($errors->has('address'))
                                        <p class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('address') }}</strong>
                                        </p>
                                    @endif
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <select class="form-control" name="role">
                                        @if(Auth::user()->role == 1)
                                            <option value="2" selected="selected"> Admin </option>
                                        @elseif(Auth::user()->role == 2)
                                            <option value="3" selected="selected"> Staff Member </option>
                                        @endif
                                    </select>
                                    <span><i class="fa fa-handshake-o" aria-hidden="true"></i></span>
                                    @if ($errors->has('role'))
                                        <p class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('role') }}</strong>
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit">signup</button>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

@endsection