<ol class="Finance-Total">                        
    <li>
        Total Costs
        <span>$ {{ $totalAmountA }}</span>
    </li>
</ol>
<div class="Client-Book">
    <table id="example22" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>S No</th>
                <th>Name</th> 
                <th>Unique ID</th>
                <th>Booking Ref.no.</th>
                <th>Contact Number</th>
                <th>Email ID</th>
                <th>Product</th>
                <th>Quantity</th>
                <th>Payment Type</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
        @if(count($photoDatas) > 0)
            @foreach($photoDatas as $photoData)
                <tr>
                    <td> </td>
                    <td> {{ $photoData['fname'] }}  {{ $photoData['lname'] }}</td>
                    <td> {{ $photoData['unique'] }} </td>
                    <td> {{ $photoData['booknum'] }} </td>
                    <td> {{ $photoData['number'] }} </td>
                    <td> {{ $photoData['email'] }} </td>
                    <td> {{ $photoData['product'] }} </td>
                    <td> {{ $photoData['quantity'] }} </td>
                    <td> {{ $photoData['payment_type'] }} </td>
                    <td> {{ $photoData['amount'] }} </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>

<script>
    $(document).ready(function() {
        $('#example22').DataTable();
    });
</script>