@extends('layouts.admin')
@section('content')

<div class="MainWrapper">
        <div class="Wrapper">

            <div class="LogsArea">
                <a href="{{ route('dashboard') }}" class="Back"><i class="fa fa-long-arrow-left"></i> Back</a>
                <h2>PHONE MESSAGES LOG</h2>

                <ul class="BoardList">
                @if(count($phones) > 0)
                @foreach($phones as $phone)
                    <li>
                        <div class="BoardBox">
                            <figure><img src="{{ asset('admin/images/Profile.png') }}"></figure>
                            <h3><a href="javascript:void(0)"> {{ $phone['name'] }} </a> </h3>
                            <p> <strong> Number :</strong> {{ $phone['number'] }}</p>
                            <p> <strong> reason :</strong> {{ $phone['reason'] }}</p>
                            <p>{{ $phone['message'] }}</p>
                            <h5>{{ $phone['uname'] }} {{ $phone['date'] }}</h5> 
                        </div>
                    </li>
                @endforeach
                @endif
                </ul>

            </div>

        </div>
    </div>

@endsection

@section('scripts')
@endsection