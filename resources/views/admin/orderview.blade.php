@extends('layouts.admin')
@section('content')

<div class="MainWrapper">
    <div class="Wrapper">

        <div class="AppointmentArea">
            
            <div class="row">
                <div class="col-sm-8">
                    <div class="AppointmentForm">
                        <h3>Order @if($type=="purchase") Purchase @else Deposite @endif</h3>
                        <form class="form-horizontal" method="post" action="{{ route('ordersave', $clients['bookref']) }}">
                            @csrf
                            <input type="hidden" name="buttontype" value="{{ $type }}">
                            <h4>Order Information</h4> 
                            <div class="form-group">
                                <label class="control-label col-sm-4">Photo Number, Size & Quantity</label>
                                <div class="col-sm-8">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name="photonum" placeholder="Photo Number">
                                            @if ($errors->has('photonum'))
                                                <p class="invalid-feedback" role="alert">
                                                    <strong> The Photo Number field is required. </strong>
                                                </p>
                                            @endif
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name="size" placeholder="Size">
                                            @if ($errors->has('size'))
                                                <p class="invalid-feedback" role="alert">
                                                    <strong> The Size field is required. </strong>
                                                </p>
                                            @endif
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name="qty" placeholder="Quantity">
                                            @if ($errors->has('qty'))
                                                <p class="invalid-feedback" role="alert">
                                                    <strong> The Quantity field is required. </strong>
                                                </p>
                                            @endif
                                        </div>
                                    </div>                                                
                                </div>
                            </div>

                            <h4>Retouching Requests</h4>

                            <div class="form-group">
                                <label class="control-label col-sm-4">Smooth Skin</label>
                                <div class="col-sm-8">
                                    <label class="radio-inline"><input type="radio" value="1" name="skin">Yes</label>
                                    <label class="radio-inline"><input type="radio" value="0" name="skin" checked>No</label>
                                </div>                                    
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">Add Hair Volume</label>
                                <div class="col-sm-8">
                                    <label class="radio-inline"><input type="radio" value="1" name="hair">Yes</label>
                                    <label class="radio-inline"><input type="radio" value="0" name="hair" checked>No</label>
                                </div>                                    
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">Liquify Arms</label>
                                <div class="col-sm-8">
                                    <label class="radio-inline"><input type="radio" value="1" name="arms">Yes</label>
                                    <label class="radio-inline"><input type="radio" value="0" name="arms" checked>No</label>
                                </div>                                    
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">Liquify Tummy</label>
                                <div class="col-sm-8">
                                    <label class="radio-inline"><input type="radio" value="1" name="tummy" checked>Yes</label>
                                    <label class="radio-inline"><input type="radio" value="0" name="tummy">No</label>
                                </div>                                    
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">Liquify Legs</label>
                                <div class="col-sm-8">
                                    <label class="radio-inline"><input type="radio" value="1" name="legs" checked>Yes</label>
                                    <label class="radio-inline"><input type="radio" value="0" name="legs">No</label>
                                </div>                                    
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">Tidy Background</label>
                                <div class="col-sm-8">
                                    <label class="radio-inline"><input type="radio" value="1" name="tidy">Yes</label>
                                    <label class="radio-inline"><input type="radio" value="0" name="tidy" checked>No</label>
                                </div>                                    
                            </div>

                            <h4>Payment</h4>

                            <div class="form-group">
                                <label class="control-label col-sm-4">Total Package Cost (£)</label>
                                <div class="col-sm-8">
                                    <label class="radio-inline"><input type="radio" value="1" name="packcost" checked>Yes</label>
                                    <label class="radio-inline"><input type="radio" value="0" name="packcost">No</label>
                                </div>                                    
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">Booking Fee (£)</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="bookfee" value="{{ $clients['bookfee'] }}" required="">
                                    @if ($errors->has('bookfee'))
                                        <p class="invalid-feedback" role="alert">
                                            <strong> The Booking Fee field is required. </strong>
                                        </p>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">Booking Fee To Be Refunded (£)</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="bookrefund" value="{{ $clients['bookdeposite'] }}" required="">
                                    @if ($errors->has('bookrefund'))
                                        <p class="invalid-feedback" role="alert">
                                            <strong> The Booking Fee To Be Refunded field is required. </strong>
                                        </p>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">Payments To Date (£)</label>
                                <div class="col-sm-8">
                                    <label class="radio-inline"><input type="radio" name="paydate" value="1" checked>Yes</label>
                                    <label class="radio-inline"><input type="radio" name="paydate" value="0">No</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">Extra (£)</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="extra" value="" required="">
                                    @if ($errors->has('extra'))
                                        <p class="invalid-feedback" role="alert">
                                            <strong> The Extra Amount field is required. </strong>
                                        </p>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">Outstanding Amount (£)</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="outstandingamt" value="{{ $clients['outstanding'] }}" required="">
                                    @if ($errors->has('outstandingamt'))
                                        <p class="invalid-feedback" role="alert">
                                            <strong> The Outstanding Amount field is required. </strong>
                                        </p>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="Buttons"> 
                                    <button type="submit" name="complete" value="complete">Complete</button>
                                    <button type="submit" name="cancel" value="cancel">Cancel</button> 
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="col-sm-4">
                </div> 
            </div>

        </div>

    </div>
</div>

@endsection

@section('scripts')

@endsection