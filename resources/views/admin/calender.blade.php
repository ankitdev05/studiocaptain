@extends('layouts.admin')
@section('content')


<div class="MainWrapper">
    <div class="Wrapper">

        <div class="Calender_View">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#calendartab1">Calendar</a></li>
                <li><a data-toggle="tab" href="#calendartab2">Last 10 Action</a></li>
                <li><a data-toggle="tab" href="#calendartab3">Appointment book</a></li>
            </ul>

            <div class="tab-content">
                <div id="calendartab1" class="tab-pane fade in  active">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#Today">Today</a></li>
                        <li><a data-toggle="tab" href="#Tommorow">Tommorow</a></li>
                        <li><a data-toggle="tab" href="#Month">This Month</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="Today" class="tab-pane fade in active">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="CalenderLeft">
                                        <div class="CalenderBox">
                                            <div class="responsive-calendar">
                                                <div class="controls">
                                                    <a class="pull-left" data-go="prev">
                                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                                    </a>
                                                    <h4><span data-head-year></span> <span data-head-month></span></h4>
                                                    <a class="pull-right" data-go="next">
                                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                    </a>
                                                </div> 
                                                <div class="day-headers">
                                                    <div class="day header">Mon</div>
                                                    <div class="day header">Tue</div>
                                                    <div class="day header">Wed</div>
                                                    <div class="day header">Thu</div>
                                                    <div class="day header">Fri</div>
                                                    <div class="day header">Sat</div>
                                                    <div class="day header">Sun</div>
                                                </div>
                                                <div class="days" data-group="days">
                                                </div>
                                            </div>
                                            <div class="responsive-calendar-placeholder">
                                            </div>
                                        </div>
                                    </div>
                                </div>
        
                                <div class="col-sm-9">
                                    <div class="Today">
                                        <h2>Appointment List</h2>
                                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>S No</th>
                                                    <th>Name</th> 
                                                    <th>Book ID</th>
                                                    <th>Service Category</th>
                                                    <th>Service SubCategory</th>
                                                    <th>Service Sub-subCategory</th>
                                                    <th>Date</th>
                                                    <th>Time</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody class="tabledatachange">
                                            @if(count($todayArr) > 0)
                                            @php $a=1; @endphp
                                            @foreach($todayArr as $todayA)
                                                <tr>
                                                    <td> {{ $a }} </td>
                                                    <td> {{ $todayA['name'] }} </td>
                                                    <td> {{ $todayA['Booknum'] }} </td>
                                                    <td> {{ $todayA['Category'] }} </td>
                                                    <td> {{ $todayA['SubCategory'] }} </td>
                                                    <td> {{ $todayA['SubsubCategory'] }} </td>
                                                    <td> {{ $todayA['date'] }} </td>
                                                    <td> {{ $todayA['time'] }} </td>
                                                    @if($todayA['Categoryid'] == 1)
                                                    <td> <a href="{{ route('appointment-view',$todayA['id']) }}"> View </a> </td>
                                                    @else
                                                    <td> <a href="{{ route('treatment-appointment-view',$todayA['id']) }}"> View </a> </td>
                                                    @endif
                                                </tr>
                                                @php $a++; @endphp
                                            @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
        
                            </div>
                        </div>

                        <div id="Tommorow" class="tab-pane fade">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="CalenderLeft">
                                        <div class="CalenderBox">
                                            <div class="responsive-calendar">
                                                <div class="controls">
                                                    <a class="pull-left" data-go="prev">
                                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                                    </a>
                                                    <h4><span data-head-year></span> <span data-head-month></span></h4>
                                                    <a class="pull-right" data-go="next">
                                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                    </a>
                                                </div> 
                                                <div class="day-headers">
                                                    <div class="day header">Mon</div>
                                                    <div class="day header">Tue</div>
                                                    <div class="day header">Wed</div>
                                                    <div class="day header">Thu</div>
                                                    <div class="day header">Fri</div>
                                                    <div class="day header">Sat</div>
                                                    <div class="day header">Sun</div>
                                                </div>
                                                <div class="days" data-group="days">
                                                </div>
                                            </div>
                                            <div class="responsive-calendar-placeholder">
                                            </div>
                                        </div>
                                    </div>
                                </div>
        
                                <div class="col-sm-9">
                                    <div class="Today">
                                        <h2>Appointment List</h2>
                                        <table id="exampleone" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>S No</th>
                                                    <th>Name</th> 
                                                    <th>Book ID</th>
                                                    <th>Service Category</th>
                                                    <th>Service SubCategory</th>
                                                    <th>Service Sub-subCategory</th>
                                                    <th>Date</th>
                                                    <th>Time</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody class="tabledatachange">
                                            @if(count($tomArr) > 0)
                                            @php $a=1; @endphp
                                            @foreach($tomArr as $tomA)
                                                <tr>
                                                    <td> {{ $a }} </td>
                                                    <td> {{ $tomA['name'] }} </td>
                                                    <td> {{ $tomA['Booknum'] }} </td>
                                                    <td> {{ $tomA['Category'] }} </td>
                                                    <td> {{ $tomA['SubCategory'] }} </td>
                                                    <td> {{ $tomA['SubsubCategory'] }} </td>
                                                    <td> {{ $tomA['date'] }} </td>
                                                    <td> {{ $tomA['time'] }} </td>
                                                    @if($tomA['Categoryid'] == 1)
                                                    <td> <a href="{{ route('appointment-view',$tomA['id']) }}"> View </a> </td>
                                                    @else
                                                    <td> <a href="{{ route('treatment-appointment-view',$tomA['id']) }}"> View </a> </td>
                                                    @endif
                                                </tr>
                                                @php $a++; @endphp
                                            @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
        
                            </div>
                        </div>

                        <div id="Month" class="tab-pane fade">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="CalenderLeft">
                                        <form class="formaction" method="POST" action="{{ route('calendarmonth') }}">
                                            <div class="form-group">
                                                <label>From</label>
                                                <input type="date" id="fromdate" name="fromdate" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>To</label>
                                                <input type="date" id="todate" name="todate" class="form-control">
                                            </div>
                                            <button id="monthapply" type="button">Apply</button>
                                        </form>
                                    </div>
                                </div>
        
                                <div class="col-sm-9">
                                    <div class="Today">
                                        <h2>Appointment List</h2>
                                        <table id="exampletwo" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>S No</th>
                                                    <th>Name</th> 
                                                    <th>Book ID</th>
                                                    <th>Service Category</th>
                                                    <th>Service SubCategory</th>
                                                    <th>Service Sub-subCategory</th>
                                                    <th>Date</th>
                                                    <th>Time</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="updatetable">
                                            @if(count($todayArr) > 0)
                                            @php $a=1; @endphp
                                            @foreach($todayArr as $todayA)
                                                <tr>
                                                    <td> {{ $a }} </td>
                                                    <td> {{ $todayA['name'] }} </td>
                                                    <td> {{ $todayA['Booknum'] }} </td>
                                                    <td> {{ $todayA['Category'] }} </td>
                                                    <td> {{ $todayA['SubCategory'] }} </td>
                                                    <td> {{ $todayA['SubsubCategory'] }} </td>
                                                    <td> {{ $todayA['date'] }} </td>
                                                    <td> {{ $todayA['time'] }} </td>
                                                    <td> <a href=""> View </a> </td>
                                                </tr>
                                                @php $a++; @endphp
                                            @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
        
                            </div>
                        </div>
                    </div>
                </div>

                <div id="calendartab2" class="tab-pane fade">
                    <ul>
                        @if(count($lastActions) > 0)
                        @foreach($lastActions as $lastAction)
                        <li>
                            <div class="TodoBox"> 
                                <span><i class="fa fa-user-circle-o"></i></span>
                                <p> {{ $lastAction->action }} </p>                               
                            </div>
                        </li>
                        @endforeach
                        @endif
                    </ul>

                    <!-- <ul>
                        <li>
                            <div class="TodoBox">
                            @if(count($lastActions) > 0)
                            @foreach($lastActions as $lastAction)
                                <div style="margin-bottom: 15px;">
                                    <span><i class="fa fa-user-circle-o"></i></span>
                                    <p> {{ $lastAction->action }} </p>
                                </div>
                            @endforeach
                            @endif
                            </div>
                        </li>
                    </ul> -->

                </div>

                <div id="calendartab333" class="tab-pane fade" style="display: none;">
                    <div class="Today">
                        
                        <div class="TodayHead">
                            <ul class="nav nav-tabs">
                            @if(count($gettimeWises) > 0)
                            @php $t=1; @endphp
                            @foreach($gettimeWises as $gettimeWise)
                                @if($t == 1)
                                    <li class="active"><a data-toggle="tab" href="#tab{{ $t }}">{{ $gettimeWise['time'] }}</a></li> 
                                @else
                                    <li><a data-toggle="tab" href="#tab{{ $t }}">{{ $gettimeWise['time'] }}</a></li>
                                @endif
                                @php $t++; @endphp
                            @endforeach
                            @endif
                            </ul>
                        </div>

                        <div class="TodayBody">
                            <div class="tab-content">
                            @if(count($gettimeWises) > 0)
                            @php $t=1; @endphp
                            @foreach($gettimeWises as $gettimeWise)

                                <div id="tab{{ $t }}" class="tab-pane fade @if($t == 1) in active @endif">
                                    <div class="AppoinmentBox">
                                        <article>
                                            <h3>Studio 1</h3>
                                            @php $bookdata1 = $gettimeWise['data1'] @endphp
                                            @if(count($bookdata1) > 0)
                                                <aside>
                                                    <a href="{{ route('appointment-view',$bookdata1['id']) }}">
                                                        <p><span>Name:</span> {{ $bookdata1['name'] }} </p>
                                                        <p><span>Booking Number:</span> {{ $bookdata1['bookNum'] }}</p>
                                                        <p><span>Booking Date:</span> {{ $bookdata1['bookdate'] }}</p>
                                                        <p><span>Time:</span> {{ $bookdata1['booktime'] }}</p>
                                                    </a>
                                                </aside>
                                            @endif
                                        </article>

                                        <article>
                                            <h3>Studio 2</h3>
                                            @php $bookdata2 = $gettimeWise['data2'] @endphp
                                            @if(count($bookdata2) > 0)
                                                <aside>
                                                    <a href="{{ route('appointment-view',$bookdata2['id']) }}">
                                                        <p><span>Name:</span> {{ $bookdata2['name'] }} </p>
                                                        <p><span>Booking Number:</span> {{ $bookdata2['bookNum'] }}</p>
                                                        <p><span>Booking Date:</span> {{ $bookdata2['bookdate'] }}</p>
                                                        <p><span>Time:</span> {{ $bookdata2['booktime'] }}</p>
                                                    </a>
                                                </aside>
                                            @endif
                                        </article>

                                        <article>
                                            <h3>Makeover</h3>
                                            @php $bookdata3 = $gettimeWise['data3'] @endphp
                                            @if(count($bookdata3) > 0)
                                                <aside>
                                                    <a href="{{ route('treatment-appointment-view',$bookdata3['id']) }}">
                                                        <p><span>Name:</span> {{ $bookdata3['name'] }} </p>
                                                        <p><span>Booking Number:</span> {{ $bookdata3['bookNum'] }}</p>
                                                        <p><span>Booking Date:</span> {{ $bookdata3['bookdate'] }}</p>
                                                        <p><span>Time:</span> {{ $bookdata3['booktime'] }}</p>
                                                    </a>
                                                </aside>
                                            @endif
                                        </article>

                                        <article>
                                            <h3>Nails</h3>
                                            @php $bookdata4 = $gettimeWise['data4'] @endphp
                                            @if(count($bookdata4) > 0)
                                                <aside>
                                                    <a href="{{ route('treatment-appointment-view',$bookdata4['id']) }}">
                                                        <p><span>Name:</span> {{ $bookdata4['name'] }} </p>
                                                        <p><span>Booking Number:</span> {{ $bookdata4['bookNum'] }}</p>
                                                        <p><span>Booking Date:</span> {{ $bookdata4['bookdate'] }}</p>
                                                        <p><span>Time:</span> {{ $bookdata4['booktime'] }}</p>
                                                    </a>
                                                </aside>
                                            @endif
                                        </article>

                                        <article>
                                            <h3>Viewing</h3>
                                            @php $bookdata5 = $gettimeWise['data5'] @endphp
                                            @if(count($bookdata5) > 0)
                                                <aside>
                                                    <a href="{{ route('viewing',$bookdata5['bookNum']) }}">
                                                        <p><span>Name:</span> {{ $bookdata5['name'] }} </p>
                                                        <p><span>Booking Number:</span> {{ $bookdata5['bookNum'] }}</p>
                                                        <p><span>Booking Date:</span> {{ $bookdata5['bookdate'] }}</p>
                                                        <p><span>Time:</span> {{ $bookdata5['booktime'] }}</p>
                                                    </a>
                                                </aside>
                                            @endif
                                        </article>

                                    </div>
                                </div>
                            @php $t++; @endphp
                            @endforeach
                            @endif   

                            </div>
                        </div>

                        <div class="clear"></div>

                    </div>
                </div>

                <div id="calendartab3" class="tab-pane fade">
                    <div class="AppoinmentBox" style="display: block;">

                        <div class="col-sm-4 col-md-4">
                            <div class="CalenderLeft">
                                <div class="CalenderBox">
                                    <div class="responsive-calendar">
                                        <div class="controls">
                                            <a class="pull-left" data-go="prev">
                                                <i class="fa fa-angle-left" aria-hidden="true"></i>
                                            </a>
                                            <h4><span data-head-year></span> <span data-head-month></span></h4>
                                            <a class="pull-right" data-go="next">
                                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                            </a>
                                        </div> 
                                        <div class="day-headers">
                                            <div class="day header">Mon</div>
                                            <div class="day header">Tue</div>
                                            <div class="day header">Wed</div>
                                            <div class="day header">Thu</div>
                                            <div class="day header">Fri</div>
                                            <div class="day header">Sat</div>
                                            <div class="day header">Sun</div>
                                        </div>
                                        <div class="days" data-group="days">
                                        </div>
                                    </div>
                                    <div class="responsive-calendar-placeholder">
                                    </div>
                                </div>
                            </div>
                        </div>

                        
                        <div style="clear:both;"></div>


                        <div class="col-sm-12 col-md-12 tableblockchange">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Time</th>
                                        <th>Studio 1</th>
                                        <th>Studio 2</th>
                                        <th>Makeover</th>
                                        <th>Nails</th>
                                        <th>Viewing</th> 
                                    </tr>
                                </thead>

                                <tbody>
                                @foreach($gettimeWises as $gettimeWise)
                                    <tr>
                                        <td> {{ $gettimeWise['time'] }} </td>
                                        <td>
                                        @php $bookdata1 = $gettimeWise['data1'] @endphp
                                        @if(count($bookdata1) > 0)
                                            <aside class="Booking">
                                                <a href="{{ route('appointment-view',$bookdata1['id']) }}">
                                                   <p><span>Name:</span> {{ $bookdata1['name'] }} </p>
                                                   <p><span>Booking Number:</span> {{ $bookdata1['bookNum'] }}</p>
                                                   <p><span>Booking Date:</span> {{ $bookdata1['bookdate'] }}</p>
                                                   <p><span>Time:</span> {{ $bookdata1['booktime'] }}</p> 
                                                </a>
                                            </aside>
                                        @else
                                            <aside class="Booking colorempty">
                                                <a href="javascript:void(0)" class="{{$gettimeWise['time']}}" onclick="gottobooking(this.className,1,1)">  Booking </a>
                                            </aside>
                                        @endif
                                        </td>

                                        <td>
                                        @php $bookdata2 = $gettimeWise['data2'] @endphp
                                        @if(count($bookdata2) > 0)
                                            <aside class="Booking">
                                                <a href="{{ route('appointment-view',$bookdata2['id']) }}">
                                                <p><span>Name:</span> {{ $bookdata2['name'] }} </p>
                                                <p><span>Booking Number:</span> {{ $bookdata2['bookNum'] }}</p>
                                                <p><span>Booking Date:</span> {{ $bookdata2['bookdate'] }}</p>
                                                <p><span>Time:</span> {{ $bookdata2['booktime'] }}</p>
                                                </a>
                                            </aside>
                                        @else
                                            <aside class="Booking colorempty">
                                                <a href="javascript:void(0)" class="{{$gettimeWise['time']}}" onclick="gottobooking(this.className,1,2)">  Booking </a>
                                            </aside>  
                                        @endif
                                        </td>

                                        <td>
                                        @php $bookdata3 = $gettimeWise['data3'] @endphp
                                        @if(count($bookdata3) > 0)
                                            <aside class="Booking">
                                                <a href="{{ route('treatment-appointment-view',$bookdata3['id']) }}">
                                                   <p><span>Name:</span> {{ $bookdata3['name'] }} </p>
                                                    <p><span>Booking Number:</span> {{ $bookdata3['bookNum'] }}</p>
                                                    <p><span>Booking Date:</span> {{ $bookdata3['bookdate'] }}</p>
                                                    <p><span>Time:</span> {{ $bookdata3['booktime'] }}</p>
                                                </a>
                                            </aside>
                                        @else
                                            <aside class="Booking colorempty">
                                                <a href="javascript:void(0)" class="{{$gettimeWise['time']}}" onclick="gottobooking(this.className,2,3)">  Booking </a>
                                            </aside>
                                        @endif
                                        </td>

                                        <td>
                                        @php $bookdata4 = $gettimeWise['data4'] @endphp
                                        @if(count($bookdata4) > 0)
                                            <aside class="Booking">
                                                <a href="{{ route('treatment-appointment-view',$bookdata4['id']) }}">
                                                    <p><span>Name:</span> {{ $bookdata4['name'] }} </p>
                                                    <p><span>Booking Number:</span> {{ $bookdata4['bookNum'] }}</p>
                                                    <p><span>Booking Date:</span> {{ $bookdata4['bookdate'] }}</p>
                                                    <p><span>Time:</span> {{ $bookdata4['booktime'] }}</p>
                                                </a>
                                            </aside>
                                        @else
                                            <aside class="Booking colorempty">
                                                <a href="javascript:void(0)" class="{{$gettimeWise['time']}}" onclick="gottobooking(this.className,2,4)">  Booking </a>
                                            </aside>
                                        @endif
                                        </td>

                                        <td>
                                        @php $bookdata5 = $gettimeWise['data5'] @endphp
                                        @if(count($bookdata5) > 0)
                                            <aside class="Booking">
                                                <a href="{{ route('viewing',$bookdata5['bookNum']) }}">
                                                    <p><span>Name:</span> {{ $bookdata5['name'] }} </p>
                                                    <p><span>Booking Number:</span> {{ $bookdata5['bookNum'] }}</p>
                                                    <p><span>Booking Date:</span> {{ $bookdata5['bookdate'] }}</p>
                                                    <p><span>Time:</span> {{ $bookdata5['booktime'] }}</p>
                                                </a>
                                            </aside>
                                        @endif
                                        </td> 
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>

                    </div>
                </div>

                <input type="hidden" value="{{ date('d-m-Y') }}" id="getselecttime">
            </div>
        </div>

        <div class="clear"></div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('#example').DataTable();
        $('#exampleone').DataTable();
        $('#exampletwo').DataTable();
    });

    $(document).ready(function() {
        $("#monthapply").on('click', function() {
            var urlform = $('.formaction').attr('action');
            var fromdate = $("#fromdate").val();
            var todate = $("#todate").val();
            $.ajax({
                type:"POST",
                url:urlform,
                data:{fromdate:fromdate,todate:todate,_token:'{{csrf_token()}}'},
                success:function(response) {
                    $("#updatetable").html(response);
                }
            });
        });
    });

    function myfunction(day, month, year, selecting) {
        var dayselect = day+'-'+month+'-'+year;

        $(".past > a").css("background-color","#fff");
        $(".past > a").css("color","#000");

        var idSe = "."+selecting;
        $(idSe).css("background-color","#114457");
        $(idSe).css("color","#fff");
        
        $(".today > a").css("background-color","#eee");
        $(".today > a").css("color","#000");

        if($(".nav > li").hasClass('active')) {
            var ahtml = $(".nav > li.active > a").html();
            console.log(ahtml);
        }

        if(ahtml.length == 16) {
            $("#getselecttime").val(dayselect);

            $.ajax({
                type:"POST",
                url:"{{ route('calenderblock') }}",
                data:{dayselect:dayselect,_token:'{{csrf_token()}}'},
                success:function(response) {
                    $(".tableblockchange").html(response);
                }
            });

        } else {
            $.ajax({
                type:"POST",
                url:"{{ route('calenderday') }}",
                data:{dayselect:dayselect,_token:'{{csrf_token()}}'},
                success:function(response) {
                    $(".tabledatachange").html(response);
                }
            });
        }  
    }

    function gottobooking(time,id1,id2) {
        var dayselect = $("#getselecttime").val();
        var createUrl = "{{ url('booking-calendar') }}/"+dayselect+"/"+time+"/"+id1+"/"+id2;
        //console.log(createUrl);
        window.location.href = createUrl;
    }
</script>


@endsection