@extends('layouts.admin')
@section('content')

<div class="MainWrapper">
    <div class="Wrapper">

        <div class="AppointmentArea">
            

            <div class="row">
                <div class="col-sm-8"> 
                    <div class="Client-Book">
                        <h3>Lead Book</h3>
                        <div class="ClientHead">
                            
                            <!--<a id="assignid" href="JavaScript:Void(0);" data-toggle="modal" data-target="#Assign" style="display:none;">Assign</a>-->
                            @if(Auth::user()->role != 3)
                            <a href="JavaScript:Void(0);" data-toggle="modal" data-target="#upload">Upload Lead</a> 
                            @endif
                            <a href="{{ route('add-lead') }}">Add Lead</a>

                            <a href="javascript:void(0)" onclick="exportexcel()"> Export Excel</a>

                        </div>

                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>S No</th>
                                    <th>Name</th> 
                                    <th>Unique ID</th>
                                    <th>Contact Number</th>
                                    <th>Email ID</th>
                                    <th>Lead Source</th>
                                    <th>Asigned Book</th>
                                    <th>DOB</th>
                                    <th>Address</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(count($clients)>0)
                            @php $x=1; @endphp
                            @foreach($clients as $client)
                                <tr>
                                    <td> {{ $x }} </td>
                                    <td><a href="javascript:void(0)" id="{{$client['id']}}" class="clickclient"> {{ $client['fname'] }} {{ $client['lname'] }} </a> </td>
                                    <td> {{ $client['unique'] }} </td>
                                    <td>{{ $client['number'] }}</td>
                                    <td>{{ $client['email'] }}</td>
                                    <td> {{ $client['lead_source'] }} </td>
                                    <td>{{ $client['bookdate'] }}</td>
                                    <td>{{ $client['dob'] }}</td>
                                    <td>{{ $client['address'] }}</td>
                                    <td> <a href="{{ route('lead', $client['id']) }}">View</a> </td>
                                </tr>
                                @php $x++; @endphp
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div> 
                </div>
                <div class="col-sm-4">
                    <div class="CustomerBox">
                        <h3>Lead View</h3>
                        <article>
                            <figure><img src="{{ asset('admin/images/Profile.png') }}"></figure>
                                <h4> <span class="cfname"> </span> <span class="clname"> </span></h4>
                                <ul>
                                    <li><span>Unique ID</span> <span class="cuniqueid"> </span></li>
                                    <li><span>Contact Number</span> <span class="cnumber"> </span><li>
                                    <li><span>Email</span> <span class="cemail"> </span></li>
                                    <li><span>Lead Source</span> Phone Call</li>
                                    <li><span>DOB</span> <span class="cdob"> </span> </li>
                                    <li><span>Address</span> <span class="caddress"> </span> </li> 
                                </ul>
                                <h6 id="shownote" style="display:none;"><a href="JavaScript:Void(0);" data-toggle="modal" data-target="#Notice">Add notes</a></h6>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div id="Notice" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                
                <a href="JavaScript:Void(0);" class="CloseModal" data-dismiss="modal">&times;</a>
                <div class="Category">
                    <form method="post" action="{{ route('lead-note') }}">
                        @csrf
                        <h3>Notice </h3>
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" name="title" class="form-control" placeholder="Enter Title">
                        </div>
                        <div class="form-group">
                            <label>Message</label>
                            <textarea rows="5" name="message" class="form-control" placeholder="Message.."></textarea>
                        </div>
                        <input type="hidden" name="type" value="lead">
                        <input type="hidden" id="cid" name="attach" value="">
                        <button type="submit">POST</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="upload" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                
                <a href="JavaScript:Void(0);" class="CloseModal" data-dismiss="modal">&times;</a>
                <div class="Category">
                    <form method="post" action="{{ route('upload-lead') }}" enctype="multipart/form-data">
                        @csrf
                        <h3>Upload Client </h3>
                        <div class="form-group">
                            <label>File</label>
                            <input type="file" name="file" class="form-control" placeholder="Upload File" accept=".csv">
                        </div>
                        <button type="submit" name="submit" value="submit">Upload</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>
<script type="text/javascript">
        function exportexcel() {  
            $("#example").table2excel({
                filename: "LeadData.xls"
            });
        }  
</script>  

    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                "lengthMenu": [[10, 25, 50,100, -1], [10, 25, 50,100, "All"]]
            });
        });

        $(document).ready(function(){
            $('.clickclient').on('click', function() {
                var id = $(this).attr('id');
                
                $.ajax({
                    type:"POST",
                    url:"{{ route('lead-view') }}",
                    data:{id:id, _token: '{{csrf_token()}}'},
                    success: function(response) {
                        $('.cfname').html(response.msg.fname);
                        $('.clname').html(response.msg.lname);
                        $('.cuniqueid').html(response.msg.unique_id);
                        $('.cnumber').html(response.msg.number);
                        $('.cemail').html(response.msg.email);
                        $('.cdob').html(response.msg.dob);
                        $('.caddress').html(response.msg.address);
                        $('#cid').val(response.msg.id);
                        $('#shownote').css('display','block');

                        $("#assignid").css("display","initial");
                        $("#leadset").val(id);
                    }
                });
            });
        });
    </script>
@endsection