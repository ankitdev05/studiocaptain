@if(count($monthArr) > 0)
    @php $a=1; @endphp
    @foreach($monthArr as $monthA)
        <tr>
            <td> {{ $a }} </td>
            <td> {{ $monthA['name'] }} </td>
            <td> {{ $monthA['Booknum'] }} </td>
            <td> {{ $monthA['Category'] }} </td>
            <td> {{ $monthA['SubCategory'] }} </td>
            <td> {{ $monthA['SubsubCategory'] }} </td>
            <td> {{ $monthA['date'] }} </td>
            <td> {{ $monthA['time'] }} </td>
            @if($monthA['Categoryid'] == 1)
            <td> <a href="{{ route('appointment-view',$monthA['id']) }}"> View </a> </td>
            @else
            <td> <a href="{{ route('treatment-appointment-view',$monthA['id']) }}"> View </a> </td>
            @endif
        </tr>
        @php $a++; @endphp
    @endforeach
@else
    <tr>
        <td>  </td>
        <td>  </td>
        <td>  </td>
        <td>  </td>
        <td>  </td>
        <td>  </td>
        <td>  </td>
        <td>  </td>
        <td>  </td>
    </tr>
@endif