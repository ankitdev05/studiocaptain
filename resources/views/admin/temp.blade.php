@extends('layouts.admin')
@section('content')

<div class="MainWrapper">
    <div class="Wrapper">

        <div class="Calender_View">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#calendartab1">Calendar</a></li>
                <li><a data-toggle="tab" href="#calendartab2">Last 10 Action</a></li>
                <li><a data-toggle="tab" href="#calendartab3">Appointment book</a></li>
            </ul>

            <div class="tab-content">
                <div id="calendartab1" class="tab-pane fade in active">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#Today">Today</a></li>
                        <li><a data-toggle="tab" href="#Tommorow">Tommorow</a></li>
                        <li><a data-toggle="tab" href="#Month">This Month</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="Today" class="tab-pane fade in active">
                            
                        </div>
                    </div>
                </div>
                <div id="calendartab2" class="tab-pane fade">
                </div>
                <div id="calendartab3" class="tab-pane fade">
                </div>
            </div>
        </div>


    </div>
</div>
@endsection

@section('scripts')

@endsection