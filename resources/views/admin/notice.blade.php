@extends('layouts.admin')
@section('content')

<div class="MainWrapper">
    <div class="Wrapper">

        <div class="LogsArea">
            <a href="{{ route('dashboard') }}" class="Back"><i class="fa fa-long-arrow-left"></i> Back</a>
            <h2>Notice Board</h2>

            <div class="Logs-details">
                <div class="DetailsHead">
                    <figcaption>
                    <h3> {{ $notices['title'] }} </h3>
                        <span><a href="{{ route('notice-complete', $notices['nid']) }}"> Complete </a></span>
                    </figcaption>
                    <aside>
                        <h5>{{ $notices['uname'] }} {{ $notices['date'] }}</h5>
                        <p>{{ $notices['message'] }}</p>

                        <span class="Task"> {{ count($notes) }} </span>
                    </aside>
                </div>

                <div class="DetailsBody">
                    <h3>Notes</h3>
                    <ul class="BoardList">
                    @if(count($notes) > 0)
                    @foreach($notes as $note)
                        <li>
                            <div class="BoardBox">
                                <a href="#">
                                    <figure><img src="{{ asset('admin/images/Profile.png')}}"></figure>
                                    <h5>{{ $note['uname'] }} {{ $note['date'] }}</h5>
                                    <p> {{ $note['message'] }}</p>
                                </a>
                            </div>
                        </li>
                    @endforeach
                    @endif
                    </ul>

                    <form method="POST" action="{{ route('add-note', $notices['nid']) }}">
                        @csrf
                        <figure><img src="{{ asset('admin/images/Profile.png') }}"></figure>
                        <textarea rows="2" name="message" placeholder="Add Notes ...."></textarea>
                        <input type="hidden" name="type" value="notice">
                        <button>Post</button>
                    </form>

                </div>

            </div>

        </div>

    </div>
</div>

@endsection

@section('scripts')

@endsection