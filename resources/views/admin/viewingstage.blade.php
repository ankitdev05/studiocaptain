@extends('layouts.admin')
@section('content')

<div class="MainWrapper">
    <div class="Wrapper">

        <div class="AppointmentArea">

            <div class="row">
                <div class="col-sm-8">
                    <div class="AppointmentForm">
                        <h3> Appointment Detail  </h3>
                        <form class="form-horizontal" >

                            <div class="form-group">
                                <label class="control-label col-sm-4">First Name</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['fname'] }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">Last Name</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['lname'] }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">Email</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['email'] }}">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Contact Number</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['number'] }}">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Date Of Birth</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['dob'] }}">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Address</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['address'] }}">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Type Of Services</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['catid'] }}">
                                    <input type="text" class="form-control" value="{{ $clients['subcatid'] }}">
                                    <input type="text" class="form-control" value="{{ $clients['subsubcatid'] }}">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Booking Date</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['bookdate'] }}">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Booking Time</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['booktime'] }}">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Booking Duration</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['duration'] }}">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Note</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['note'] }}">
                                </div>
                            </div> 

                            <div class="form-group">
                                <div class="Buttons">
                                    <a href="{{ route('order', [$clients['bookref'],'purchase']) }}">Purchase</a>
                                    <a href="{{ route('order', [$clients['bookref'],'deposite']) }}">Deposite only</a>
                                    <a href="{{ route('refund', $clients['bookref']) }}">No Purchase</a>
                                    <a href="{{ route('photoshoot') }}">no Show</a> 
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

            </div>

        </div>

    </div>
</div>

@endsection

@section('scripts')

@endsection