@extends('layouts.admin')
@section('content')

<div class="MainWrapper">
        <div class="Wrapper">

            <div class="LogsArea">
                <a href="{{ route('dashboard') }}" class="Back"><i class="fa fa-long-arrow-left"></i> Back</a>
                <h2>Notice Log</h2>

                <ul class="BoardList">
                @if(count($notices) > 0)
                @foreach($notices as $notice)
                    <li>
                        <div class="BoardBox">
                            <figure><img src="{{ asset('admin/images/Profile.png') }}"></figure>
                            <h3><a href="javascript:void(0)"> {{ $notice['title'] }} </a> </h3>
                            <p>{{ $notice['message'] }}</p>
                            <h5>{{ $notice['uname'] }} {{ $notice['date'] }}</h5> 
                        </div>
                    </li>
                @endforeach
                @endif
                </ul>

            </div>

        </div>
    </div>

@endsection

@section('scripts')
@endsection