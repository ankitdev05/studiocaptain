<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Studio Captain Pro | Login</title>

    <link rel="icon" href="{{asset('admin/images/Logos.png')}}" type="image/png">

    <link rel="stylesheet" href="{{asset('admin/css/style.css')}}">  

</head>

<body>
    
    <div class="LoginArea">
        <div class="LoginBox">
            <figure><img src="{{asset('admin/images/Logo.png')}}"></figure>
            <form method="post" action="{{ route('login-check') }}">
                @csrf
                <div class="form-group">
                    <input type="text" name="email" class="form-control" placeholder="Please Enter Email"> 
                    <label>Email ID</label>
                    <span><i class="fa fa-envelope"></i></span>
                </div>
                    @if($errors->has('email'))
                        <p class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('email') }} </strong>
                        </p>
                    @endif

                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Please Enter Password">
                    <label>Password</label>
                    <span><i class="fa fa-key" aria-hidden="true"></i></span>
                </div>
                    @error('password')
                        <p class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </p>
                    @enderror


                <button>Login</button>

               <!--  <p>Don't Have Account Sign Up <a href="#">Here</a></p>
                <h6><a href="JavaScript:Void(0);" data-toggle="modal" data-target="#Forgot">Forgot Password ?</a></h6> -->

            </form>
        </div>
    </div>


    <div id="Forgot" class="modal fade" role="dialog">
        <div class="modal-dialog"> 
            <div class="modal-content"> 
                <div class="modal-body">

                    <div class="ForgotForm">
                        <a class="Close" href="JavaScript:Void(0);" data-dismiss="modal">&times;</a>
                        <form>
                            <figure><img src="{{asset('admin/images/Logo.png')}}"></figure>
                            <div class="form-group">
                                <input type="text" name="" class="form-control" placeholder="Please Enter Email"> 
                                <label>Email ID</label>
                                <span><i class="fa fa-envelope"></i></span>
                            </div>
                            <button>Login</button>
                        <p>Already Have Account Sign in  <a href="{{ route('login') }}">Here</a></p>
                        </form>
                    </div> 
                </div> 
            </div>
        </div>
    </div>

 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="{{asset('admin/js/bootstrap.min.js')}}"></script> 
    <script src="{{asset('admin/js/index.js')}}"></script>

</body>
</html>