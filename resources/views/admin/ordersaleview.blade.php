@extends('layouts.admin')
@section('content')

<div class="MainWrapper">
    <div class="Wrapper">

        <div class="AppointmentArea">
            <div class="row">
                <div class="col-sm-8">
                    <div class="AppointmentForm">
                        <h3>Sale</h3>
                        <form class="form-horizontal" method="POST" action="{{ route('sale-save-order', $clients['refnum']) }}">
                            @csrf
                            <div class="form-group">
                                <label class="control-label col-sm-4">First Name</label>
                                <div class="col-sm-8">
                                    <input type="text" name="fname" value="{{ $clients['fname'] }}" class="form-control" readonly="readonly">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">Last Name</label>
                                <div class="col-sm-8">
                                    <input type="text" name="lname" value="{{ $clients['lname'] }}" class="form-control" readonly="readonly">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">Email</label>
                                <div class="col-sm-8">
                                    <input type="text" name="email" value="{{ $clients['email'] }}" class="form-control" readonly="readonly">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Contact Number</label>
                                <div class="col-sm-8">
                                    <input type="text" name="number" value="{{ $clients['number'] }}" class="form-control" readonly="readonly">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Date Of Birth</label>
                                <div class="col-sm-8">
                                    <input type="date" name="dob" value="{{ $clients['dob'] }}" class="form-control" readonly="readonly">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Address</label>
                                <div class="col-sm-8">
                                    <input type="text" name="address" value="{{ $clients['address'] }}" class="form-control" readonly="readonly">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Product</label>
                                <div class="col-sm-8">
                                    <input type="text" name="product" value="" class="form-control" placeholder="Product name">
                                    @if ($errors->has('product'))
                                        <p class="invalid-feedback" role="alert">
                                            <strong> The product field is required. </strong>
                                        </p>
                                    @endif
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Quantity</label>
                                <div class="col-sm-8">
                                    <input type="number" name="quantity" value="{{ $clients['quantity'] }}" class="form-control" placeholder="Quantity">
                                    @if ($errors->has('quantity'))
                                        <p class="invalid-feedback" role="alert">
                                            <strong> The quantity field is required. </strong>
                                        </p>
                                    @endif
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Payment Type</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="type">
                                        <option value=""> Select Payment Type </option>
                                        <option value="Card" @if($clients['payment_type'] == "Card") selected="selected" @endif> Card </option>
                                        <option value="Cash" @if($clients['payment_type'] == "Cash") selected="selected" @endif> Cash </option>
                                    </select>
                                    @if ($errors->has('type'))
                                    <p class="invalid-feedback" role="alert">
                                        <strong> The Payment Type field is required. </strong>
                                    </p>
                                @endif
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Amount</label>
                                <div class="col-sm-8">
                                    <input type="text" name="amount" value="{{ $clients['amount'] }}" class="form-control" placeholder="Amount">
                                    @if ($errors->has('amount'))
                                    <p class="invalid-feedback" role="alert">
                                        <strong> The Amount field is required. </strong>
                                    </p>
                                @endif
                                </div>
                            </div>                       

                            <div class="form-group">
                                <div class="Buttons"> 
                                    <button type="submit">Completed</button> 
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="CustomerBox">
                    </div>
                </div> 
            </div>

        </div>

    </div>
</div>

@endsection

@section('scripts')
@endsection