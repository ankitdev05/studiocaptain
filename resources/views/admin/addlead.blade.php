@extends('layouts.admin')
@section('content')

<div class="MainWrapper">
    <div class="Wrapper">

        <div class="AppointmentArea">
            <div class="row">
                <div class="col-sm-7">
                    <div class="AppointmentForm">
                        <h3>Add Lead</h3>
                        <form class="form-horizontal" method="post" action="{{ route('lead-create') }}">
                            @csrf
                            <div class="form-group">
                                <label class="control-label col-sm-4">First Name</label>
                                <div class="col-sm-8">
                                    <input type="text" name="fname" class="form-control" placeholder="First Name">
                                </div>
                                @if ($errors->has('fname'))
                                    <p class="invalid-feedback" role="alert">
                                        <strong> The First name field is required. </strong>
                                    </p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">Last Name</label>
                                <div class="col-sm-8">
                                    <input type="text" name="lname" class="form-control" placeholder="Last Name">
                                </div>
                                @if ($errors->has('lname'))
                                    <p class="invalid-feedback" role="alert">
                                        <strong> The Last name field is required. </strong>
                                    </p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">Email</label>
                                <div class="col-sm-8">
                                    <input type="text" name="email" class="form-control" placeholder="Email">
                                </div>
                                @if ($errors->has('email'))
                                    <p class="invalid-feedback" role="alert">
                                        <strong> {{ $errors->first('email') }} </strong>
                                    </p>
                                @endif
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Contact Number</label>
                                <div class="col-sm-8">
                                    <input type="text" name="number" class="form-control" placeholder="Contact Number">
                                </div>
                                @if ($errors->has('number'))
                                    <p class="invalid-feedback" role="alert">
                                        <strong> The Contact Number field is required. </strong>
                                    </p>
                                @endif
                            </div>  

                            <div class="form-group">
                                <label class="control-label col-sm-4">Date Of Birth</label>
                                <div class="col-sm-8">
                                    <input type="date" name="dob" class="form-control" placeholder="01/08/1996">
                                    @if ($errors->has('number'))
                                        <p class="invalid-feedback" role="alert">
                                            <strong> The Date of birth field is required. </strong>
                                        </p>
                                    @endif
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Address</label>
                                <div class="col-sm-8">
                                    <input type="text" name="address" class="form-control" placeholder="Address">
                                    @if ($errors->has('address'))
                                        <p class="invalid-feedback" role="alert">
                                            <strong> {{ $errors->first('address') }} </strong>
                                        </p>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-sm-4">Lead Source</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="source">
                                        <option>Select Reason for Calling</option>
                                        <option value="service2">Type Of Services 2</option>
                                        <option value="service3">Type Of Services 3</option>
                                        <option value="service4">Type Of Services 4</option>
                                        <option value="service5">Type Of Services 5</option>
                                    </select> 
                                </div>
                            </div>  

                            <div class="form-group">
                                <label class="control-label col-sm-4">Note</label>
                                <div class="col-sm-8">
                                    <input type="text" name="note" class="form-control" placeholder="Note">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Attachment</label>
                                <div class="col-sm-8">
                                    <input type="file" name="attachfile" class="form-control" >
                                </div>
                            </div> 

                            <div class="form-group">
                                <div class="Buttons">
                                    <button type="submit">Add</button>
                                    <a href="{{ route('lead-book') }}">Cancel</a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

            </div>

        </div>

    </div>
</div>

@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('#catselect').on('change', function() {
            var id = $(this).val();
            $.ajax({
                type:"POST",
                url:"{{ route('subcategory') }}",
                data:{id:id, _token: '{{csrf_token()}}'},
                success: function(response) {
                    $('#subcatselect').html(response.msg);
                }
            });
        });
    });
    $(document).ready(function() {
        $('#subcatselect').on('change', function() {
            var id = $(this).val();
            $.ajax({
                type:"POST",
                url:"{{ route('subsubcategory') }}",
                data:{id:id, _token: '{{csrf_token()}}'},
                success: function(response) {
                    $('#subsubcatselect').html(response.msg);
                }
            });
        });
    });
</script>
@endsection