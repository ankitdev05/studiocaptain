@extends('layouts.admin')
@section('content')

<div class="MainWrapper">
    <div class="Wrapper">

        <div class="AppointmentArea">
            
            <div class="row">
                <div class="col-sm-8">
                    <div class="AppointmentForm">
                        <h3>Consulation Form </h3>
                        <form class="form-horizontal" method="post" action="{{ route('consultation-save') }}">
                            @csrf
                            <div class="form-group">
                                <label class="control-label col-sm-4">Date</label>
                                <div class="col-sm-8">
                                    <input type="date" name="formdate" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">Customer Ref Number</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['unique'] }}" placeholder="Customer Ref Number">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">First Name</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['fname'] }}" placeholder="First Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">Last Name</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['lname'] }}" placeholder="Last Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">Mobile Number</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['number'] }}" placeholder="Mobile Number">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Email Address</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['email'] }}" placeholder="Email Address">
                                </div>
                            </div>   

                                                            

                            <div class="form-group">
                                <label class="control-label col-sm-8">Fashion/Editorial - More posed style of photography with great use of shape and colour.</label>
                                <div class="col-sm-4">
                                    <label class="radio-inline"><input type="radio" value="1" name="Editorial" checked>Yes</label>
                                    <label class="radio-inline"><input type="radio" value="0" name="Editorial">No</label>
                                </div>                                    
                            </div>                               

                            <div class="form-group">
                                <label class="control-label col-sm-8">Contemporary - More lifestyle shots using white backgrounds with fun and funky poses.</label>
                                <div class="col-sm-4">
                                    <label class="radio-inline"><input type="radio" value="1" name="Contemporary" checked>Yes</label>
                                    <label class="radio-inline"><input type="radio" value="0" name="Contemporary">No</label>
                                </div>                                    
                            </div>                               

                            <div class="form-group">
                                <label class="control-label col-sm-8">Nude/Lingerie - All four photographers are professionals and all areas in our studio are discreet.</label>
                                <div class="col-sm-4">
                                    <label class="radio-inline"><input type="radio" value="1" name="Lingerie">Yes</label>
                                    <label class="radio-inline"><input type="radio" value="0" name="Lingerie" checked>No</label>
                                </div>                                    
                            </div>                               

                            <div class="form-group">
                                <label class="control-label col-sm-8">Photographers Discretions - This sill contain all 3 options to ensure you get a wider range of shots.</label>
                                <div class="col-sm-4">
                                    <label class="radio-inline"><input type="radio" value="1" name="Photographers">Yes</label>
                                    <label class="radio-inline"><input type="radio" value="0" name="Photographers" checked>No</label>
                                </div>                                    
                            </div>                               

                            <div class="form-group">
                                <label class="control-label col-sm-8">How Many Outfits Do You Have? (Max 4)</label>
                                <div class="col-sm-4">
                                    <label class="radio-inline"><input type="radio" value="1" name="Outfits" checked>Yes</label>
                                    <label class="radio-inline"><input type="radio" value="0" name="Outfits">No</label>
                                </div>                                    
                            </div>                               

                            <div class="form-group">
                                <label class="control-label col-sm-8">Do you give permission for The Hello Gorgeous Group to use your future promotional material?</label>
                                <div class="col-sm-4">
                                    <label class="radio-inline"><input type="radio" value="1" name="permission">Yes</label>
                                    <label class="radio-inline"><input type="radio" value="0" name="permission" checked>No</label>
                                </div>                                    
                            </div>                               

                            <div class="form-group">
                                <div class="Buttons">
                                    <button type="submit">Completed</button>
                                    <a href="{{ route('arrive', $clients['bookid']) }}">Cancel</a> 
                                </div>
                            </div>
                            <input type="hidden" name="client" value="{{ $clients['id'] }}">
                            <input type="hidden" name="book" value="{{ $clients['bookid'] }}">
                        </form>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="CustomerBox">
                        <h3>Customer Info</h3>
                        <article>
                            <!--<figure><img src="images/Profile.png"></figure>-->
                            <h4>{{ $clients['fname'] }} {{ $clients['lname'] }}</h4>
                            <ul>
                                <li><span>Unique ID</span> {{ $clients['unique'] }}</li>
                                <li><span>Contact Number</span> {{ $clients['number'] }}<li>
                                <li><span>Email</span> {{ $clients['email'] }}</li>
                                <li><span>Lead Source</span> {{ $clients['source'] }}</li>
                                <li><span>DOB</span> {{ $clients['dob'] }}</li>
                                <li><span>Address</span> {{ $clients['address'] }}</li>
                            </ul>

                        </article>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

@endsection

@section('scripts')

@endsection