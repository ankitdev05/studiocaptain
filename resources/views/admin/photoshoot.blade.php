@extends('layouts.admin')
@section('content')

<div class="MainWrapper">
    <div class="Wrapper">

        <div class="AppointmentArea">
            

            <div class="row">
                <div class="col-sm-12"> 
                    <div class="Client-Book">
                        <h3>Photoshoot <a href="{{ route('past-photoshoot') }}">Past Photoshoot</a></h3>
                        
                        <div class="ClientHead">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#calendartab1">Retouching</a></li>
                                <li><a data-toggle="tab" href="#calendartab2">Print</a></li>
                                <li><a data-toggle="tab" href="#calendartab3">Frames</a></li>
                                <li><a href="{{ route('photoshoot-debit') }}">Debit</a></li>
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div id="calendartab1" class="tab-pane fade in active">
                                <table id="example" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>S No</th>
                                            <th>Booking Ref No.</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Contact Number</th>
                                            <th>Email ID</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($bookArr)>0)
                                    @php $x=1; @endphp
                                    @foreach($bookArr as $book)
                                        <tr>
                                            <td> {{ $x }} </td>
                                            <td> {{ $book['ref_num'] }} </td>
                                            <td>{{ $book['fname'] }}</td>
                                            <td>{{ $book['lname'] }}</td>
                                            <td>{{ $book['number'] }}</td>
                                            <td>{{ $book['email'] }}</td>
                                            <td>@if($book['pay_status'] == 1) <a href="{{ route('photostatus', $book['id']) }}"> Incomplete </a> @else Complete @endif</td>
                                        </tr>
                                        @php $x++; @endphp
                                    @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>

                            <div id="calendartab2" class="tab-pane fade">
                                <table id="example1" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>S No</th>
                                            <th>Booking Ref No.</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Contact Number</th>
                                            <th>Email ID</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($bookArr1)>0)
                                    @php $x=1; @endphp
                                    @foreach($bookArr1 as $book1)
                                        <tr>
                                            <td> {{ $x }} </td>
                                            <td> {{ $book1['ref_num'] }} </td>
                                            <td>{{ $book1['fname'] }}</td>
                                            <td>{{ $book1['lname'] }}</td>
                                            <td>{{ $book1['number'] }}</td>
                                            <td>{{ $book1['email'] }}</td>
                                            <td>@if($book1['pay_status'] == 1) <a href="{{ route('photostatus', $book1['id']) }}"> Incomplete </a> @else Complete @endif</td>
                                        </tr>
                                        @php $x++; @endphp
                                    @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>

                            <div id="calendartab3" class="tab-pane fade">
                                <table id="example2" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>S No</th>
                                            <th>Booking Ref No.</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Contact Number</th>
                                            <th>Email ID</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($bookArr2)>0)
                                    @php $x=1; @endphp
                                    @foreach($bookArr2 as $book2)
                                        <tr>
                                            <td> {{ $x }} </td>
                                            <td> {{ $book2['ref_num'] }} </td>
                                            <td>{{ $book2['fname'] }}</td>
                                            <td>{{ $book2['lname'] }}</td>
                                            <td>{{ $book2['number'] }}</td>
                                            <td>{{ $book2['email'] }}</td>
                                            <td>@if($book2['pay_status'] == 1) <a href="{{ route('photostatus', $book2['id']) }}"> Incomplete </a> @else Complete @endif</td>
                                        </tr>
                                        @php $x++; @endphp
                                    @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div> 
                </div>
            </div>

        </div>

    </div>
</div>

@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('#example').DataTable();
        $('#example1').DataTable();
        $('#example2').DataTable();
    });
</script>
@endsection