@extends('layouts.admin')
@section('content')

<div class="MainWrapper">
    <div class="Wrapper">

        <div class="AppointmentArea">
            <div class="row">
                <div class="col-sm-8">
                    <div class="AppointmentForm">
                        <h3>Sale</h3>
                        <form class="form-horizontal" method="POST" action="{{ route('salesave') }}">
                            @csrf
                            <div class="form-group">
                                <label class="control-label col-sm-4">First Name</label>
                                <div class="col-sm-8">
                                    <input type="text" name="fname" class="form-control" required placeholder="First Name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">Last Name</label>
                                <div class="col-sm-8">
                                    <input type="text" name="lname" class="form-control" required placeholder="Last Name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">Email</label>
                                <div class="col-sm-8">
                                    <input type="text" name="email" class="form-control" required placeholder="Email">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Contact Number</label>
                                <div class="col-sm-8">
                                    <input type="text" name="number" class="form-control" required placeholder="Contact Number">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Date Of Birth</label>
                                <div class="col-sm-8">
                                    <input type="date" name="dob" class="form-control" required placeholder="01/08/1996">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Address</label>
                                <div class="col-sm-8">
                                    <input type="text" name="address" class="form-control" required placeholder="Address">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Product</label>
                                <div class="col-sm-8">
                                    <input type="text" name="product" class="form-control" required placeholder="Product name">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Quantity</label>
                                <div class="col-sm-8">
                                    <input type="number" name="quantity" class="form-control" required placeholder="Quantity">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Payment Type</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="type">
                                        <option value="Card" selected="selected"> Card </option>
                                        <option value="Cash"> Cash </option>
                                    </select> 
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Amount</label>
                                <div class="col-sm-8">
                                    <input type="text" name="amount" class="form-control" required  placeholder="Amount">
                                </div>
                            </div>                       

                            <div class="form-group">
                                <div class="Buttons"> 
                                    <button type="submit">Save</button> 
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="CustomerBox">
                    </div>
                </div> 
            </div>

        </div>

    </div>
</div>

@endsection

@section('scripts')
@endsection