@extends('layouts.admin')
@section('content')

<div class="MainWrapper">
    <div class="Wrapper">

        <div class="AppointmentArea">
            
            <div class="row">
                <div class="col-sm-8">
                    <div class="AppointmentForm">
                        <h3>Manage Client Info </h3>
                        <form class="form-horizontal">

                            <div class="ClientOption">
                                <a href="{{ route('client-book') }}">Back</a>
                                <a href="{{ route('booking-view', [$clients['id'], "client"]) }}">Booking</a>
                                <!--<a href="{{ route('sale-view', $clients['id']) }}">Sale</a>-->
                                <a href="JavaScript:Void(0);" data-toggle="modal" data-target="#Email">Email/SMS</a>
                                <a href="JavaScript:Void(0);" data-toggle="modal" data-target="#Notice">ADD Notes</a>
                                <a href="{{ route('client-edit', $clients['id']) }}">Edit</a>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">First Name</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['fname'] }}" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">Last Name</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['lname'] }}" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">Email</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['email'] }}" readonly>
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Contact Number</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['number'] }}" readonly>
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Date Of Birth</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['dob'] }}" readonly>
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Address</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['address'] }}" readonly>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-sm-4">Note</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['note'] }}" readonly>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="CustomerBox">
                        <h3>Customer File 
                            @if(Auth::user()->role != 3)
                                <a href="{{ route('client-send-detail', $clients['id']) }}">Send Details</a>
                            @endif
                        </h3>
                        
                        <article>

                            <!-- <h5>Order History</h5>
                            <ul>
                                <li><span>Photoshoot</span> 25 Nov, 2019</li>
                                <li><span>Photoshoot</span> 27 Nov, 2019</li>
                            </ul> -->

                            <ol>
                            @if(count($attachArr) > 0)
                                @foreach($attachArr as $attach)
                                    <li><a href="{{ asset("storage/clientattach/".$attach['attach']) }}" target="_blank"><i class="fa fa-file"></i></a></li>
                                @endforeach
                            @endif
                            </ol>
                        </article>
                    </div>

                    <div class="CustomerBox">
                            <h3> Notes </h3>
                            <article>
                                @if(count($getNotes) > 0)
                                @foreach($getNotes as $getNote)
                                    <h3> {{ $getNote->title }} </h3>
                                    <article>
                                        @if(strlen($getNote->datetime) > 1)
                                            {{ $getNote->datetime }}
                                        @endif
                                        <p> {{ $getNote->message }} </p>
                                    </article>
                                    <hr>
                                @endforeach
                                @endif
                            </article>
                        </div>
                </div>
            </div>

        </div>

    </div>
</div>

<div class="ModalBox">
        <div id="Notice" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">                    
                        <a href="JavaScript:Void(0);" class="CloseModal" data-dismiss="modal">&times;</a>
                        <div class="Category">
                            <form method="post" action="{{ route('client-note') }}">
                                @csrf
                                <h3>Notice </h3>
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" name="title" class="form-control" placeholder="Enter Title">
                                </div>
                                <div class="form-group">
                                    <label>Message</label>
                                    <textarea rows="5" name="message" class="form-control" placeholder="Message.."></textarea>
                                </div>
                                <input type="hidden" name="type" value="client">
                                <input type="hidden" name="attach" value="{{ $clients['id'] }}">
                                <button type="submit">POST</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="Email" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">                    
                        <a href="JavaScript:Void(0);" class="CloseModal" data-dismiss="modal">&times;</a>
                        <div class="Category">
                            <form method="post" action="{{ route('client-email', $clients['id']) }}">
                                @csrf
                                <h3>Email / SMS Send </h3>
                                <div class="form-group">
                                    <label>To,</label>
                                    <input type="text" class="form-control" name="to" value="{{ $clients['email'] }}" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Message</label>
                                    <textarea rows="5" class="form-control" name="message" placeholder="Message.."></textarea>
                                </div>
                                <button type="submit">SEND</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection

@section('scripts')
@endsection