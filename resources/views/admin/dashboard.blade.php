@extends('layouts.admin')
@section('content')

    <div class="MainWrapper">
        <div class="Wrapper">

            <div class="DashboardArea">
                <div class="row">
                    <div class="col-sm-6">

                        <div class="TargetArea">
                        @if(Auth::user()->role == 1)
                            <a href="JavaScript:Void(0);" data-toggle="modal" data-target="#Calender">Set Target</a>
                        @endif
                            <h3>Targets</h3>
                            <ul>
                                <li>
                                    <div class="OverBox" id="dailyclick" style="background-color:#016bb5;color:#fff;">
                                        <p>Daily</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="OverBox" id="monthclick">
                                        <p>Monthly</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="OverBox" id="yearclick">
                                        <p>Yearly</p>
                                    </div>
                                </li>
                            </ul>
                            <div class="clear"></div>
                        </div>

                        <div class="StatusArea" id="dailyStatusArea">
                            <ul>
                                <li>
                                    <p>Photoshoot Booking</p>
                                    <div class="progress">
                                        <div class="progress-bar active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{$dailyPercent['target1']}}%"> {{ (int)$dailyPercent['target1']}}% </div>
                                    </div>
                                </li>
                                <li>
                                    <p>Lead</p>
                                    <div class="progress">
                                        <div class="progress-bar active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{$dailyPercent['target2']}}%"> {{ (int)$dailyPercent['target2']}}% </div>
                                    </div>
                                </li>
                                <li>
                                    <p>Converted Lead</p>
                                    <div class="progress">
                                        <div class="progress-bar active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{$dailyPercent['target3']}}%"> {{ (int)$dailyPercent['target3']}}% </div>
                                    </div>
                                </li>
                                <li>
                                    <p>Customer Satisfaction</p>
                                    <div class="progress">
                                        <div class="progress-bar active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{$dailyPercent['target4']}}%"> {{ (int)$dailyPercent['target4']}}% </div>
                                    </div>
                                </li>
                                <li>
                                    <p>Binned</p>
                                    <div class="progress">
                                        <div class="progress-bar active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{$dailyPercent['target5']}}%"> {{ (int)$dailyPercent['target5']}}% </div>
                                    </div>
                                </li>
                                <li>
                                    <p>Photoshoot Not Viewed</p>
                                    <div class="progress">
                                        <div class="progress-bar active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{$dailyPercent['target6']}}%"> {{ (int)$dailyPercent['target6']}}% </div>
                                    </div>
                                </li>
                                <li>
                                    <p>Phone Call Lists</p>
                                    <div class="progress">
                                        <div class="progress-bar active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{$dailyPercent['target7']}}%"> {{ (int)$dailyPercent['target7']}}% </div>
                                    </div>
                                </li>
                                <li>
                                    <p>To Do List</p>
                                    <div class="progress">
                                        <div class="progress-bar active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{$dailyPercent['target8']}}%"> {{ (int)$dailyPercent['target8']}}% </div>
                                    </div>
                                </li>
                            </ul>
                            <div class="clear"></div>
                        </div>

                        <div class="StatusArea" id="monthStatusArea" style="display: none;">
                            <ul>
                                <li>
                                    <p>Photoshoot Booking</p>
                                    <div class="progress">
                                        <div class="progress-bar active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{$monthPercent['target1']}}%"> {{ (int)$monthPercent['target1']}}% </div>
                                    </div>
                                </li>
                                <li>
                                    <p>Lead</p>
                                    <div class="progress">
                                        <div class="progress-bar active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{$monthPercent['target2']}}%"> {{ (int)$monthPercent['target2']}}% </div>
                                    </div>
                                </li>
                                <li>
                                    <p>Converted Lead</p>
                                    <div class="progress">
                                        <div class="progress-bar active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{$monthPercent['target3']}}%"> {{ (int)$monthPercent['target3']}}% </div>
                                    </div>
                                </li>
                                <li>
                                    <p>Customer Satisfaction</p>
                                    <div class="progress">
                                        <div class="progress-bar active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{$monthPercent['target4']}}%"> {{ (int)$monthPercent['target4']}}% </div>
                                    </div>
                                </li>
                                <li>
                                    <p>Binned</p>
                                    <div class="progress">
                                        <div class="progress-bar active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{$monthPercent['target5']}}%"> {{ (int)$monthPercent['target5']}}% </div>
                                    </div>
                                </li>
                                <li>
                                    <p>Photoshoot Not Viewed</p>
                                    <div class="progress">
                                        <div class="progress-bar active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{$monthPercent['target6']}}%"> {{ (int)$monthPercent['target6']}}% </div>
                                    </div>
                                </li>
                                <li>
                                    <p>Phone Call Lists</p>
                                    <div class="progress">
                                        <div class="progress-bar active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{$monthPercent['target7']}}%"> {{ (int)$monthPercent['target7']}}% </div>
                                    </div>
                                </li>
                                <li>
                                    <p>To Do List</p>
                                    <div class="progress">
                                        <div class="progress-bar active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{$monthPercent['target8']}}%"> {{ (int)$monthPercent['target8']}}% </div>
                                    </div>
                                </li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                        <div class="StatusArea" id="yearStatusArea" style="display: none;">
                            <ul>
                                <li>
                                    <p>Photoshoot Booking</p>
                                    <div class="progress">
                                        <div class="progress-bar active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{$yearPercent['target1']}}%"> {{ (int)$yearPercent['target1']}}% </div>
                                    </div>
                                </li>
                                <li>
                                    <p>Lead</p>
                                    <div class="progress">
                                        <div class="progress-bar active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{$yearPercent['target2']}}%"> {{ (int)$yearPercent['target2']}}% </div>
                                    </div>
                                </li>
                                <li>
                                    <p>Converted Lead</p>
                                    <div class="progress">
                                        <div class="progress-bar active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{$yearPercent['target3']}}%"> {{ (int)$yearPercent['target3']}}% </div>
                                    </div>
                                </li>
                                <li>
                                    <p>Customer Satisfaction</p>
                                    <div class="progress">
                                        <div class="progress-bar active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{$yearPercent['target4']}}%"> {{ (int)$yearPercent['target4']}}% </div>
                                    </div>
                                </li>
                                <li>
                                    <p>Binned</p>
                                    <div class="progress">
                                        <div class="progress-bar active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{$yearPercent['target5']}}%"> {{ (int)$yearPercent['target5']}}% </div>
                                    </div>
                                </li>
                                <li>
                                    <p>Photoshoot Not Viewed</p>
                                    <div class="progress">
                                        <div class="progress-bar active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{$yearPercent['target6']}}%"> {{ (int)$yearPercent['target6']}}% </div>
                                    </div>
                                </li>
                                <li>
                                    <p>Phone Call List</p>
                                    <div class="progress">
                                        <div class="progress-bar active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{$yearPercent['target7']}}%"> {{ (int)$yearPercent['target7']}}% </div>
                                    </div>
                                </li>
                                <li>
                                    <p>To Do List</p>
                                    <div class="progress">
                                        <div class="progress-bar active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{$yearPercent['target8']}}%"> {{ (int)$yearPercent['target8']}}% </div>
                                    </div>
                                </li>
                            </ul>
                            <div class="clear"></div>
                        </div>

                    </div>

                    <div class="col-sm-6">
                        <div class="TodoArea">
                            <h3>To-DO</h3>

                            <ul>
                            @if(count($todoArrays)>0)
                            @foreach($todoArrays as $todoArray)
                                <li>
                                    <div class="TodoBox">
                                        <h4><span class="Check"></span> <a href="javascript:void(0)"> {{ $todoArray['title'] }} </a> </h4>
                                        <p> {{ $todoArray['content'] }} </p>
                                        <h6>
                                            <span><i class="fa fa-calendar-check-o"></i> {{ $todoArray['created_at'] }} </span>
                                            <span><i class="fa fa-user-circle-o"></i> {{ $todoArray['name'] }} </span>
                                        </h6>
                                    </div>
                                </li>
                            @endforeach
                            @endif
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>

                </div>

                <div class="NoticesetArea">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="NoticesetBox">
                                <div class="NoticesetHead">
                                    <h4>Notice Board </h4>
                                    <a href="JavaScript:Void(0);" data-toggle="modal" data-target="#NoticeMsg" class="Pink"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                                    <a href="{{ route('notice-logs') }}" class="Light_Blue">Log</a>
                                </div>

                                <div class="NoticesetBody">
                                    <ul class="BoardList">
                                    @if(count($notices) > 0)
                                    @foreach($notices as $notice)
                                        <li>
                                            <div class="BoardBox">
                                                <figure><img src="{{asset('admin/images/Profile.png')}}"></figure>
                                                <h3><a href="{{ route('notice', $notice['nid']) }}"> {{ $notice['title'] }} </a> <span>{{ $notice['countNote'] }}</span> </h3>
                                                <p><strong>{{ $notice['uname'] }}, {{ $notice['date'] }}</strong> {{ $notice['message'] }}</p>
                                                 
                                                <div class="Complete">
                                                    <span> <a href="{{ route('notice-complete', $notice['nid']) }}"> Complete </a> </span>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                    @endif
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="NoticesetBox">
                                <div class="NoticesetHead">
                                    <h4>Phone Messages </h4>
                                    <a href="JavaScript:Void(0);" data-toggle="modal" data-target="#PhoneMsg" class="Pink"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                                    <a href="{{ route('phone-logs') }}" class="Light_Blue">Log</a>
                                </div>

                                <div class="NoticesetBody">
                                    <ul class="BoardList">
                                    @if(count($phones) > 0)
                                    @foreach($phones as $phone)
                                        <li>
                                            <div class="BoardBox">
                                                <figure><img src="{{asset('admin/images/Profile.png')}}"></figure>
                                                <h3><a href="{{ route('phone-board', $phone['pid']) }}">{{ $phone['title'] }}</a> <span>{{ $phone['countNote'] }}</span> </h3>
                                                <p><strong>{{ $phone['uname'] }}, {{ $phone['date'] }}</strong> {{ $phone['message'] }} </p>
                                                <div class="Complete">
                                                    <span> <a href="{{ route('phone-complete', $phone['pid']) }}"> Complete </a> </span>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                    @endif    
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div id="Calender" class="modal fade" role="dialog">
            <div class="modal-dialog"> 
                <div class="modal-content"> 
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#Today">Today</a></li>
                            <li><a data-toggle="tab" href="#Month">This Month</a></li>
                            <li><a data-toggle="tab" href="#Year">This Year</a></li>
                        </ul>
    
                        <div class="tab-content">
                            <div id="Today" class="tab-pane fade in active">
                                <form method="post" action="{{ route('target-daily') }}">
                                    @csrf
                                    <div class="form-group">
                                        <input type="number" name="photobooking" class="form-control" min="1" max="100" required @if(count($dailyTargetArr)>0) value="{{ $dailyTargetArr['photobooking'] }}" @endif>
                                        <label>Photoshoot Booking</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="lead" class="form-control" min="1" max="100" required @if(count($dailyTargetArr)>0) value="{{ $dailyTargetArr['lead'] }}" @endif> 
                                        <label>Lead</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="convertlead" class="form-control" min="1" max="100" required @if(count($dailyTargetArr)>0) value="{{ $dailyTargetArr['convertlead'] }}" @endif> 
                                        <label>Converted Lead</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="satisfaction" class="form-control" min="1" max="100" required @if(count($dailyTargetArr)>0) value="{{ $dailyTargetArr['satisfaction'] }}" @endif> 
                                        <label>Customer Satisfaction</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="binned" class="form-control" min="1" max="100" required @if(count($dailyTargetArr)>0) value="{{ $dailyTargetArr['binned'] }}" @endif> 
                                        <label>Binned</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="notviewed" class="form-control" min="1" max="100" required @if(count($dailyTargetArr)>0) value="{{ $dailyTargetArr['notviewed'] }}" @endif> 
                                        <label>Photoshoot Not Viewed</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="phonecall" class="form-control" min="1" max="100" required @if(count($dailyTargetArr)>0) value="{{ $dailyTargetArr['phonecall'] }}" @endif> 
                                        <label>Phone Call List</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="todo" class="form-control" min="1" max="100" required @if(count($dailyTargetArr)>0) value="{{ $dailyTargetArr['todo'] }}" @endif>
                                        <label>To Do List</label>
                                    </div>
    
                                    <button type="submit">Apply</button>
                                </form>
                            </div>
                            <div id="Month" class="tab-pane fade">
                                <form method="post" action="{{ route('target-month') }}">
                                    @csrf
                                    <div class="form-group">
                                        <input type="number" name="photobooking" class="form-control used" min="1" max="100" required @if(count($monthTargetArr)>0) value="{{ $monthTargetArr['photobooking'] }}" @endif> 
                                        <label>Photoshoot Booking</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="lead" class="form-control used" min="1" max="100" required @if(count($monthTargetArr)>0) value="{{ $monthTargetArr['lead'] }}" @endif> 
                                        <label>Lead</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="convertlead" class="form-control used" min="1" max="100" required @if(count($monthTargetArr)>0) value="{{ $monthTargetArr['convertlead'] }}" @endif> 
                                        <label>Converted Lead</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="satisfaction" class="form-control used" min="1" max="100" required @if(count($monthTargetArr)>0) value="{{ $monthTargetArr['satisfaction'] }}" @endif> 
                                        <label>Customer Satisfaction</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="binned" class="form-control used" min="1" max="100" required @if(count($monthTargetArr)>0) value="{{ $monthTargetArr['binned'] }}" @endif> 
                                        <label>Binned</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="notviewed" class="form-control used" min="1" max="100" required @if(count($monthTargetArr)>0) value="{{ $monthTargetArr['notviewed'] }}" @endif> 
                                        <label>Photoshoot Not Viewed</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="phonecall" class="form-control used" min="1" max="100" required @if(count($monthTargetArr)>0) value="{{ $monthTargetArr['phonecall'] }}" @endif> 
                                        <label>Phone Call List</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="todo" class="form-control used" min="1" max="100" required @if(count($monthTargetArr)>0) value="{{ $monthTargetArr['todo'] }}" @endif>
                                        <label>To Do List</label>
                                    </div>
    
                                    <button type="submit">Apply</button>
    
                                </form>
                            </div>
                            <div id="Year" class="tab-pane fade">
                                <form method="post" action="{{ route('target-year') }}">
                                    @csrf
                                    <div class="form-group">
                                        <input type="number" name="photobooking" class="form-control used" min="1" max="100" required @if(count($yearTargetArr)>0) value="{{ $yearTargetArr['photobooking'] }}" @endif> 
                                        <label>Photoshoot Booking</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="lead" class="form-control used" min="1" max="100" required @if(count($yearTargetArr)>0) value="{{ $yearTargetArr['lead'] }}" @endif> 
                                        <label>Lead</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="convertlead" class="form-control used" min="1" max="100" required @if(count($yearTargetArr)>0) value="{{ $yearTargetArr['convertlead'] }}" @endif> 
                                        <label>Converted Lead</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="satisfaction" class="form-control used" min="1" max="100" required @if(count($yearTargetArr)>0) value="{{ $yearTargetArr['satisfaction'] }}" @endif> 
                                        <label>Customer Satisfaction</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="binned" class="form-control used" min="1" max="100" required @if(count($yearTargetArr)>0) value="{{ $yearTargetArr['binned'] }}" @endif> 
                                        <label>Binned</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="notviewed" class="form-control used" min="1" max="100" required @if(count($yearTargetArr)>0) value="{{ $yearTargetArr['notviewed'] }}" @endif> 
                                        <label>Photoshoot Not Viewed</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="phonecall" class="form-control used" min="1" max="100" required @if(count($yearTargetArr)>0) value="{{ $yearTargetArr['phonecall'] }}" @endif> 
                                        <label>Phone Call List</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="todo" class="form-control used" min="1" max="100" required @if(count($yearTargetArr)>0) value="{{ $yearTargetArr['todo'] }}" @endif>
                                        <label>To Do List</label>
                                    </div>
    
                                    <button type="submit">Apply</button>
                                </form>
                            </div>
                        </div>
                    </div> 
                </div>
    
            </div>
        </div>
    
    
        <div id="NoticeMsg" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        
                        <a href="JavaScript:Void(0);" class="CloseModal" data-dismiss="modal">&times;</a>
                        <div class="Category">
                            <form method="post" action="{{ route('add-notice') }}">
                                @csrf
                                <h3>Notice Message</h3>
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" name="title" class="form-control" placeholder="Enter Title">
                                </div>
                                <div class="form-group">
                                    <label>Message</label>
                                    <textarea rows="5" name="message" class="form-control" placeholder="Message.."></textarea>
                                </div>
                                <button type="submit">POST</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    
        <div id="PhoneMsg" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        
                        <a href="JavaScript:Void(0);" class="CloseModal" data-dismiss="modal">&times;</a>
                        <div class="Category">
                            <form method="post" action="{{ route('add-phone') }}">
                                @csrf
                                <h3>Phone Message</h3>
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" name="name" class="form-control" placeholder="Enter Name">
                                </div>
                                <div class="form-group">
                                    <label>Number</label>
                                    <input type="text" name="number" class="form-control" placeholder="+91 9876543210">
                                </div>
                                <div class="form-group">
                                    <label>Reason For Calling</label>
                                    <input type="text" name="reason" class="form-control" placeholder="Enter Reason">
                                </div>
                                <div class="form-group">
                                    <label>Message</label>
                                    <textarea rows="5" name="message" class="form-control" placeholder="Message.."></textarea>
                                </div>
                                <button type="submit">POST</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#dailyclick').on('click', function() {
                $('#dailyStatusArea').css("display","block");
                $('#monthStatusArea').css("display","none");
                $('#yearStatusArea').css("display","none");
                
                $('#dailyclick').css("background-color","#016bb5");
                $('#dailyclick').css("color","#fff");

                $('#monthclick').css("background-color","#fff");
                $('#monthclick').css("color","#000");

                $('#yearclick').css("background-color","#fff");
                $('#yearclick').css("color","#000");

            });
            $('#monthclick').on('click', function() {
                $('#dailyStatusArea').css("display","none");
                $('#monthStatusArea').css("display","block");
                $('#yearStatusArea').css("display","none");

                $('#dailyclick').css("background-color","#fff");
                $('#dailyclick').css("color","#000");

                $('#monthclick').css("background-color","#ac70ba");
                $('#monthclick').css("color","#fff");

                $('#yearclick').css("background-color","#fff");
                $('#yearclick').css("color","#000");
            });
            $('#yearclick').on('click', function() {
                $('#dailyStatusArea').css("display","none");
                $('#monthStatusArea').css("display","none");
                $('#yearStatusArea').css("display","block");

                $('#dailyclick').css("background-color","#fff");
                $('#dailyclick').css("color","#000");

                $('#monthclick').css("background-color","#fff");
                $('#monthclick').css("color","#000");

                $('#yearclick').css("background-color","#00caa9");
                $('#yearclick').css("color","#fff");
            });
        });
    </script>
@endsection