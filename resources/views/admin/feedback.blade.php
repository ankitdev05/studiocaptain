@extends('layouts.admin')
@section('content')

<div class="MainWrapper">
    <div class="Wrapper">
        <div class="AppointmentArea">
            <div class="row">
                <div class="col-sm-8">
                    <div class="AppointmentForm">
                        <h3>Customer Satisfactory Feedback Form</h3>
                        <form class="form-horizontal" id="formnone" method="post" action="{{ route('feedback-save', $id) }}">
                            @csrf
                            <div class="form-group">
                                <label class="control-label col-sm-4">How do you rate your overall experience?</label>
                                <div class="col-sm-2" style="padding-top: 9px;">
									<input type="radio" name="experience" id="radio_experience" value="bad"> Bad
                                </div>
                                <div class="col-sm-2" style="padding-top: 9px;">
									<input type="radio" name="experience" id="radio_experience" value="average"> Average
                                </div>
                                <div class="col-sm-2" style="padding-top: 9px;">
									<input type="radio" name="experience" id="radio_experience" value="good"> Good
                                </div>
                                @if ($errors->has('experience'))
                                    <p class="invalid-feedback" role="alert">
                                        <strong> The Experience field is required. </strong>
                                    </p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">Comments</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" type="textarea" name="comments" id="comments" placeholder="Your Comments" maxlength="6000" rows="7"></textarea>
                                    @if ($errors->has('lname'))
                                        <p class="invalid-feedback" role="alert">
                                            <strong> The Last name field is required. </strong>
                                        </p>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4"> Rating </label>
                                <div class="col-sm-8">
    								<input id="input-1" name="inputone" class="rating rating-loading" value="0" data-min="0" data-max="5" data-step="1" data-size="md">

                                </div>
                            </div>
                            

                            <div class="form-group">
                                <div class="Buttons">
                                    <button type="submit"> Submit </button> 
                                </div>
                            </div>

                        </form>

                        <div class="panel panel-success" style="display: none;">
					      	<div class="panel-heading text-center"> Success! </div>
					      	<div class="panel-body text-center"> Feedback Submitted Successfully </div>
					    </div>

					    <div class="panel panel-danger" style="display: none;">
					      	<div class="panel-heading text-center"> Alert! </div>
					      	<div class="panel-body text-center"> Feedback Already Submitted </div>
					    </div>

                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
<style type="text/css">
	.Navigation{display: none;}
	.Profile{display: none;}
</style>
@endsection

@section('scripts')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/css/star-rating.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/js/star-rating.min.js"></script>

<script>
$("#input-id").rating();
</script>


@if (Session::has('success'))
	<script type="text/javascript">
		$(document).ready(function() {
			$("#formnone").css("display","none");
			$(".panel-success").css("display","block");
			$(".panel-danger").css("display","none");
		});
	</script>
@endif

@if (Session::has('error'))
	<script type="text/javascript">
		$(document).ready(function() {
			$("#formnone").css("display","none");
			$(".panel-success").css("display","none");
			$(".panel-danger").css("display","block");
		});
	</script>
@endif


@endsection