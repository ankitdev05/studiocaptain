<table>
    <thead>
        <tr>
            <th>Time</th>
            <th>Studio 1</th>
            <th>Studio 2</th>
            <th>Makeover</th>
            <th>Nails</th>
            <th>Viewing</th> 
        </tr>
    </thead>

    <tbody>
    @foreach($gettimeWises as $gettimeWise)
        <tr>
            <td> {{ $gettimeWise['time'] }} </td>
            <td>
            @php $bookdata1 = $gettimeWise['data1'] @endphp
            @if(count($bookdata1) > 0)
                <aside class="Booking">
                    <a href="{{ route('appointment-view',$bookdata1['id']) }}">
                       <p><span>Name:</span> {{ $bookdata1['name'] }} </p>
                       <p><span>Booking Number:</span> {{ $bookdata1['bookNum'] }}</p>
                       <p><span>Booking Date:</span> {{ $bookdata1['bookdate'] }}</p>
                       <p><span>Time:</span> {{ $bookdata1['booktime'] }}</p> 
                    </a>
                </aside>
            @else
                <aside class="Booking colorempty">
                    <a href="javascript:void(0)" class="{{$gettimeWise['time']}}" onclick="gottobooking(this.className)">  Booking </a>
                </aside>
            @endif
            </td>

            <td>
            @php $bookdata2 = $gettimeWise['data2'] @endphp
            @if(count($bookdata2) > 0)
                <aside class="Booking">
                    <a href="{{ route('appointment-view',$bookdata2['id']) }}">
                    <p><span>Name:</span> {{ $bookdata2['name'] }} </p>
                    <p><span>Booking Number:</span> {{ $bookdata2['bookNum'] }}</p>
                    <p><span>Booking Date:</span> {{ $bookdata2['bookdate'] }}</p>
                    <p><span>Time:</span> {{ $bookdata2['booktime'] }}</p>
                    </a>
                </aside>
            @else
                <aside class="Booking colorempty">
                    <a href="javascript:void(0)" class="{{$gettimeWise['time']}}" onclick="gottobooking(this.className)">  Booking </a>
                </aside>  
            @endif
            </td>

            <td>
            @php $bookdata3 = $gettimeWise['data3'] @endphp
            @if(count($bookdata3) > 0)
                <aside class="Booking">
                    <a href="{{ route('treatment-appointment-view',$bookdata3['id']) }}">
                       <p><span>Name:</span> {{ $bookdata3['name'] }} </p>
                        <p><span>Booking Number:</span> {{ $bookdata3['bookNum'] }}</p>
                        <p><span>Booking Date:</span> {{ $bookdata3['bookdate'] }}</p>
                        <p><span>Time:</span> {{ $bookdata3['booktime'] }}</p>
                    </a>
                </aside>
            @else
                <aside class="Booking colorempty">
                    <a href="javascript:void(0)" class="{{$gettimeWise['time']}}" onclick="gottobooking(this.className)">  Booking </a>
                </aside>
            @endif
            </td>

            <td>
            @php $bookdata4 = $gettimeWise['data4'] @endphp
            @if(count($bookdata4) > 0)
                <aside class="Booking">
                    <a href="{{ route('treatment-appointment-view',$bookdata4['id']) }}">
                        <p><span>Name:</span> {{ $bookdata4['name'] }} </p>
                        <p><span>Booking Number:</span> {{ $bookdata4['bookNum'] }}</p>
                        <p><span>Booking Date:</span> {{ $bookdata4['bookdate'] }}</p>
                        <p><span>Time:</span> {{ $bookdata4['booktime'] }}</p>
                    </a>
                </aside>
            @else
                <aside class="Booking colorempty">
                    <a href="javascript:void(0)" class="{{$gettimeWise['time']}}" onclick="gottobooking(this.className)">  Booking </a>
                </aside>
            @endif
            </td>

            <td>
            @php $bookdata5 = $gettimeWise['data5'] @endphp
            @if(count($bookdata5) > 0)
                <aside class="Booking">
                    <a href="{{ route('viewing',$bookdata5['bookNum']) }}">
                        <p><span>Name:</span> {{ $bookdata5['name'] }} </p>
                        <p><span>Booking Number:</span> {{ $bookdata5['bookNum'] }}</p>
                        <p><span>Booking Date:</span> {{ $bookdata5['bookdate'] }}</p>
                        <p><span>Time:</span> {{ $bookdata5['booktime'] }}</p>
                    </a>
                </aside>
            @endif
            </td> 
        </tr>
    @endforeach
    </tbody>

</table>