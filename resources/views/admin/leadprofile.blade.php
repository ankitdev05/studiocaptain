@extends('layouts.admin')
@section('content')


<div class="MainWrapper">
    <div class="Wrapper">

        <div class="AppointmentArea">
            <div class="row">
                <div class="col-sm-8">
                    <div class="AppointmentForm">
                        <h3>Manage Client Info</h3>
                            <div class="ClientOption">
                                <a href="{{ route('lead-book') }}">Back</a>
                                <a href="{{ route('booking-view', [$clients['id'],'lead']) }}">Booking</a>
                                <a href="JavaScript:Void(0);" data-toggle="modal" data-target="#Callback">Call Back</a>
                                <a href="JavaScript:Void(0);" data-toggle="modal" data-target="#Bin">Bin</a>  
                                <a href="{{ route('lead-edit', $clients['id']) }}">Edit</a>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">First Name</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" placeholder="Ankit" value="{{ $clients['fname'] }}" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">Last Name</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" placeholder="patel" value="{{ $clients['lname'] }}" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">Email</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" placeholder="ankit@gmail.com" value="{{ $clients['email'] }}" readonly>
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Contact Number</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" placeholder="+91 9876543210" value="{{ $clients['number'] }}" readonly>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">Date Of Birth</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['dob'] }}" readonly>
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Address</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $clients['address'] }}" readonly>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">Lead Source</label>
                                <div class="col-sm-8">
                                    <select class="form-control">
                                        <option>Select Reason for Calling</option>
                                        <option value="service2" @if($clients['lead_source'] == "service2") selected @endif>Type Of Services 2</option>
                                        <option value="service3" @if($clients['lead_source'] == "service3") selected @endif>Type Of Services 3</option>
                                        <option value="service4" @if($clients['lead_source'] == "service4") selected @endif>Type Of Services 4</option>
                                        <option value="service5" @if($clients['lead_source'] == "service5") selected @endif>Type Of Services 5</option>
                                    </select> 
                                </div>
                            </div>  

                            <div class="form-group">
                                <label class="control-label col-sm-4">Note</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" placeholder="Note" value="{{ $clients['note'] }}" readonly>
                                </div>
                            </div> 

                    </div>
                </div>


                 <div class="col-sm-4">
                    <div class="CustomerBox">
                        <h3>Customer File 
                        </h3>
                        <article>
                            <ol>
                            @if(count($attachArr) > 0)
                                @foreach($attachArr as $attach)
                                    <li><a href="{{ asset("storage/clientattach/".$attach['attach']) }}" target="_blank"><i class="fa fa-file"></i></a></li>
                                @endforeach
                            @endif
                            </ol>
                        </article>
                    </div>
                    <div class="CustomerBox">
                    @if(count($getNotes) > 0)
                    @foreach($getNotes as $getNote)
                        <h3> {{ $getNote->title }} </h3>
                        <article>
                            @if(strlen($getNote->datetime) > 1)
                                {{ $getNote->datetime }}
                            @endif
                            <p> {{ $getNote->message }} </p>
                        </article>
                        <hr>
                    @endforeach
                    @endif
                        <article>
                                <h6><a href="JavaScript:Void(0);" data-toggle="modal" data-target="#Notice">Add notes</a></h6>
                        </article>
                    </div> 

            </div>

        </div>

    </div>
</div>

<div id="Notice" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                
                <a href="JavaScript:Void(0);" class="CloseModal" data-dismiss="modal">&times;</a>
                <div class="Category">
                    <form method="post" action="{{ route('lead-note') }}">
                        @csrf
                        <h3>Notice </h3>
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" name="title" class="form-control" placeholder="Enter Title">
                        </div>
                        <div class="form-group">
                            <label>Message</label>
                            <textarea rows="5" name="message" class="form-control" placeholder="Message.."></textarea>
                        </div>
                        <input type="hidden" name="type" value="lead">
                        <input type="hidden" id="cid" name="attach" value="{{ $clients['id'] }}">
                        <button type="submit">POST</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="Callback" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                
                <a href="JavaScript:Void(0);" class="CloseModal" data-dismiss="modal">&times;</a>
                <div class="Category">
                    <form method="post" action="{{ route('lead-note') }}">
                        @csrf
                        <h3>Notice </h3>
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" name="title" class="form-control" value="Callback" readonly>
                        </div>
                        <div class="form-group">
                                <label>Date/Time</label>
                                <input type="date" name="datetime" class="form-control" required>
                            </div>
                        <div class="form-group">
                            <label>Message</label>
                            <textarea rows="5" name="message" class="form-control" placeholder="Message.." required></textarea>
                        </div>
                        <input type="hidden" name="type" value="lead">
                        <input type="hidden" id="cid" name="attach" value="{{ $clients['id'] }}">
                        <button type="submit">POST</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="Bin" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                
                <a href="JavaScript:Void(0);" class="CloseModal" data-dismiss="modal">&times;</a>
                <div class="Category">
                    <form>
                        <h3>Bin </h3>
                        <p>Are you sure want to move in bin!</p>
                        <div class="form-group">
                            <a href="{{ route('bin', $clients['id']) }}"> Confirm </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts')
@endsection