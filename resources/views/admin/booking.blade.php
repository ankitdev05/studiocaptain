@extends('layouts.admin')
@section('content')


<div class="MainWrapper">
    <div class="Wrapper">

        <div class="AppointmentArea">
            <div class="row">
                <div class="col-sm-7">
                    <div class="AppointmentForm">
                        <h3>Booking</h3>
                        <form id="myform" class="form-horizontal" method="POST" action="{{ route('bookingsave') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label class="control-label col-sm-4">First Name</label>
                                <div class="col-sm-8">
                                    <input type="text" name="fname" class="form-control" placeholder="First Name">
                                </div>
                                @if ($errors->has('fname'))
                                    <p class="invalid-feedback" role="alert">
                                        <strong> The First name field is required. </strong>
                                    </p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">Last Name</label>
                                <div class="col-sm-8">
                                    <input type="text" name="lname" class="form-control" placeholder="Last Name">
                                </div>
                                @if ($errors->has('lname'))
                                    <p class="invalid-feedback" role="alert">
                                        <strong> The Last name field is required. </strong>
                                    </p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">Email</label>
                                <div class="col-sm-8">
                                    <input type="text" name="email" class="form-control" placeholder="Email">
                                </div>
                                @if ($errors->has('email'))
                                    <p class="invalid-feedback" role="alert">
                                        <strong> {{ $errors->first('email') }} </strong>
                                    </p>
                                @endif
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Contact Number</label>
                                <div class="col-sm-8">
                                    <input type="text" name="number" class="form-control" placeholder="Contact Number">
                                </div>
                                @if ($errors->has('number'))
                                    <p class="invalid-feedback" role="alert">
                                        <strong> The Contact Number field is required. </strong>
                                    </p>
                                @endif
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Date Of Birth</label>
                                <div class="col-sm-8">
                                    <input type="date" name="dob" class="form-control" placeholder="01/08/1996">
                                </div>
                                @if ($errors->has('dob'))
                                    <p class="invalid-feedback" role="alert">
                                        <strong> The Date of Birth field is required. </strong>
                                    </p>
                                @endif
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Address</label>
                                <div class="col-sm-8">
                                    <input type="text" name="address" class="form-control" placeholder="Address">
                                </div>
                                @if ($errors->has('address'))
                                    <p class="invalid-feedback" role="alert">
                                        <strong> The address field is required. </strong>
                                    </p>
                                @endif
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Type Of Services</label>
                                <div class="col-sm-8">
                                    <select name="catid" id="catselect" class="form-control" required>
                                        <option value="">Type Of Services</option>
                                        @foreach($cats as $cat)
                                            <option value="{{ $cat->id }}"> {{ $cat->name }} </option>
                                        @endforeach
                                    </select>

                                    <select name="subcatid" id="subcatselect" class="form-control" required>
                                        <option>Type Of Services</option>
                                    </select>
                                    
                                    <select name="subsubcatid" id="subsubcatselect" class="form-control" required>
                                        <option>Type Of Services</option>
                                    </select>
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Booking Date</label>
                                <div class="col-sm-8">
                                    <input type="date" name="bookdate" class="form-control" placeholder="01 Nov, 2019">
                                </div>
                                @if ($errors->has('bookdate'))
                                    <p class="invalid-feedback" role="alert">
                                        <strong> {{ $errors->first('bookdate') }} </strong>
                                    </p>
                                @endif
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Booking Time</label>
                                <div class="col-sm-8">
                                    <input id="addtime1" type="text" name="booktime" class="form-control" placeholder="11:00 AM">
                                </div>
                                @if ($errors->has('booktime'))
                                    <p class="invalid-feedback" role="alert">
                                        <strong> {{ $errors->first('booktime') }} </strong>
                                    </p>
                                @endif
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Booking Duration</label>
                                <div class="col-sm-8">
                                    <select name="duration" class="form-control" required>
                                        <option value="30" selected="selected"> 30 minutes </option>
                                        <option value="1"> 1 Hour </option>
                                        <option value="1:30"> 1 Hour 30 minutes </option>
                                        <option value="2"> 2 Hour </option>
                                        <option value="2:30"> 2 Hour 30 minutes </option>
                                        <option value="3"> 3 Hour </option>
                                        <option value="3:30"> 3 Hour 30 minutes </option>
                                        <option value="4"> 4 Hour </option>
                                        <option value="4:30"> 4 Hour 30 minutes </option>
                                    </select>
                                </div>
                                @if ($errors->has('duration'))
                                    <p class="invalid-feedback" role="alert">
                                        <strong> {{ $errors->first('duration') }} </strong>
                                    </p>
                                @endif
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Note</label>
                                <div class="col-sm-8">
                                    <input type="text" name="note" class="form-control" placeholder="Note">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">Payment</label>
                                <div class="col-sm-8">

                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Voucher</label>
                                        <div class="col-sm-8">
                                            <input type="text" id="price1" onkeyup="calTotal()" name="voucher" class="form-control" required placeholder="Enter Voucher">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Deposite</label>
                                        <div class="col-sm-8">
                                            <input type="text" id="price2" onkeyup="calTotal()" name="deposite" class="form-control" required placeholder="Enter Deposite">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Add On</label>
                                        <div class="col-sm-8">
                                            <input type="text" id="price3" onkeyup="calTotal()" name="addon" class="form-control" required placeholder="Enter Add On">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Total</label>
                                        <div class="col-sm-8">
                                            <input type="text" id="totalget" name="total" class="form-control" required placeholder="Total..">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Payment Type</label>
                                        <div class="col-sm-8">
                                            <select name="type" class="form-control" required>
                                                <option value="Cash" selected="selected"> Cash </option>
                                                <option value="Card"> Card </option>
                                                <option value="Pay online"> Pay online </option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-4">Attachment</label>
                                <div class="col-sm-8">
                                    <input type="file" name="fileattach" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="Buttons">
                                    <button type="submit">Confirm</button>
                                    <a href="{{ route('client-book') }}">Cancel</a> 
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="CustomerBox">
                        <h3>Booked Date</h3>
                        <div class="CalenderBox">
                            <div class="responsive-calendar">
                                <div class="controls">
                                    <a class="pull-left" data-go="prev">
                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                    </a>
                                    <h4><span data-head-year></span> <span data-head-month></span></h4>
                                    <a class="pull-right" data-go="next">
                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    </a>
                                </div> 
                                <div class="day-headers">
                                    <div class="day header">Mon</div>
                                    <div class="day header">Tue</div>
                                    <div class="day header">Wed</div>
                                    <div class="day header">Thu</div>
                                    <div class="day header">Fri</div>
                                    <div class="day header">Sat</div>
                                    <div class="day header">Sun</div>
                                </div>
                                <div class="days" data-group="days">
                                </div>
                            </div>
                            <div class="responsive-calendar-placeholder">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('#catselect').on('change', function() {
            var id = $(this).val();
            $.ajax({
                type:"POST",
                url:"{{ route('subcategory') }}",
                data:{id:id, _token: '{{csrf_token()}}'},
                success: function(response) {
                    $('#subcatselect').html(response.msg);
                }
            });
        });
    });
    $(document).ready(function() {
        $('#subcatselect').on('change', function() {
            var id = $(this).val();
            $.ajax({
                type:"POST",
                url:"{{ route('subsubcategory') }}",
                data:{id:id, _token: '{{csrf_token()}}'},
                success: function(response) {
                    $('#subsubcatselect').html(response.msg);
                }
            });
        });
    });
</script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script>
    // just for the demos, avoids form submit
    // jQuery.validator.setDefaults({
    //     debug: true,
    //     success: "valid"
    // });
    $( "#myform" ).validate({
        rules: {
            fname: 'required',
            lname: 'required',
            email: 'required',
            number: 'required',
            dob: 'required',
            address: 'required',
            catid: 'required',
            subcatid: 'required',
            subsubcatid: 'required',
            bookdate: 'required',
            booktime: 'required',
            duration: 'required',
        },
        messages: {
            fname: 'First name field is required',
            lname: 'Last name field is required',
            email: 'Email field is required',
            number: 'Contact number field is required',
            dob: 'Date of Birth field is required',
            address: 'Address field is required',
            catid: 'Service Type field is required',
            subcatid: 'Service Type field is required',
            subsubcatid: 'Service Type field is required',
            bookdate: 'Booking Date field is required',
            booktime: 'Booking Time field is required',
            duration: 'Duration Time field is required',

        }
    });
</script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.js"></script>
<script type="text/javascript">
    // $(document).ready(function () {
    //     $('#addtime1').timepicker({
    //        datepicker:false,
    //        formatTime:"h:i a",
    //        step:1
    //     }); 
    // });
    $(document).ready(function () {
        $('#addtime1').timepicker({
            datepicker:false,
            formatTime:"h:i a",
            minTime: '9:00 AM',
            maxTime: '9:00 PM',
            step:15
        }); 
    });
</script>
<script type="text/javascript">
    function calTotal(getprice) {
        var price1 = $("#price1").val();
        if(price1.length < 0) {
            price1 = 0;
        }
        var price2 = $("#price2").val();
        if(price2.length < 0) {
            price2 = 0;
        }
        var price3 = $("#price3").val();
        if(price3.length < 0) {
            price2 = 0;
        }
        var total = Number(price1) + Number(price2) + Number(price3);

        $("#totalget").val(total);
    }

</script>
@endsection