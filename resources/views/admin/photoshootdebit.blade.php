@extends('layouts.admin')
@section('content')

<div class="MainWrapper">
    <div class="Wrapper">

        <div class="AppointmentArea">
            
            <div class="row">
                <div class="col-sm-12"> 
                    <div class="Client-Book">
                        <h3>Photoshoot Debit</h3>
                        <div class="ClientHead">
                        </div>

                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>S No</th> 
                                    <th>Name</th>
                                    <th>Unique Id</th>
                                    <th>Contact Number</th>
                                    <th>Email ID</th>  
                                    <th>Booking Ref.no.</th>
                                    <th>Booking Date</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(count($bookArr)>0)
                            @php $x=1; @endphp
                            @foreach($bookArr as $book)
                                <tr>
                                    <td> {{ $x }} </td>
                                    <td> {{ $book['fname'] }} {{ $book['lname'] }} </td>
                                    <td> {{ $book['unique'] }} </td>
                                    <td>{{ $book['number'] }}</td>
                                    <td>{{ $book['email'] }}</td>
                                    <td>{{ $book['refnum'] }}</td>
                                    <td>{{ $book['bookdate'] }}</td>
                                    <td>{{ $book['total'] }}</td>
                                </tr>
                                @php $x++; @endphp
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div> 
                </div>
                    
            </div>

        </div>

    </div>
</div>

@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable();
        });
    </script>
@endsection