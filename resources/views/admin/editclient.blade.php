@extends('layouts.admin')
@section('content')

<div class="MainWrapper">
        <div class="Wrapper">

            <div class="AppointmentArea">
                

                <div class="row">
                    <div class="col-sm-8">
                        <div class="AppointmentForm">
                            <h3>Edit Client Info</h3>
                            <form class="form-horizontal" method="post" action="{{ route('client-update', $clients['id']) }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">First Name</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="fname" value="{{ $clients['fname'] }}" class="form-control" placeholder="First Name">
                                            @if ($errors->has('fname'))
                                                <p class="invalid-feedback" role="alert">
                                                    <strong> The First name field is required. </strong>
                                                </p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Last Name</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="lname" value="{{ $clients['lname'] }}" class="form-control" placeholder="Last Name">
                                            @if ($errors->has('lname'))
                                                <p class="invalid-feedback" role="alert">
                                                    <strong> The Last name field is required. </strong>
                                                </p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Email</label>
                                        <div class="col-sm-8">
                                            <input type="email" name="email" value="{{ $clients['email'] }}" class="form-control" placeholder="Email">
                                            @if ($errors->has('email'))
                                                <p class="invalid-feedback" role="alert">
                                                    <strong> {{ $errors->first('email') }} </strong>
                                                </p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Contact Number</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="number" value="{{ $clients['number'] }}" class="form-control" placeholder="Contact Number">
                                            @if ($errors->has('number'))
                                                <p class="invalid-feedback" role="alert">
                                                    <strong> The Contact Number field is required. </strong>
                                                </p>
                                            @endif
                                        </div>
                                    </div> 
        
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Date Of Birth</label>
                                        <div class="col-sm-8">
                                            <input type="date" name="dob" value="{{ $clients['dob'] }}" class="form-control" placeholder="01/08/1996">
                                            @if ($errors->has('number'))
                                                <p class="invalid-feedback" role="alert">
                                                    <strong> The Date of birth field is required. </strong>
                                                </p>
                                            @endif
                                        </div>
                                    </div> 
        
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Address</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="address" value="{{ $clients['address'] }}" class="form-control" placeholder="Address">
                                            @if ($errors->has('address'))
                                                <p class="invalid-feedback" role="alert">
                                                    <strong> {{ $errors->first('address') }} </strong>
                                                </p>
                                            @endif
                                        </div>
                                    </div>
        
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Note</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="note" value="{{ $clients['note'] }}" class="form-control" placeholder="Note">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Attach File</label>
                                        <div class="col-sm-8">
                                            <input type="file" name="fileattach" class="form-control" placeholder="File">
                                        </div>
                                    </div>
        
                                    <div class="form-group">
                                        <div class="Buttons">
                                            <button type="submit">Save</button>
                                            <a href="{{ route('client-book') }}">Cancel</a>  
                                        </div>
                                    </div>
        
                                </form>        
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="CustomerBox">
                            
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
@endsection

@section('scripts')

    <script>
        $(document).ready(function() {
            $('#catselect').on('change', function() {
                var id = $(this).val();
                $.ajax({
                    type:"POST",
                    url:"{{ route('subcategory') }}",
                    data:{id:id, _token: '{{csrf_token()}}'},
                    success: function(response) {
                        $('#subcatselect').html(response.msg);
                    }
                });
            });
        });
        $(document).ready(function() {
            $('#subcatselect').on('change', function() {
                var id = $(this).val();
                $.ajax({
                    type:"POST",
                    url:"{{ route('subsubcategory') }}",
                    data:{id:id, _token: '{{csrf_token()}}'},
                    success: function(response) {
                        $('#subsubcatselect').html(response.msg);
                    }
                });
            });
        });
    </script>

@endsection