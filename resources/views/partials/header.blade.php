<header>
    <div class="Header">
        <div class="Logo">
            <img src="{{ asset('admin/images/Logos.png')}}">
        </div>

        <div class="Profile">
            <figure><img src="{{ asset('admin/images/Profile.png')}}"></figure>
            @if(Auth::check())
                <h4> {{ Auth::user()->name }} </h4>
                <p> 
                    @if(Auth::user()->role == 1) Super Admin
                    @elseif(Auth::user()->role == 2) Admin
                    @else Staff member
                    @endif
                
                </p>
            @endif

            <ul>
                @if(Auth::check()) 
                    @if(Auth::user()->role == 1 ||  Auth::user()->role == 2)
                        <li><a href="{{route('add-user')}}"><i class="fa fa-sign-out"></i> Create User </a></li>
                        <li><a href="{{route('user-lists')}}"><i class="fa fa-sign-out"></i> Create List </a></li>
                    @elseif(Auth::user()->role == 2)
                        <li><a href="{{route('add-user')}}"><i class="fa fa-sign-out"></i> Create User </a></li>
                    @endif
                @endif
                <li><a href="{{route('logout')}}"><i class="fa fa-sign-out"></i> Logout</a></li>
            </ul>
        </div>

        <div class="Navigation">
            <ul>
                <li class="{{ Request::is('dashboard*') ? 'active' : '' }}"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="{{ Request::is('calender*') ? 'active' : '' }}"><a href="{{ route('calender') }}">Calendar</a></li>
                <li class="{{ Request::is('client-book*') ? 'active' : '' }}"><a href="{{ route('client-book') }}">Client Book</a></li>
                <li class="{{ Request::is('lead-book*') ? 'active' : '' }}"><a href="{{ route('lead-book') }}">Lead Book</a></li>
                <li class="{{ Request::is('booking*') ? 'active' : '' }}"><a href="{{ route('booking') }}">Booking</a></li>
                <li class="{{ Request::is('sale*') ? 'active' : '' }}"><a href="{{ route('sale') }}">Sale</a></li>
                <li class="{{ Request::is('photoshoot*') ? 'active' : '' }}"><a href="{{ route('photoshoot') }}">Photoshoot</a></li>
                @if(Auth::check()) 
                    @if(Auth::user()->role == 1)
                        <li class="{{ Request::is('finance*') ? 'active' : '' }}"><a href="{{ route('finance') }}">Finance</a></li>
                    @endif
                @endif
            </ul>
        </div>
        <div class="clear"></div>
    </div>
</header>