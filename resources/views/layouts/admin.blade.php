<!DOCTYPE html>
<html>

<head>
    @include('partials.head')
</head>
    
<body>
    @include('partials.header')

    <!-- CONTENT -->
        @yield('content')
    <!-- / CONTENT -->

    @include('partials.footer')
    @yield('scripts')
</body>
</html>